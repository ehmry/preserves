__ignored__ := $(shell ./setup.sh)

PDFS=preserves.pdf preserves-schema.pdf

all: $(PDFS)

clean:
	rm -f $(PDFS)

%.pdf: %.md preserves.css
	google-chrome --headless --disable-gpu --print-to-pdf=$@ \
		http://localhost:4000/preserves/$*.html

test-all:
	make -C tests
	(cd implementations/javascript; npm test)
	(cd implementations/python; make test)
	(cd implementations/racket/preserves; make testonly)
	(cd implementations/rust; cargo test)
