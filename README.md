---
projectpages: "https://gitlab.com/preserves/preserves"
projecttree: "https://gitlab.com/preserves/preserves/tree/main"
title: "Preserves: an Expressive Data Language"
no_site_title: true
---

This [repository]({{page.projectpages}}) contains a
[proposal](preserves.html) and various implementations of *Preserves*,
a new data model and serialization format in many ways comparable to
JSON, XML, S-expressions, CBOR, ASN.1 BER, and so on.

> **WARNING** Everything in this repository is experimental and in
> flux! The design of Preserves is not finalised and may change
> drastically. Please offer any feedback you may have with this in
> mind.

## Core documents

### Preserves data model and serialization formats

Preserves is defined in terms of a syntax-neutral
[data model and semantics](preserves.html#starting-with-semantics)
which all transfer syntaxes share. This allows trivial, completely
automatic, perfect-fidelity conversion between syntaxes.

 - [Preserves tutorial](TUTORIAL.html)
 - [Preserves specification](preserves.html), including semantics,
   data model, textual syntax, and compact binary syntax
 - [Canonical Form for Binary Syntax](canonical-binary.html)
 - [Syrup](https://github.com/ocapn/syrup#pseudo-specification), a
   hybrid binary/human-readable syntax for the Preserves data model

### Preserves schema and queries

 - [Preserves Schema specification](preserves-schema.html)
 - [Preserves Path specification](preserves-path.html)

## Implementations

Implementations of the data model, plus the textual and/or binary transfer syntaxes:

 - [Preserves for Nim](https://git.sr.ht/~ehmry/preserves-nim)
 - [Preserves for Python]({{page.projecttree}}/implementations/python/) ([`pip install preserves`](https://pypi.org/project/preserves/))
 - [Preserves for Racket]({{page.projecttree}}/implementations/racket/preserves/) ([`raco pkg install preserves`](https://pkgs.racket-lang.org/package/preserves))
 - [Preserves for Rust]({{page.projecttree}}/implementations/rust/) ([crates.io package](https://crates.io/crates/preserves))
 - [Preserves for Squeak Smalltalk](https://squeaksource.com/Preserves.html) (`Installer ss project: 'Preserves'; install: 'Preserves'`)
 - [Preserves for TypeScript and JavaScript]({{page.projecttree}}/implementations/javascript/) ([`yarn add @preserves/core`](https://www.npmjs.com/package/@preserves/core))

Implementations of the data model, plus Syrup transfer syntax:

 - [Syrup for Racket](https://github.com/ocapn/syrup/blob/master/impls/racket/syrup/syrup.rkt)
 - [Syrup for Guile](https://github.com/ocapn/syrup/blob/master/impls/guile/syrup.scm)
 - [Syrup for Python](https://github.com/ocapn/syrup/blob/master/impls/python/syrup.py)
 - [Syrup for JavaScript](https://github.com/zarutian/agoric-sdk/blob/zarutian/captp_variant/packages/captp/lib/syrup.js)
 - [Syrup for Haskell](https://github.com/zenhack/haskell-preserves)

## Tools

### Preserves documents

 - [preserves-tool](doc/preserves-tool.html), generic syntax translation and pretty-printing

### Preserves Schema documents and codegen

 - [Tools for working with Preserves Schema](doc/schema-tools.html)

## Additional resources

 - Some [conventions for common data types](conventions.html)
 - [Open questions](questions.html); see also the
   [issues list]({{page.projectpages}}/issues)
 - [Why not Just Use JSON?](why-not-json.html)

## Contact

Tony Garnock-Jones <tonyg@leastfixedpoint.com>

## Licensing

The contents of this repository are made available to you under the
[Apache License, version 2.0](LICENSE)
(<http://www.apache.org/licenses/LICENSE-2.0>), and are Copyright
2018-2021 Tony Garnock-Jones.
