---
title: "Canonical Form for Binary Syntax"
---

  [spec]: preserves.html

When two `Value`s are written down in *canonical form*, comparing
their *syntax* for equivalence gives the same result as comparing them
*semantically* according to the equivalence defined in the
[Preserves specification][spec].[^equivalence-not-ordering]

  [^equivalence-not-ordering]: However, canonical form does *not*
    induce a match between lexicographic ordering on syntax and
    semantic ordering [as specified][spec]. It *only* induces a
    connection between equivalences.

That is, canonical forms are equal if and only if the encoded `Value`s
are equal.

This document specifies canonical form for the Preserves compact
binary syntax.

**Annotations.**
Annotations *MUST NOT* be present.

**Sets.**
The elements of a `Set` *MUST* be serialized sorted in ascending order
by comparing their canonical encoded binary representations.

**Dictionaries.**
The key-value pairs in a `Dictionary` *MUST* be serialized sorted in
ascending order by comparing the canonical encoded binary
representations of their keys.[^no-need-for-by-value]

  [^no-need-for-by-value]: There is no need to order by (key, value)
    pair, since a `Dictionary` has no duplicate keys.

**Other kinds of `Value`.**
There are no special canonicalization restrictions on
`SignedInteger`s, `String`s, `ByteString`s, `Symbol`s, `Boolean`s,
`Float`s, `Double`s, `Record`s, `Sequence`s, or `Embedded`s. The
constraints given for these `Value`s in the [specification][spec]
suffice to ensure canonicity.

<!-- Heading to visually offset the footnotes from the main document: -->
## Notes
