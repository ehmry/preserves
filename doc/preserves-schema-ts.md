---
title: preserves-schema-ts
---

The `preserves-schema-ts` program reads
[Preserves Schema](../preserves-schema.html) DSL input files. For each
input file, it produces a TypeScript source file of the same name but
with `.ts` in place of `.prs`.

## Installation

Install node.js v12 or newer. Then, `yarn global add @preserves/schema`.

## Usage

    Usage: preserves-schema-ts [options] [input...]

    Compile Preserves schema definitions to TypeScript

    Arguments:
      input                      Input filename or glob

    Options:
      --output <directory>       Output directory for modules (default: next to sources)
      --stdout                   Prints each module to stdout one after the other instead of writing them to files in the `--output`
                                 directory
      --base <directory>         Base directory for sources (default: common prefix)
      --core <path>              Import path for @preserves/core (default: "@preserves/core")
      --watch                    Watch base directory for changes
      --traceback                Include stack traces in compiler errors
      --module <namespace=path>  Additional Namespace=path import (default: [])
      -h, --help                 display help for command
