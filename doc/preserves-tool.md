---
title: preserves-tool
---

The `preserves-tool` program is a swiss army knife for translating
Preserves document syntax to and from various surface syntaxes, for
stripping annotations, and for pretty-printing.

It reads Preserves `Value`s (either a fixed number (`--count`) or
until EOF (`--all`)) from `stdin`, autodetecting binary or text syntax
for each by default, and prints the same `Value`s either using the
same or a different Preserves syntax on `stdout`. By default, it will
print using text syntax with pretty-printing.

## Installation

Install Racket. Then, `raco pkg install preserves`.

## Usage

    usage: preserves-tool [ <option> ... ]

    <option> is one of

      --atob
         Text to binary
      --all
         Read until EOF (default)
      --count <n>
         Read n items
      --btoa
         Binary to text
      --ia, --input-any
         Autodetect input mode (default)
      --ib, --input-binary
         Set binary input mode
      --it, --input-text
         Set text input mode
      --ob, --output-binary
         Set binary output mode
      --ot, --output-text
         Set text output mode (default)
      --indent
         Enable indent and set text output mode (default)
      --no-indent
         Disable indent and set text output mode
      --annotations
         Output annotations (default)
      --no-annotations
         Strip annotations
      --help, -h
         Show this help
      --
         Do not treat any remaining argument as a switch (at this level)

     Multiple single-letter switches can be combined after
     one `-`. For example, `-h-` is the same as `-h --`.
