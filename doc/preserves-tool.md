---
title: preserves-tool
---

The `preserves-tool` program is a swiss army knife for working with
Preserves documents.

    preserves-tools 1.0.0

    USAGE:
        preserves-tool <SUBCOMMAND>

    FLAGS:
        -h, --help       Print help information
        -V, --version    Print version information

    SUBCOMMANDS:
        completions
        convert
        help           Print this message or the help of the given subcommand(s)
        quote

## Installation

The tool is
[written in Rust](https://crates.io/crates/preserves-tools).
[Install `cargo`.](https://doc.rust-lang.org/cargo/getting-started/installation.html)
Then, `cargo install preserves-tools`.

## Subcommands

The tool includes three subcommands.

### `preserves-tool convert`

This is the main tool. It can

 - translate between the various Preserves text and binary document
   syntaxes;
 - strip annotations;
 - pretty-print; and
 - break down and filter documents using [preserves path]({{
   site.baseurl }}{% link preserves-path.md %}) selectors.

#### Usage

    preserves-tool-convert

    USAGE:
        preserves-tool convert [FLAGS] [OPTIONS]

    FLAGS:
            --collect
            --escape-spaces
        -h, --help             Print help information
        -V, --version          Print version information

    OPTIONS:
        -i, --input-format <INPUT_FORMAT>
                [default: auto-detect] [possible values: auto-detect, text, binary]

            --indent <on/off>
                [default: on] [possible values: disabled, no, n, off, 0, false,
                enabled, yes, y, on, 1, true]

            --limit <LIMIT>


        -o, --output-format <OUTPUT_FORMAT>
                [default: text] [possible values: text, binary, unquoted]

            --read-annotations <on/off>
                [default: on] [possible values: disabled, no, n, off, 0, false,
                enabled, yes, y, on, 1, true]

            --select <SELECT>
                [default: *]

            --select-output <SELECT_OUTPUT>
                [default: sequence] [possible values: sequence, set]

            --write-annotations <on/off>
                [default: on] [possible values: disabled, no, n, off, 0, false,
                enabled, yes, y, on, 1, true]

### `preserves-tool quote`

This subcommand reads chunks from standard input and outputs each one
as a Preserves `String`, `Symbol`, or `ByteString` using either the
text or binary Preserves surface syntax.

This is useful when writing shell scripts that interact with other
programs using Preserves as an interchange format.

It defaults to taking the entirety of standard input as a single large
chunk, but it can also work with newline- or `nul`-delimited chunks.

#### Usage

```
preserves-tool-quote

USAGE:
    preserves-tool quote [OPTIONS] <SUBCOMMAND>

FLAGS:
    -h, --help       Print help information
    -V, --version    Print version information

OPTIONS:
    -o, --output-format <OUTPUT_FORMAT>    [default: text] [possible values: text, binary, unquoted]

SUBCOMMANDS:
    byte-string
    help           Print this message or the help of the given subcommand(s)
    string
    symbol
```

```
preserves-tool-quote-string

USAGE:
    preserves-tool quote string [FLAGS] [OPTIONS]

FLAGS:
        --escape-spaces
    -h, --help                  Print help information
        --include-terminator
    -V, --version               Print version information

OPTIONS:
        --input-terminator <INPUT_TERMINATOR>    [default: eof] [possible values: eof, newline, nul]
```

```
preserves-tool-quote-symbol

USAGE:
    preserves-tool quote symbol [FLAGS] [OPTIONS]

FLAGS:
        --escape-spaces
    -h, --help                  Print help information
        --include-terminator
    -V, --version               Print version information

OPTIONS:
        --input-terminator <INPUT_TERMINATOR>    [default: eof] [possible values: eof, newline, nul]
```

```
preserves-tool-quote-byte-string

USAGE:
    preserves-tool quote byte-string

FLAGS:
    -h, --help       Print help information
    -V, --version    Print version information
```

### `preserves-tool completions`

This subcommand outputs Bash completion code to stdout, for sourcing
at shell startup time.

#### Usage

Add the following to your `.profile` or similar:

    eval "$(preserves-tool completions bash 2>/dev/null)"

Multiple shell dialects are supported (courtesy of
[`clap`](https://crates.io/crates/clap)):

```
preserves-tool-completions

USAGE:
    preserves-tool completions <dialect>

ARGS:
    <dialect>    [possible values: bash, zsh, power-shell, fish, elvish]

FLAGS:
    -h, --help       Print help information
    -V, --version    Print version information
```
