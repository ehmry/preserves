---
title: Tools for working with Preserves Schema
---

A number of tools for working with [Preserves Schema]({{ site.baseurl
}}{% link preserves-schema.md %}) exist:

 - [preserves-schemac](preserves-schemac.html), generic Schema reader and linter
 - [preserves-schema-rkt](preserves-schema-rkt.html), Racket code generator
 - [preserves-schema-rs](preserves-schema-rs.html), Rust code generator
 - [preserves-schema-ts](preserves-schema-ts.html), TypeScript code generator
