{-|
Dhall encoding of an arbitrary Preserves value
-}
let Preserves/Type
    : Type
    = ∀(Preserves : Type) →
      ∀ ( value
        : { boolean : Bool → Preserves
          , double : Double → Preserves
          , integer : Integer → Preserves
          , string : Text → Preserves
          , symbol : Text → Preserves
          , sequence : List Preserves → Preserves
          , set : List Preserves → Preserves
          , dictionary :
              List { mapKey : Preserves, mapValue : Preserves } → Preserves
          }
        ) →
        Preserves

in  Preserves/Type
