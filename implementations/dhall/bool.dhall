{-|
Create a Preserves boolean item from a `Bool` value
-}
let Preserves = ./Type.dhall

let bool
    : Bool → Preserves
    = λ(x : Bool) →
      λ(Preserves : Type) →
      λ ( value
        : { boolean : Bool → Preserves
          , double : Double → Preserves
          , integer : Integer → Preserves
          , string : Text → Preserves
          , symbol : Text → Preserves
          , sequence : List Preserves → Preserves
          , set : List Preserves → Preserves
          , dictionary :
              List { mapKey : Preserves, mapValue : Preserves } → Preserves
          }
        ) →
        value.boolean x

in  bool
