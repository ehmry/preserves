{-|
Create a Preserves map item from a Dhall `Map` of `Preserves` values

See ./textMap.dhall for an example.
-}
let Prelude = ./Prelude.dhall

let List/map = Prelude.List.map

let Map/Entry = Prelude.Map.Entry

let Preserves = ./Type.dhall

let Preserves/Entry = Map/Entry Preserves Preserves

let Preserves/Map = List Preserves/Entry

let map
    : Preserves/Map → Preserves
    = λ(x : Preserves/Map) →
      λ(Preserves : Type) →
        let Preserves/Entry = Map/Entry Preserves Preserves

        in  λ ( value
              : { boolean : Bool → Preserves
                , double : Double → Preserves
                , integer : Integer → Preserves
                , string : Text → Preserves
                , symbol : Text → Preserves
                , sequence : List Preserves → Preserves
                , set : List Preserves → Preserves
                , dictionary :
                    List { mapKey : Preserves, mapValue : Preserves } →
                      Preserves
                }
              ) →
              value.dictionary
                ( List/map
                    Preserves/Entry@1
                    Preserves/Entry
                    ( λ(e : Preserves/Entry@1) →
                        { mapKey = e.mapKey Preserves value
                        , mapValue = e.mapValue Preserves value
                        }
                    )
                    x
                )

in  map
