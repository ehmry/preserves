{-|
Create a Preserves floating-point number item from a `Double` value
-}
let Preserves = ./Type.dhall

let double
    : Double → Preserves
    = λ(x : Double) →
      λ(Preserves : Type) →
      λ ( value
        : { boolean : Bool → Preserves
          , double : Double → Preserves
          , integer : Integer → Preserves
          , string : Text → Preserves
          , symbol : Text → Preserves
          , sequence : List Preserves → Preserves
          , set : List Preserves → Preserves
          , dictionary :
              List { mapKey : Preserves, mapValue : Preserves } → Preserves
          }
        ) →
        value.double x

in  double
