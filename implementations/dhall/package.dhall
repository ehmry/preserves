{ Type = ./Type.dhall
, sequence = ./sequence.dhall
, sequenceOf = ./sequenceOf.dhall
, bool = ./bool.dhall
, render = ./render.dhall
, double = ./double.dhall
, fromJSON = ./fromJSON.dhall
, integer = ./integer.dhall
, dictionary = ./dictionary.dhall
, dictionaryOf = ./dictionaryOf.dhall
, string = ./string.dhall
}
