{-
Render a `Preserves` value to a diagnostic `Text` value
-}
let Preserves = ./Type.dhall

let Prelude = ./Prelude.dhall

let Map/Type = Prelude.Map.Type

let Text/concatSep = Prelude.Text.concatSep

let Text/concatMapSep = Prelude.Text.concatMapSep

let render
    : Preserves → Text
    = λ(value : Preserves) →
        value
          Text
          { boolean = λ(x : Bool) → if x then "#t" else "#f"
          , double = Double/show
          , integer = Prelude.JSON.renderInteger
          , string = Text/show
          , symbol = λ(sym : Text) → "${sym}"
          , record =
              λ(label : Text) →
              λ(fields : List Text) →
                    "<${label}"
                ++  (if Prelude.List.null Text fields then "" else " ")
                ++  Text/concatSep " " fields
                ++  ">"
          , sequence = λ(xs : List Text) → "[ " ++ Text/concatSep " " xs ++ " ]"
          , set = λ(xs : List Text) → "#{" ++ Text/concatSep " " xs ++ " }"
          , dictionary =
              λ(m : Map/Type Text Text) →
                    "{ "
                ++  Text/concatMapSep
                      " "
                      { mapKey : Text, mapValue : Text }
                      ( λ(e : { mapKey : Text, mapValue : Text }) →
                          "${e.mapKey}: ${e.mapValue}"
                      )
                      m
                ++  " }"
          , embedded = λ(value : Text) → "#!${value}"
          }

let Preserves/boolean = ./boolean.dhall

let Preserves/integer = ./integer.dhall

let Preserves/double = ./double.dhall

let Preserves/symbol = ./symbol.dhall

let Preserves/record = ./record.dhall

let Preserves/sequenceOf = ./sequenceOf.dhall

let Preserves/dictionaryOf = ./dictionaryOf.dhall

let Preserves/dictionaryOfSymbols = Preserves/dictionaryOf Text Preserves/symbol

let Preserves/embedded = ./embedded.dhall

let example0 =
        assert
      :   ''
          ${render
              ( Preserves/dictionaryOfSymbols
                  Preserves
                  (λ(x : Preserves) → x)
                  ( toMap
                      { a = Preserves/integer +1
                      , b =
                          Preserves/sequenceOf
                            Integer
                            Preserves/integer
                            [ +2, +3 ]
                      , c =
                          Preserves/dictionaryOfSymbols
                            Double
                            Preserves/double
                            (toMap { d = 1.0, e = -1.0 })
                      , d = Preserves/embedded (Preserves/boolean True)
                      , e =
                          Preserves/record
                            (Preserves/symbol "capture")
                            [ Preserves/record
                                (Preserves/symbol "_")
                                ([] : List Preserves)
                            ]
                      }
                  )
              )}
          ''
        ≡ ''
          { a: 1 b: [ 2 3 ] c: { d: 1.0 e: -1.0 } d: #!#t e: <capture <_>> }
          ''

in  render
