{-
Render a `Preserves` value to a diagnostic `Text` value
-}
let Preserves = ./Type.dhall

let Prelude = ./Prelude.dhall

let Map/Type = Prelude.Map.Type

let Text/concatSep = Prelude.Text.concatSep

let Text/concatMapSep = Prelude.Text.concatMapSep

let render
    : Preserves → Text
    = λ(value : Preserves) →
        value
          Text
          { boolean = λ(x : Bool) → if x then "#t" else "#f"
          , double = Double/show
          , integer = Integer/show
          , string = Text/show
          , symbol = λ(sym : Text) → "${sym}"
          , sequence = λ(xs : List Text) → "[ " ++ Text/concatSep " " xs ++ " ]"
          , set = λ(xs : List Text) → "#{" ++ Text/concatSep " " xs ++ " }"
          , dictionary =
              λ(m : Map/Type Text Text) →
                    "{ "
                ++  Text/concatMapSep
                      " "
                      { mapKey : Text, mapValue : Text }
                      ( λ(e : { mapKey : Text, mapValue : Text }) →
                          "${e.mapKey}: ${e.mapValue}"
                      )
                      m
                ++  " }"
          }

let Preserves/integer = ./integer.dhall

let Preserves/double = ./double.dhall

let Preserves/symbol = ./symbol.dhall

let Preserves/sequenceOf = ./sequenceOf.dhall

let Preserves/dictionaryOf = ./dictionaryOf.dhall

let Preserves/dictionaryOfSymbols = Preserves/dictionaryOf Text Preserves/symbol

let example0 =
        assert
      :   ''
          ${render
              ( Preserves/dictionaryOfSymbols
                  Preserves
                  (λ(x : Preserves) → x)
                  ( toMap
                      { a = Preserves/integer +1
                      , b =
                          Preserves/sequenceOf
                            Integer
                            Preserves/integer
                            [ +2, +3 ]
                      , c =
                          Preserves/dictionaryOfSymbols
                            Double
                            Preserves/double
                            (toMap { d = 1.0, e = -1.0 })
                      }
                  )
              )}
          ''
        ≡ ''
          { a: +1 b: [ +2 +3 ] c: { d: 1.0 e: -1.0 } }
          ''

in  render
