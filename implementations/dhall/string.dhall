{-|
Create a Preserves string from a `Text` value
-}
let Preserves = ./Type.dhall

let string
    : Text → Preserves
    = λ(x : Text) →
      λ(Preserves : Type) →
      λ ( value
        : { boolean : Bool → Preserves
          , double : Double → Preserves
          , integer : Integer → Preserves
          , string : Text → Preserves
          , symbol : Text → Preserves
          , sequence : List Preserves → Preserves
          , set : List Preserves → Preserves
          , dictionary :
              List { mapKey : Preserves, mapValue : Preserves } → Preserves
          }
        ) →
        value.string x

in  string
