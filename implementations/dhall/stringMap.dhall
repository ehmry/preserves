{-|
Create a Preserves dictionary value from a Dhall `Map` of `Preserves` values

See ./render.dhall for an example.
-}
let Prelude = ./Prelude.dhall

let List/map = Prelude.List.map

let Preserves = ./Type.dhall

let Preserves/dictionary = ./dictionary.dhall

let Preserves/string = ./string.dhall

let TextEntry = { mapKey : Text, mapValue : Preserves }

let stringMap
    : List TextEntry → Preserves
    = λ(xs : List TextEntry) →
        Preserves/dictionary
          ( List/map
              TextEntry
              { mapKey : Preserves, mapValue : Preserves }
              (λ(x : TextEntry) → x with mapKey = Preserves/string x.mapKey)
              xs
          )

in  stringMap
