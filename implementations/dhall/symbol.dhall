{-|
Create a Preserves symbol from a `Text` value
-}
let Preserves = ./Type.dhall

let symbol
    : Text → Preserves
    = λ(x : Text) →
      λ(Preserves : Type) →
      λ ( value
        : { boolean : Bool → Preserves
          , double : Double → Preserves
          , integer : Integer → Preserves
          , string : Text → Preserves
          , symbol : Text → Preserves
          , sequence : List Preserves → Preserves
          , set : List Preserves → Preserves
          , dictionary :
              List { mapKey : Preserves, mapValue : Preserves } → Preserves
          }
        ) →
        value.symbol x

in  symbol
