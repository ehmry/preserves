((nil . ((eval .
               (setq tide-tsserver-executable
                     (concat
                      (let ((d (dir-locals-find-file ".")))
                        (if (stringp d) d (car d)))
                      "node_modules/typescript/lib/tsserver.js"))))))
