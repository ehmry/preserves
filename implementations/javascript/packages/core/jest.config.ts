import './src/values'; // required here because it monkey-patches
                       // various globals like Array, Symbol etc., and
                       // we need the patching to be done here so the
                       // patching is consistently visible in the
                       // per-test sub-VMs.

export default {
  preset: 'ts-jest',
  testEnvironment: 'node',
};
