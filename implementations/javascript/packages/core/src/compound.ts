import type { Compound, Value } from "./values";
import type { GenericEmbedded } from "./embedded";
import { Dictionary, Set } from "./dictionary";

export function isCompound<T = GenericEmbedded>(x: Value<T>): x is Compound<T>
{
    return (Array.isArray(x) || Set.isSet(x) || Dictionary.isDictionary(x));
}
