import { Encoder, canonicalEncode, canonicalString } from "./encoder";
import { Tag } from "./constants";
import { FlexMap, FlexSet, _iterMap } from "./flex";
import { PreserveOn } from "./symbols";
import { stringify } from "./text";
import { Value } from "./values";
import { Bytes } from './bytes';
import { GenericEmbedded } from "./embedded";

export type DictionaryType = 'Dictionary' | 'Set';
export const DictionaryType = Symbol.for('DictionaryType');

export class KeyedDictionary<K extends Value<T>, V, T = GenericEmbedded> extends FlexMap<K, V> {
    get [DictionaryType](): DictionaryType {
        return 'Dictionary';
    }

    static isKeyedDictionary<K extends Value<T>, V, T = GenericEmbedded>(x: any): x is KeyedDictionary<K, V, T> {
        return x?.[DictionaryType] === 'Dictionary';
    }

    constructor(items?: readonly [K, V][]);
    constructor(items?: Iterable<readonly [K, V]>);
    constructor(items?: Iterable<readonly [K, V]>) {
        super(canonicalString, items);
    }

    mapEntries<W, S extends Value<R>, R = GenericEmbedded>(f: (entry: [K, V]) => [S, W]): KeyedDictionary<S, W, R> {
        const result = new KeyedDictionary<S, W, R>();
        for (let oldEntry of this.entries()) {
            const newEntry = f(oldEntry);
            result.set(newEntry[0], newEntry[1])
        }
        return result;
    }

    asPreservesText(): string {
        return '{' +
            Array.from(_iterMap(this.entries(), ([k, v]) =>
                stringify(k) + ': ' + stringify(v))).join(', ') +
            '}';
    }

    clone(): KeyedDictionary<K, V, T> {
        return new KeyedDictionary(this);
    }

    toString(): string {
        return this.asPreservesText();
    }

    get [Symbol.toStringTag]() { return 'Dictionary'; }

    [PreserveOn](encoder: Encoder<T>) {
        if (encoder.canonical) {
            const entries = Array.from(this);
            const pieces = entries.map<[Bytes, number]>(([k, _v], i) => [canonicalEncode(k), i]);
            pieces.sort((a, b) => Bytes.compare(a[0], b[0]));
            encoder.state.emitbyte(Tag.Dictionary);
            pieces.forEach(([_encodedKey, i]) => {
                const [k, v] = entries[i];
                encoder.push(k);
                encoder.push(v as unknown as Value<T>); // Suuuuuuuper unsound
            });
            encoder.state.emitbyte(Tag.End);
        } else {
            encoder.state.emitbyte(Tag.Dictionary);
            this.forEach((v, k) => {
                encoder.push(k);
                encoder.push(v as unknown as Value<T>); // Suuuuuuuper unsound
            });
            encoder.state.emitbyte(Tag.End);
        }
    }
}

export class Dictionary<T = GenericEmbedded, V = Value<T>> extends KeyedDictionary<Value<T>, V, T> {
    static isDictionary<T = GenericEmbedded, V = Value<T>>(x: any): x is Dictionary<T, V> {
        return x?.[DictionaryType] === 'Dictionary';
    }
}

export class KeyedSet<K extends Value<T>, T = GenericEmbedded> extends FlexSet<K> {
    get [DictionaryType](): DictionaryType {
        return 'Set';
    }

    static isKeyedSet<K extends Value<T>, T = GenericEmbedded>(x: any): x is KeyedSet<K, T> {
        return x?.[DictionaryType] === 'Set';
    }

    constructor(items?: Iterable<K>) {
        super(canonicalString, items);
    }

    map<S extends Value<R>, R = GenericEmbedded>(f: (value: K) => S): KeyedSet<S, R> {
        return new KeyedSet(_iterMap(this[Symbol.iterator](), f));
    }

    filter(f: (value: K) => boolean): KeyedSet<K, T> {
        const result = new KeyedSet<K, T>();
        for (let k of this) if (f(k)) result.add(k);
        return result;
    }

    toString(): string {
        return this.asPreservesText();
    }

    asPreservesText(): string {
        return '#{' +
            Array.from(_iterMap(this.values(), stringify)).join(', ') +
            '}';
    }

    clone(): KeyedSet<K, T> {
        return new KeyedSet(this);
    }

    get [Symbol.toStringTag]() { return 'Set'; }

    [PreserveOn](encoder: Encoder<T>) {
        if (encoder.canonical) {
            const pieces = Array.from(this).map<[Bytes, K]>(k => [canonicalEncode(k), k]);
            pieces.sort((a, b) => Bytes.compare(a[0], b[0]));
            encoder.encodevalues(Tag.Set, pieces.map(e => e[1]));
        } else {
            encoder.encodevalues(Tag.Set, this);
        }
    }
}

export class Set<T = GenericEmbedded> extends KeyedSet<Value<T>, T> {
    static isSet<T = GenericEmbedded>(x: any): x is Set<T> {
        return x?.[DictionaryType] === 'Set';
    }
}
