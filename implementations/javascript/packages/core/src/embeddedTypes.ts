import { GenericEmbedded, EmbeddedType, EmbeddedTypeDecode, EmbeddedTypeEncode } from "./embedded";
import { Encoder, EncoderState, identityEmbeddedTypeEncode } from "./encoder";
import { genericEmbeddedTypeDecode, ReaderStateOptions } from "./reader";
import { Value } from "./values";
import { DecoderState, neverEmbeddedTypeDecode } from "./decoder";

export const genericEmbeddedTypeEncode: EmbeddedTypeEncode<GenericEmbedded> = {
    encode(s: EncoderState, v: GenericEmbedded): void {
        new Encoder(s, this).push(v.generic);
    },

    toValue(v: GenericEmbedded): Value<GenericEmbedded> {
        return v.generic;
    }
};

export const genericEmbeddedType: EmbeddedType<GenericEmbedded> =
    Object.assign({},
                  genericEmbeddedTypeDecode,
                  genericEmbeddedTypeEncode);

export const neverEmbeddedTypeEncode: EmbeddedTypeEncode<never> = {
    encode(_s: EncoderState, _v: never): void {
        throw new Error("Embeddeds not permitted encoding Preserves document");
    },

    toValue(_v: never): Value<GenericEmbedded> {
        throw new Error("Embeddeds not permitted encoding Preserves document");
    }
};

export const neverEmbeddedType: EmbeddedType<never> =
    Object.assign({},
                  neverEmbeddedTypeDecode,
                  neverEmbeddedTypeEncode);

export const identityEmbeddedTypeDecode: EmbeddedTypeDecode<any> = {
    decode(_s: DecoderState): any {
        throw new Error("Cannot decode identityEmbeddedType");
    },

    fromValue(_v: Value<GenericEmbedded>, _options: ReaderStateOptions): any {
        throw new Error("Cannot decode identityEmbeddedType");
    },
};

export const identityEmbeddedType: EmbeddedType<any> =
    Object.assign({},
                  identityEmbeddedTypeDecode,
                  identityEmbeddedTypeEncode);
