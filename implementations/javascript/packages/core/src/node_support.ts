// Patching to support node.js extensions.

import { Annotated } from './annotated';
import { Bytes } from './bytes';
import { Set, Dictionary } from './dictionary';
import { Record } from './record';

import * as util from 'util';

[Bytes, Annotated, Set, Dictionary].forEach((C) => {
    (C as any).prototype[util.inspect.custom] =
        function (_depth: any, _options: any) {
            return this.asPreservesText();
        };
});

Record.fallbackToString = util.inspect;
