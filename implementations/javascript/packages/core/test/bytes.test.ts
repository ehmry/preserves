import { Bytes, decodeBase64, fromJS } from '../src/index';
import './test-utils';

describe('immutable byte arrays', () => {
    describe('Uint8Array methods', () => {
        const bs = Bytes.of(10, 20, 30, 40);
        it('should yield entries', () => {
            expect(fromJS(Array.from(bs.entries())))
                .is(fromJS([[0,10],[1,20],[2,30],[3,40]]));
        });
        it('should implement every', () => {
            expect(bs.every((b) => !(b & 1))).toBe(true);
            expect(bs.every((b) => b !== 50)).toBe(true);
            expect(!(bs.every((b) => b !== 20))).toBe(true);
        });
        it('should implement find', () => {
            expect(bs.find((b) => b > 20)).toBe(30);
            expect(bs.find((b) => b > 50)).toBe(void 0);
        });
        it('should implement findIndex', () => {
            expect(bs.findIndex((b) => b > 20)).toBe(2);
            expect(bs.findIndex((b) => b > 50)).toBe(-1);
        });
        it('should implement forEach', () => {
            const vs: number[] = [];
            bs.forEach((b) => vs.push(b));
            expect(fromJS(vs)).is(fromJS([10, 20, 30, 40]));
        });
        it('should implement includes', () => {
            expect(bs.includes(20)).toBe(true);
            expect(!bs.includes(50)).toBe(true);
        });
        it('should implement indexOf', () => {
            expect(bs.indexOf(20)).toBe(1);
            expect(bs.indexOf(50)).toBe(-1);
        });
        it('should implement join', () => {
            expect(bs.join('-')).toBe('10-20-30-40');
        });
        it('should implement keys', () => {
            expect(fromJS(Array.from(bs.keys()))).is(fromJS([0,1,2,3]));
        });
        it('should implement values', () => {
            expect(fromJS(Array.from(bs.values()))).is(fromJS([10,20,30,40]));
        });
        it('should implement filter', () => {
            expect(bs.filter((b) => b !== 30)).is(Bytes.of(10,20,40));
        });
        it('should implement slice', () => {
            const vs = bs.slice(2);
            expect(Object.is(vs._view.buffer, bs._view.buffer)).toBe(false);
            expect(vs._view.buffer.byteLength).toBe(2);
            expect(vs.get(0)).toBe(30);
            expect(vs.get(1)).toBe(40);
            expect(vs.length).toBe(2);
        });
        it('should implement subarray', () => {
            const vs = bs.subarray(2);
            expect(Object.is(vs._view.buffer, bs._view.buffer)).toBe(true);
            expect(vs._view.buffer.byteLength).toBe(4);
            expect(vs.get(0)).toBe(30);
            expect(vs.get(1)).toBe(40);
            expect(vs.length).toBe(2);
        });
        it('should implement reverse', () => {
            const vs = bs.reverse();
            expect(Object.is(vs._view.buffer, bs._view.buffer)).toBe(false);
            expect(bs.get(0)).toBe(10);
            expect(bs.get(3)).toBe(40);
            expect(vs.get(0)).toBe(40);
            expect(vs.get(3)).toBe(10);
        });
        it('should implement sort', () => {
            const vs = bs.reverse().sort();
            expect(Object.is(vs._view.buffer, bs._view.buffer)).toBe(false);
            expect(bs.get(0)).toBe(10);
            expect(bs.get(3)).toBe(40);
            expect(vs.get(0)).toBe(10);
            expect(vs.get(3)).toBe(40);
        });
    });
});

describe('base64 decoder', () => {
    describe('RFC4648 tests', () => {
        it('10.0', () => expect(decodeBase64("")).is(Bytes.of()));
        it('10.1', () => expect(decodeBase64("Zg==")).is(Bytes.of(102)));
        it('10.2', () => expect(decodeBase64("Zm8=")).is(Bytes.of(102, 111)));
        it('10.3', () => expect(decodeBase64("Zm9v")).is(Bytes.of(102, 111, 111)));
        it('10.4', () => expect(decodeBase64("Zm9vYg==")).is(Bytes.of(102, 111, 111, 98)));
        it('10.5', () => expect(decodeBase64("Zm9vYmE=")).is(Bytes.of(102, 111, 111, 98, 97)));
        it('10.6', () => expect(decodeBase64("Zm9vYmFy")).is(Bytes.of(102, 111, 111, 98, 97, 114)));

        it('10.1b', () => expect(decodeBase64("Zg")).is(Bytes.of(102)));
        it('10.2b', () => expect(decodeBase64("Zm8")).is(Bytes.of(102, 111)));
        it('10.4b', () => expect(decodeBase64("Zm9vYg")).is(Bytes.of(102, 111, 111, 98)));
        it('10.5b', () => expect(decodeBase64("Zm9vYmE")).is(Bytes.of(102, 111, 111, 98, 97)));
    });

    describe('RFC4648 examples', () => {
        it('example0', () =>
            expect(decodeBase64('FPucA9l+')).is(Bytes.of(0x14, 0xfb, 0x9c, 0x03, 0xd9, 0x7e)));
        it('example1', () =>
            expect(decodeBase64('FPucA9k=')).is(Bytes.of(0x14, 0xfb, 0x9c, 0x03, 0xd9)));
        it('example1b', () =>
            expect(decodeBase64('FPucA9k')).is(Bytes.of(0x14, 0xfb, 0x9c, 0x03, 0xd9)));
        it('example2', () =>
            expect(decodeBase64('FPucAw==')).is(Bytes.of(0x14, 0xfb, 0x9c, 0x03)));
        it('example2b', () =>
            expect(decodeBase64('FPucAw=')).is(Bytes.of(0x14, 0xfb, 0x9c, 0x03)));
        it('example2c', () =>
            expect(decodeBase64('FPucAw')).is(Bytes.of(0x14, 0xfb, 0x9c, 0x03)));
    });

    describe('Misc test cases', () => {
        it('gQ==', () => expect(decodeBase64('gQ==')).is(Bytes.of(0x81)));
    });
});
