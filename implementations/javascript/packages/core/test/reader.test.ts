import { Bytes, Decoder, genericEmbeddedType, encode, Reader } from '../src/index';
import './test-utils';

import * as fs from 'fs';

describe('reading common test suite', () => {
    const samples_bin = fs.readFileSync(__dirname + '/../../../../../tests/samples.bin');
    const samples_pr = fs.readFileSync(__dirname + '/../../../../../tests/samples.pr', 'utf-8');

    it('should read equal to decoded binary without annotations', () => {
        const s1 = new Reader(samples_pr, { embeddedDecode: genericEmbeddedType, includeAnnotations: false }).next();
        const s2 = new Decoder(samples_bin, { embeddedDecode: genericEmbeddedType, includeAnnotations: false }).next();
        expect(s1).is(s2);
    });

    it('should read equal to decoded binary with annotations', () => {
        const s1 = new Reader(samples_pr, { embeddedDecode: genericEmbeddedType, includeAnnotations: true }).next();
        const s2 = new Decoder(samples_bin, { embeddedDecode: genericEmbeddedType, includeAnnotations: true }).next();
        expect(s1).is(s2);
    });

    it('should read and encode back to binary with annotations', () => {
        const s = new Reader(samples_pr, { embeddedDecode: genericEmbeddedType, includeAnnotations: true }).next();
        const bs = Bytes.toIO(encode(s, {
            embeddedEncode: genericEmbeddedType,
            includeAnnotations: true,
            canonical: true,
        }));
        expect(bs).toEqual(new Uint8Array(samples_bin));
    });
});
