import { compile } from '../index';
import fs from 'fs';
import path from 'path';
import minimatch from 'minimatch';
import { Command } from 'commander';
import * as M from '../meta';
import chalk from 'chalk';
import { Position } from '@preserves/core';
import chokidar from 'chokidar';
import { changeExt, Diagnostic, expandInputGlob, formatFailures } from './cli-utils';

export type CommandLineArguments = {
    inputs: string[];
    base: string | undefined;
    output: string | undefined;
    stdout: boolean;
    core: string;
    watch: boolean;
    traceback: boolean;
    module: string[];
};

export type CompilationResult = {
    options: CommandLineArguments,
    inputFiles: Array<InputFile>,
    failures: Array<Diagnostic>,
    base: string,
    output: string,
};

export type InputFile = {
    inputFilePath: string,
    outputFilePath: string,
    schemaPath: M.ModulePath,
    schema: M.Schema,
};

function failureCount(type: 'warn' | 'error', r: CompilationResult): number {
    return r.failures.filter(f => f.type === type).length;
}

export function run(options: CommandLineArguments): void {
    if (!options.watch) {
        if (failureCount('error', runOnce(options)) > 0) {
            process.exit(1);
        }
    } else {
        function runWatch() {
            console.clear();
            console.log(chalk.gray(new Date().toISOString()) +
                ' Compiling Schemas in watch mode...\n');
            const r = runOnce(options);
            const warningCount = failureCount('warn', r);
            const errorCount = failureCount('error', r);
            const wMsg = (warningCount > 0) && chalk.yellowBright(`${warningCount} warning(s)`);
            const eMsg = (errorCount > 0) && chalk.redBright(`${errorCount} error(s)`);
            const errorSummary =
                (wMsg && eMsg) ? `with ${eMsg} and ${wMsg}` :
                (wMsg) ? `with ${wMsg}` :
                (eMsg) ? `with ${eMsg}` :
                chalk.greenBright('successfully');
            console.log(chalk.gray(new Date().toISOString()) +
                ` Processed ${r.inputFiles.length} file(s) ${errorSummary}. Waiting for changes.`);
            const watcher = chokidar.watch(r.base, {
                ignoreInitial: true,
            }).on('all', (_event, filename) => {
                if (options.inputs.some(i => minimatch(filename, i))) {
                    watcher.close();
                    runWatch();
                }
            });
        }
        runWatch();
    }
}

export function modulePathTo(file1: string, file2: string): string {
    let naive = path.relative(path.dirname(file1), file2);
    if (naive[0] !== '.' && naive[0] !== '/') naive = './' + naive;
    return changeExt(naive, '');
}

export function runOnce(options: CommandLineArguments): CompilationResult {
    const { base, failures, inputFiles: inputFiles0 } =
        expandInputGlob(options.inputs, options.base);
    const output = options.output ?? base;

    const extensionEnv: M.Environment = options.module.map(arg => {
        const i = arg.indexOf('=');
        if (i === -1) throw new Error(`--module argument must be Namespace=path: ${arg}`);
        const ns = arg.slice(0, i);
        const path = arg.slice(i + 1);
        return {
            schema: null,
            schemaModulePath: ns.split('.').map(Symbol.for),
            typescriptModulePath: path,
        };
    });

    const inputFiles: Array<InputFile> = inputFiles0.map(i => {
        const { inputFilePath, baseRelPath, modulePath, schema } = i;
        const outputFilePath = path.join(output, changeExt(baseRelPath, '.ts'));
        return { inputFilePath, outputFilePath, schemaPath: modulePath, schema };
    });

    inputFiles.forEach(c => {
        const env: M.Environment = [
            ... extensionEnv.flatMap(e => {
                const p = modulePathTo(c.outputFilePath, e.typescriptModulePath);
                if (p === null) return [];
                return [{... e, typescriptModulePath: p}];
            }),
            ... inputFiles.map(cc => ({
                schema: cc.schema,
                schemaModulePath: cc.schemaPath,
                typescriptModulePath: modulePathTo(c.outputFilePath, cc.outputFilePath),
            })),
        ];
        fs.mkdirSync(path.dirname(c.outputFilePath), { recursive: true });
        let compiledModule;
        try {
            compiledModule = compile(env, c.schemaPath, c.schema, {
                preservesModule: options.core,
                warn: (message: string, pos: Position | null) =>
                    failures.push({ type: 'warn', file: c.inputFilePath, detail: { message, pos } }),
            });
        } catch (e) {
            failures.push({ type: 'error', file: c.inputFilePath, detail: e });
        }
        if (compiledModule !== void 0) {
            if (options.stdout) {
                console.log('////------------------------------------------------------------');
                console.log('//// ' + c.outputFilePath);
                console.log();
                console.log(compiledModule);
            } else {
                fs.writeFileSync(c.outputFilePath, compiledModule, 'utf-8');
            }
        }
    });

    formatFailures(failures, options.traceback);

    return { options, inputFiles, failures, base, output };
}

export function main(argv: Array<string>) {
    new Command()
        .arguments('[input...]')
        .description('Compile Preserves schema definitions to TypeScript', {
            input: 'Input filename or glob',
        })
        .option('--output <directory>', 'Output directory for modules (default: next to sources)')
        .option('--stdout', 'Prints each module to stdout one after the other instead ' +
            'of writing them to files in the `--output` directory')
        .option('--base <directory>', 'Base directory for sources (default: common prefix)')
        .option('--core <path>', 'Import path for @preserves/core', '@preserves/core')
        .option('--watch', 'Watch base directory for changes')
        .option('--traceback', 'Include stack traces in compiler errors')
        .option('--module <namespace=path>', 'Additional Namespace=path import',
                (nsPath: string, previous: string[]): string[] => [... previous, nsPath],
                [])
        .action((inputs: string[], rawOptions) => {
            const options: CommandLineArguments = {
                inputs: inputs.map(i => path.normalize(i)),
                base: rawOptions.base,
                output: rawOptions.output,
                stdout: rawOptions.stdout,
                core: rawOptions.core,
                watch: rawOptions.watch,
                traceback: rawOptions.traceback,
                module: rawOptions.module,
            };
            Error.stackTraceLimit = Infinity;
            run(options);
        })
        .parse(argv, { from: 'user' });
}
