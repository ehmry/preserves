import { stringify } from "@preserves/core";
import * as M from "./meta";
import { CompilerOptions, ModuleContext } from "./compiler/context";
import { Formatter, block, seq } from "./compiler/block";
import { typeForDefinition } from "./compiler/gentype";
import { converterForDefinition } from "./compiler/genconverter";
import { Type } from "./compiler/type";
import { renderType } from "./compiler/rendertype";
import { genConstructor } from "./compiler/genctor";
import { unconverterForDefinition } from "./compiler/genunconverter";
import { sourceCodeFor } from "./compiler/value";

export function compile(env: M.Environment, schema: M.Schema, options: CompilerOptions = {}): string {
    const mod = new ModuleContext(env, schema, options);

    const embeddedName = schema.embeddedType;
    mod.defineType(seq(`export type _embedded = `,
                       renderType(embeddedName._variant === 'false'
                           ? Type.ref('any')
                           : typeForDefinition(mod.resolver(), M.Definition.Pattern(M.Pattern.SimplePattern(M.SimplePattern.Ref(embeddedName.value))))),
                       `;`));
    mod.defineType(`export type _val = _.Value<_embedded>;`);

    for (const [name, def] of schema.definitions) {
        const t = typeForDefinition(mod.resolver(), def);
        const nameStr = stringify(name);

        mod.defineType(seq(`export type ${nameStr} = `, renderType(t), `;`));

        if (t.kind === 'union') {
            mod.defineFunction(_ctx =>
                seq(`export namespace ${nameStr} `, block(
                    ... Array.from(t.variants).map(([vn, vt]) =>
                        genConstructor(vn, vn, vt, nameStr))
                )));
        } else {
            mod.defineFunction(_ctx =>
                genConstructor(nameStr, void 0, t, nameStr));
        }
    }

    for (const [name0, def] of schema.definitions) {
        const name = name0 as symbol;

        mod.defineFunction(ctx =>
            seq(`export function as${name.description!}`,
                '(v: _val): ', name.description!, ' ',
                ctx.block(() => [
                    seq(`let result = to${name.description!}(v)`),
                    seq(`if (result === void 0) `,
                        `throw new TypeError(\`Invalid ${name.description!}: \${_.stringify(v)}\`)`),
                    seq(`return result`)])));

        mod.defineFunction(ctx =>
            seq(`export function to${name.description!}`,
                '(v: _val): undefined | ', name.description!, ' ',
                ctx.block(() => [seq(`let result: undefined | `, name.description!),
                                 ... converterForDefinition(ctx, def, 'v', 'result'),
                                 seq(`return result`)])));

        mod.defineFunction(ctx =>
            seq(`export function from${name.description!}`,
                '(_v: ', name.description!, '): _val ',
                ctx.block(() => unconverterForDefinition(ctx, def, '_v'))));
    }

    const f = new Formatter();
    f.write(`import * as _ from ${JSON.stringify(options.preservesModule ?? '@preserves/core')};\n`);
    mod.imports.forEach(([identifier, path]) => {
        f.write(`import * as ${identifier} from ${JSON.stringify(path)};\n`);
    });
    f.newline();

    const sortedLiterals = Array.from(mod.literals);
    sortedLiterals.sort((a, b) => a[1] < b[1] ? -1 : a[1] === b[1] ? 0 : 1);
    for (const [lit, varname] of sortedLiterals) {
        f.write(seq(`export const ${varname} = `, sourceCodeFor(lit), `;\n`));
    }
    f.newline();

    mod.typedefs.forEach(t => {
        f.write(t);
        f.newline();
        f.newline();
    });
    f.newline();

    mod.functiondefs.forEach(p => {
        f.write(p);
        f.newline();
        f.newline();
    });

    return f.toString();
}
