import * as _ from "@preserves/core";

export const $1 = 1;
export const $Boolean = Symbol.for("Boolean");
export const $ByteString = Symbol.for("ByteString");
export const $Double = Symbol.for("Double");
export const $Float = Symbol.for("Float");
export const $SignedInteger = Symbol.for("SignedInteger");
export const $String = Symbol.for("String");
export const $Symbol = Symbol.for("Symbol");
export const $and = Symbol.for("and");
export const $any = Symbol.for("any");
export const $atom = Symbol.for("atom");
export const $bundle = Symbol.for("bundle");
export const $definitions = Symbol.for("definitions");
export const $dict = Symbol.for("dict");
export const $dictof = Symbol.for("dictof");
export const $embedded = Symbol.for("embedded");
export const $embeddedType = Symbol.for("embeddedType");
export const $lit = Symbol.for("lit");
export const $named = Symbol.for("named");
export const $or = Symbol.for("or");
export const $rec = Symbol.for("rec");
export const $ref = Symbol.for("ref");
export const $schema = Symbol.for("schema");
export const $seqof = Symbol.for("seqof");
export const $setof = Symbol.for("setof");
export const $tuple = Symbol.for("tuple");
export const $tuplePrefix = Symbol.for("tuplePrefix");
export const $version = Symbol.for("version");
export const __lit6 = false;

export type Bundle<_embedded = _.GenericEmbedded> = {"modules": Modules<_embedded>};

export type Modules<_embedded = _.GenericEmbedded> = _.KeyedDictionary<ModulePath, Schema<_embedded>, _embedded>;

export type Schema<_embedded = _.GenericEmbedded> = {
    "version": Version,
    "embeddedType": EmbeddedTypeName,
    "definitions": Definitions<_embedded>
};

export type Version = null;

export type EmbeddedTypeName = ({"_variant": "Ref", "value": Ref} | {"_variant": "false"});

export type Definitions<_embedded = _.GenericEmbedded> = _.KeyedDictionary<symbol, Definition<_embedded>, _embedded>;

export type Definition<_embedded = _.GenericEmbedded> = (
    {
        "_variant": "or",
        "pattern0": NamedAlternative<_embedded>,
        "pattern1": NamedAlternative<_embedded>,
        "patternN": Array<NamedAlternative<_embedded>>
    } |
    {
        "_variant": "and",
        "pattern0": NamedPattern<_embedded>,
        "pattern1": NamedPattern<_embedded>,
        "patternN": Array<NamedPattern<_embedded>>
    } |
    {"_variant": "Pattern", "value": Pattern<_embedded>}
);

export type Pattern<_embedded = _.GenericEmbedded> = (
    {"_variant": "SimplePattern", "value": SimplePattern<_embedded>} |
    {"_variant": "CompoundPattern", "value": CompoundPattern<_embedded>}
);

export type SimplePattern<_embedded = _.GenericEmbedded> = (
    {"_variant": "any"} |
    {"_variant": "atom", "atomKind": AtomKind} |
    {"_variant": "embedded", "interface": SimplePattern<_embedded>} |
    {"_variant": "lit", "value": _.Value<_embedded>} |
    {"_variant": "seqof", "pattern": SimplePattern<_embedded>} |
    {"_variant": "setof", "pattern": SimplePattern<_embedded>} |
    {
        "_variant": "dictof",
        "key": SimplePattern<_embedded>,
        "value": SimplePattern<_embedded>
    } |
    {"_variant": "Ref", "value": Ref}
);

export type CompoundPattern<_embedded = _.GenericEmbedded> = (
    {
        "_variant": "rec",
        "label": NamedPattern<_embedded>,
        "fields": NamedPattern<_embedded>
    } |
    {"_variant": "tuple", "patterns": Array<NamedPattern<_embedded>>} |
    {
        "_variant": "tuplePrefix",
        "fixed": Array<NamedPattern<_embedded>>,
        "variable": NamedSimplePattern<_embedded>
    } |
    {"_variant": "dict", "entries": DictionaryEntries<_embedded>}
);

export type DictionaryEntries<_embedded = _.GenericEmbedded> = _.KeyedDictionary<_.Value<_embedded>, NamedSimplePattern<_embedded>, _embedded>;

export type AtomKind = (
    {"_variant": "Boolean"} |
    {"_variant": "Float"} |
    {"_variant": "Double"} |
    {"_variant": "SignedInteger"} |
    {"_variant": "String"} |
    {"_variant": "ByteString"} |
    {"_variant": "Symbol"}
);

export type NamedAlternative<_embedded = _.GenericEmbedded> = {"variantLabel": string, "pattern": Pattern<_embedded>};

export type NamedSimplePattern<_embedded = _.GenericEmbedded> = (
    {"_variant": "named", "value": Binding<_embedded>} |
    {"_variant": "anonymous", "value": SimplePattern<_embedded>}
);

export type NamedPattern<_embedded = _.GenericEmbedded> = (
    {"_variant": "named", "value": Binding<_embedded>} |
    {"_variant": "anonymous", "value": Pattern<_embedded>}
);

export type Binding<_embedded = _.GenericEmbedded> = {"name": symbol, "pattern": SimplePattern<_embedded>};

export type Ref = {"module": ModulePath, "name": symbol};

export type ModulePath = Array<symbol>;


export function Bundle<_embedded = _.GenericEmbedded>(modules: Modules<_embedded>): Bundle<_embedded> {return {"modules": modules};}

export function Modules<_embedded = _.GenericEmbedded>(value: _.KeyedDictionary<ModulePath, Schema<_embedded>, _embedded>): Modules<_embedded> {return value;}

export function Schema<_embedded = _.GenericEmbedded>(
    {version, embeddedType, definitions}: {
        version: Version,
        embeddedType: EmbeddedTypeName,
        definitions: Definitions<_embedded>
    }
): Schema<_embedded> {
    return {"version": version, "embeddedType": embeddedType, "definitions": definitions};
}

export function Version(): Version {return null;}

export namespace EmbeddedTypeName {
    export function Ref(value: Ref): EmbeddedTypeName {return {"_variant": "Ref", "value": value};};
    export function $false(): EmbeddedTypeName {return {"_variant": "false"};};
}

export function Definitions<_embedded = _.GenericEmbedded>(value: _.KeyedDictionary<symbol, Definition<_embedded>, _embedded>): Definitions<_embedded> {return value;}

export namespace Definition {
    export function or<_embedded = _.GenericEmbedded>(
        {pattern0, pattern1, patternN}: {
            pattern0: NamedAlternative<_embedded>,
            pattern1: NamedAlternative<_embedded>,
            patternN: Array<NamedAlternative<_embedded>>
        }
    ): Definition<_embedded> {
        return {
            "_variant": "or",
            "pattern0": pattern0,
            "pattern1": pattern1,
            "patternN": patternN
        };
    };
    export function and<_embedded = _.GenericEmbedded>(
        {pattern0, pattern1, patternN}: {
            pattern0: NamedPattern<_embedded>,
            pattern1: NamedPattern<_embedded>,
            patternN: Array<NamedPattern<_embedded>>
        }
    ): Definition<_embedded> {
        return {
            "_variant": "and",
            "pattern0": pattern0,
            "pattern1": pattern1,
            "patternN": patternN
        };
    };
    export function Pattern<_embedded = _.GenericEmbedded>(value: Pattern<_embedded>): Definition<_embedded> {return {"_variant": "Pattern", "value": value};};
}

export namespace Pattern {
    export function SimplePattern<_embedded = _.GenericEmbedded>(value: SimplePattern<_embedded>): Pattern<_embedded> {return {"_variant": "SimplePattern", "value": value};};
    export function CompoundPattern<_embedded = _.GenericEmbedded>(value: CompoundPattern<_embedded>): Pattern<_embedded> {return {"_variant": "CompoundPattern", "value": value};};
}

export namespace SimplePattern {
    export function any<_embedded = _.GenericEmbedded>(): SimplePattern<_embedded> {return {"_variant": "any"};};
    export function atom<_embedded = _.GenericEmbedded>(atomKind: AtomKind): SimplePattern<_embedded> {return {"_variant": "atom", "atomKind": atomKind};};
    export function embedded<_embedded = _.GenericEmbedded>($interface: SimplePattern<_embedded>): SimplePattern<_embedded> {return {"_variant": "embedded", "interface": $interface};};
    export function lit<_embedded = _.GenericEmbedded>(value: _.Value<_embedded>): SimplePattern<_embedded> {return {"_variant": "lit", "value": value};};
    export function seqof<_embedded = _.GenericEmbedded>(pattern: SimplePattern<_embedded>): SimplePattern<_embedded> {return {"_variant": "seqof", "pattern": pattern};};
    export function setof<_embedded = _.GenericEmbedded>(pattern: SimplePattern<_embedded>): SimplePattern<_embedded> {return {"_variant": "setof", "pattern": pattern};};
    export function dictof<_embedded = _.GenericEmbedded>({key, value}: {key: SimplePattern<_embedded>, value: SimplePattern<_embedded>}): SimplePattern<_embedded> {return {"_variant": "dictof", "key": key, "value": value};};
    export function Ref<_embedded = _.GenericEmbedded>(value: Ref): SimplePattern<_embedded> {return {"_variant": "Ref", "value": value};};
}

export namespace CompoundPattern {
    export function rec<_embedded = _.GenericEmbedded>(
        {label, fields}: {label: NamedPattern<_embedded>, fields: NamedPattern<_embedded>}
    ): CompoundPattern<_embedded> {return {"_variant": "rec", "label": label, "fields": fields};};
    export function tuple<_embedded = _.GenericEmbedded>(patterns: Array<NamedPattern<_embedded>>): CompoundPattern<_embedded> {return {"_variant": "tuple", "patterns": patterns};};
    export function tuplePrefix<_embedded = _.GenericEmbedded>(
        {fixed, variable}: {fixed: Array<NamedPattern<_embedded>>, variable: NamedSimplePattern<_embedded>}
    ): CompoundPattern<_embedded> {return {"_variant": "tuplePrefix", "fixed": fixed, "variable": variable};};
    export function dict<_embedded = _.GenericEmbedded>(entries: DictionaryEntries<_embedded>): CompoundPattern<_embedded> {return {"_variant": "dict", "entries": entries};};
}

export function DictionaryEntries<_embedded = _.GenericEmbedded>(
    value: _.KeyedDictionary<_.Value<_embedded>, NamedSimplePattern<_embedded>, _embedded>
): DictionaryEntries<_embedded> {return value;}

export namespace AtomKind {
    export function Boolean(): AtomKind {return {"_variant": "Boolean"};};
    export function Float(): AtomKind {return {"_variant": "Float"};};
    export function Double(): AtomKind {return {"_variant": "Double"};};
    export function SignedInteger(): AtomKind {return {"_variant": "SignedInteger"};};
    export function String(): AtomKind {return {"_variant": "String"};};
    export function ByteString(): AtomKind {return {"_variant": "ByteString"};};
    export function Symbol(): AtomKind {return {"_variant": "Symbol"};};
}

export function NamedAlternative<_embedded = _.GenericEmbedded>({variantLabel, pattern}: {variantLabel: string, pattern: Pattern<_embedded>}): NamedAlternative<_embedded> {return {"variantLabel": variantLabel, "pattern": pattern};}

export namespace NamedSimplePattern {
    export function named<_embedded = _.GenericEmbedded>(value: Binding<_embedded>): NamedSimplePattern<_embedded> {return {"_variant": "named", "value": value};};
    export function anonymous<_embedded = _.GenericEmbedded>(value: SimplePattern<_embedded>): NamedSimplePattern<_embedded> {return {"_variant": "anonymous", "value": value};};
}

export namespace NamedPattern {
    export function named<_embedded = _.GenericEmbedded>(value: Binding<_embedded>): NamedPattern<_embedded> {return {"_variant": "named", "value": value};};
    export function anonymous<_embedded = _.GenericEmbedded>(value: Pattern<_embedded>): NamedPattern<_embedded> {return {"_variant": "anonymous", "value": value};};
}

export function Binding<_embedded = _.GenericEmbedded>({name, pattern}: {name: symbol, pattern: SimplePattern<_embedded>}): Binding<_embedded> {return {"name": name, "pattern": pattern};}

export function Ref({module, name}: {module: ModulePath, name: symbol}): Ref {return {"module": module, "name": name};}

export function ModulePath(value: Array<symbol>): ModulePath {return value;}

export function asBundle<_embedded = _.GenericEmbedded>(v: _.Value<_embedded>): Bundle<_embedded> {
    let result = toBundle(v);
    if (result === void 0) throw new TypeError(`Invalid Bundle: ${_.stringify(v)}`);
    return result;
}

export function toBundle<_embedded = _.GenericEmbedded>(v: _.Value<_embedded>): undefined | Bundle<_embedded> {
    let result: undefined | Bundle<_embedded>;
    if (_.Record.isRecord<_.Value<_embedded>, _.Tuple<_.Value<_embedded>>, _embedded>(v)) {
        let _tmp0: (null) | undefined;
        _tmp0 = _.is(v.label, $bundle) ? null : void 0;
        if (_tmp0 !== void 0) {
            let _tmp1: (Modules<_embedded>) | undefined;
            _tmp1 = toModules(v[0]);
            if (_tmp1 !== void 0) {result = {"modules": _tmp1};};
        };
    };
    return result;
}

export function fromBundle<_embedded = _.GenericEmbedded>(_v: Bundle<_embedded>): _.Value<_embedded> {return _.Record($bundle, [fromModules<_embedded>(_v["modules"])]);}

export function asModules<_embedded = _.GenericEmbedded>(v: _.Value<_embedded>): Modules<_embedded> {
    let result = toModules(v);
    if (result === void 0) throw new TypeError(`Invalid Modules: ${_.stringify(v)}`);
    return result;
}

export function toModules<_embedded = _.GenericEmbedded>(v: _.Value<_embedded>): undefined | Modules<_embedded> {
    let _tmp0: (_.KeyedDictionary<ModulePath, Schema<_embedded>, _embedded>) | undefined;
    let result: undefined | Modules<_embedded>;
    _tmp0 = void 0;
    if (_.Dictionary.isDictionary<_embedded>(v)) {
        _tmp0 = new _.KeyedDictionary();
        for (const [_tmp2, _tmp1] of v) {
            let _tmp3: (ModulePath) | undefined;
            _tmp3 = toModulePath(_tmp2);
            if (_tmp3 !== void 0) {
                let _tmp4: (Schema<_embedded>) | undefined;
                _tmp4 = toSchema(_tmp1);
                if (_tmp4 !== void 0) {_tmp0.set(_tmp3, _tmp4); continue;};
            };
            _tmp0 = void 0;
            break;
        };
    };
    if (_tmp0 !== void 0) {result = _tmp0;};
    return result;
}

export function fromModules<_embedded = _.GenericEmbedded>(_v: Modules<_embedded>): _.Value<_embedded> {
    return new _.Dictionary<_embedded>(
        _.Array.from(_v.entries()).map(([k, v]) => [fromModulePath<_embedded>(k), fromSchema<_embedded>(v)])
    );
}

export function asSchema<_embedded = _.GenericEmbedded>(v: _.Value<_embedded>): Schema<_embedded> {
    let result = toSchema(v);
    if (result === void 0) throw new TypeError(`Invalid Schema: ${_.stringify(v)}`);
    return result;
}

export function toSchema<_embedded = _.GenericEmbedded>(v: _.Value<_embedded>): undefined | Schema<_embedded> {
    let result: undefined | Schema<_embedded>;
    if (_.Record.isRecord<_.Value<_embedded>, _.Tuple<_.Value<_embedded>>, _embedded>(v)) {
        let _tmp0: (null) | undefined;
        _tmp0 = _.is(v.label, $schema) ? null : void 0;
        if (_tmp0 !== void 0) {
            if (_.Dictionary.isDictionary<_embedded>(v[0])) {
                let _tmp1: (_.Value<_embedded>) | undefined;
                if ((_tmp1 = v[0].get($version)) !== void 0) {
                    let _tmp2: (Version) | undefined;
                    _tmp2 = toVersion(_tmp1);
                    if (_tmp2 !== void 0) {
                        let _tmp3: (_.Value<_embedded>) | undefined;
                        if ((_tmp3 = v[0].get($embeddedType)) !== void 0) {
                            let _tmp4: (EmbeddedTypeName) | undefined;
                            _tmp4 = toEmbeddedTypeName(_tmp3);
                            if (_tmp4 !== void 0) {
                                let _tmp5: (_.Value<_embedded>) | undefined;
                                if ((_tmp5 = v[0].get($definitions)) !== void 0) {
                                    let _tmp6: (Definitions<_embedded>) | undefined;
                                    _tmp6 = toDefinitions(_tmp5);
                                    if (_tmp6 !== void 0) {result = {"version": _tmp2, "embeddedType": _tmp4, "definitions": _tmp6};};
                                };
                            };
                        };
                    };
                };
            };
        };
    };
    return result;
}

export function fromSchema<_embedded = _.GenericEmbedded>(_v: Schema<_embedded>): _.Value<_embedded> {
    return _.Record(
        $schema,
        [
            new _.Dictionary<_embedded>(
                [
                    [$version, fromVersion<_embedded>(_v["version"])],
                    [$embeddedType, fromEmbeddedTypeName<_embedded>(_v["embeddedType"])],
                    [$definitions, fromDefinitions<_embedded>(_v["definitions"])]
                ]
            )
        ]
    );
}

export function asVersion<_embedded = _.GenericEmbedded>(v: _.Value<_embedded>): Version {
    let result = toVersion(v);
    if (result === void 0) throw new TypeError(`Invalid Version: ${_.stringify(v)}`);
    return result;
}

export function toVersion<_embedded = _.GenericEmbedded>(v: _.Value<_embedded>): undefined | Version {
    let _tmp0: (null) | undefined;
    let result: undefined | Version;
    _tmp0 = _.is(v, $1) ? null : void 0;
    if (_tmp0 !== void 0) {result = _tmp0;};
    return result;
}

export function fromVersion<_embedded = _.GenericEmbedded>(_v: Version): _.Value<_embedded> {return $1;}

export function asEmbeddedTypeName<_embedded = _.GenericEmbedded>(v: _.Value<_embedded>): EmbeddedTypeName {
    let result = toEmbeddedTypeName(v);
    if (result === void 0) throw new TypeError(`Invalid EmbeddedTypeName: ${_.stringify(v)}`);
    return result;
}

export function toEmbeddedTypeName<_embedded = _.GenericEmbedded>(v: _.Value<_embedded>): undefined | EmbeddedTypeName {
    let _tmp0: (Ref) | undefined;
    let result: undefined | EmbeddedTypeName;
    _tmp0 = toRef(v);
    if (_tmp0 !== void 0) {result = {"_variant": "Ref", "value": _tmp0};};
    if (result === void 0) {
        let _tmp1: (null) | undefined;
        _tmp1 = _.is(v, __lit6) ? null : void 0;
        if (_tmp1 !== void 0) {result = {"_variant": "false"};};
    };
    return result;
}

export function fromEmbeddedTypeName<_embedded = _.GenericEmbedded>(_v: EmbeddedTypeName): _.Value<_embedded> {
    switch (_v._variant) {
        case "Ref": {return fromRef<_embedded>(_v.value);};
        case "false": {return __lit6;};
    };
}

export function asDefinitions<_embedded = _.GenericEmbedded>(v: _.Value<_embedded>): Definitions<_embedded> {
    let result = toDefinitions(v);
    if (result === void 0) throw new TypeError(`Invalid Definitions: ${_.stringify(v)}`);
    return result;
}

export function toDefinitions<_embedded = _.GenericEmbedded>(v: _.Value<_embedded>): undefined | Definitions<_embedded> {
    let _tmp0: (_.KeyedDictionary<symbol, Definition<_embedded>, _embedded>) | undefined;
    let result: undefined | Definitions<_embedded>;
    _tmp0 = void 0;
    if (_.Dictionary.isDictionary<_embedded>(v)) {
        _tmp0 = new _.KeyedDictionary();
        for (const [_tmp2, _tmp1] of v) {
            let _tmp3: (symbol) | undefined;
            _tmp3 = typeof _tmp2 === 'symbol' ? _tmp2 : void 0;
            if (_tmp3 !== void 0) {
                let _tmp4: (Definition<_embedded>) | undefined;
                _tmp4 = toDefinition(_tmp1);
                if (_tmp4 !== void 0) {_tmp0.set(_tmp3, _tmp4); continue;};
            };
            _tmp0 = void 0;
            break;
        };
    };
    if (_tmp0 !== void 0) {result = _tmp0;};
    return result;
}

export function fromDefinitions<_embedded = _.GenericEmbedded>(_v: Definitions<_embedded>): _.Value<_embedded> {
    return new _.Dictionary<_embedded>(_.Array.from(_v.entries()).map(([k, v]) => [k, fromDefinition<_embedded>(v)]));
}

export function asDefinition<_embedded = _.GenericEmbedded>(v: _.Value<_embedded>): Definition<_embedded> {
    let result = toDefinition(v);
    if (result === void 0) throw new TypeError(`Invalid Definition: ${_.stringify(v)}`);
    return result;
}

export function toDefinition<_embedded = _.GenericEmbedded>(v: _.Value<_embedded>): undefined | Definition<_embedded> {
    let result: undefined | Definition<_embedded>;
    if (_.Record.isRecord<_.Value<_embedded>, _.Tuple<_.Value<_embedded>>, _embedded>(v)) {
        let _tmp0: (null) | undefined;
        _tmp0 = _.is(v.label, $or) ? null : void 0;
        if (_tmp0 !== void 0) {
            if (_.Array.isArray(v[0]) && v[0].length >= 2) {
                let _tmp1: (NamedAlternative<_embedded>) | undefined;
                _tmp1 = toNamedAlternative(v[0][0]);
                if (_tmp1 !== void 0) {
                    let _tmp2: (NamedAlternative<_embedded>) | undefined;
                    _tmp2 = toNamedAlternative(v[0][1]);
                    if (_tmp2 !== void 0) {
                        let _tmp3: (Array<_.Value<_embedded>>) | undefined;
                        let _tmp4: (Array<NamedAlternative<_embedded>>) | undefined;
                        _tmp3 = v[0].slice(2);
                        _tmp4 = [];
                        for (const _tmp5 of _tmp3) {
                            let _tmp6: (NamedAlternative<_embedded>) | undefined;
                            _tmp6 = toNamedAlternative(_tmp5);
                            if (_tmp6 !== void 0) {_tmp4.push(_tmp6); continue;};
                            _tmp4 = void 0;
                            break;
                        };
                        if (_tmp4 !== void 0) {
                            result = {"_variant": "or", "pattern0": _tmp1, "pattern1": _tmp2, "patternN": _tmp4};
                        };
                    };
                };
            };
        };
    };
    if (result === void 0) {
        if (_.Record.isRecord<_.Value<_embedded>, _.Tuple<_.Value<_embedded>>, _embedded>(v)) {
            let _tmp7: (null) | undefined;
            _tmp7 = _.is(v.label, $and) ? null : void 0;
            if (_tmp7 !== void 0) {
                if (_.Array.isArray(v[0]) && v[0].length >= 2) {
                    let _tmp8: (NamedPattern<_embedded>) | undefined;
                    _tmp8 = toNamedPattern(v[0][0]);
                    if (_tmp8 !== void 0) {
                        let _tmp9: (NamedPattern<_embedded>) | undefined;
                        _tmp9 = toNamedPattern(v[0][1]);
                        if (_tmp9 !== void 0) {
                            let _tmp10: (Array<_.Value<_embedded>>) | undefined;
                            let _tmp11: (Array<NamedPattern<_embedded>>) | undefined;
                            _tmp10 = v[0].slice(2);
                            _tmp11 = [];
                            for (const _tmp12 of _tmp10) {
                                let _tmp13: (NamedPattern<_embedded>) | undefined;
                                _tmp13 = toNamedPattern(_tmp12);
                                if (_tmp13 !== void 0) {_tmp11.push(_tmp13); continue;};
                                _tmp11 = void 0;
                                break;
                            };
                            if (_tmp11 !== void 0) {
                                result = {"_variant": "and", "pattern0": _tmp8, "pattern1": _tmp9, "patternN": _tmp11};
                            };
                        };
                    };
                };
            };
        };
        if (result === void 0) {
            let _tmp14: (Pattern<_embedded>) | undefined;
            _tmp14 = toPattern(v);
            if (_tmp14 !== void 0) {result = {"_variant": "Pattern", "value": _tmp14};};
        };
    };
    return result;
}

export function fromDefinition<_embedded = _.GenericEmbedded>(_v: Definition<_embedded>): _.Value<_embedded> {
    switch (_v._variant) {
        case "or": {
            return _.Record(
                $or,
                [
                    [
                        fromNamedAlternative<_embedded>(_v["pattern0"]),
                        fromNamedAlternative<_embedded>(_v["pattern1"]),
                        ... _v["patternN"].map(v => fromNamedAlternative<_embedded>(v))
                    ]
                ]
            );
        };
        case "and": {
            return _.Record(
                $and,
                [
                    [
                        fromNamedPattern<_embedded>(_v["pattern0"]),
                        fromNamedPattern<_embedded>(_v["pattern1"]),
                        ... _v["patternN"].map(v => fromNamedPattern<_embedded>(v))
                    ]
                ]
            );
        };
        case "Pattern": {return fromPattern<_embedded>(_v.value);};
    };
}

export function asPattern<_embedded = _.GenericEmbedded>(v: _.Value<_embedded>): Pattern<_embedded> {
    let result = toPattern(v);
    if (result === void 0) throw new TypeError(`Invalid Pattern: ${_.stringify(v)}`);
    return result;
}

export function toPattern<_embedded = _.GenericEmbedded>(v: _.Value<_embedded>): undefined | Pattern<_embedded> {
    let _tmp0: (SimplePattern<_embedded>) | undefined;
    let result: undefined | Pattern<_embedded>;
    _tmp0 = toSimplePattern(v);
    if (_tmp0 !== void 0) {result = {"_variant": "SimplePattern", "value": _tmp0};};
    if (result === void 0) {
        let _tmp1: (CompoundPattern<_embedded>) | undefined;
        _tmp1 = toCompoundPattern(v);
        if (_tmp1 !== void 0) {result = {"_variant": "CompoundPattern", "value": _tmp1};};
    };
    return result;
}

export function fromPattern<_embedded = _.GenericEmbedded>(_v: Pattern<_embedded>): _.Value<_embedded> {
    switch (_v._variant) {
        case "SimplePattern": {return fromSimplePattern<_embedded>(_v.value);};
        case "CompoundPattern": {return fromCompoundPattern<_embedded>(_v.value);};
    };
}

export function asSimplePattern<_embedded = _.GenericEmbedded>(v: _.Value<_embedded>): SimplePattern<_embedded> {
    let result = toSimplePattern(v);
    if (result === void 0) throw new TypeError(`Invalid SimplePattern: ${_.stringify(v)}`);
    return result;
}

export function toSimplePattern<_embedded = _.GenericEmbedded>(v: _.Value<_embedded>): undefined | SimplePattern<_embedded> {
    let _tmp0: (null) | undefined;
    let result: undefined | SimplePattern<_embedded>;
    _tmp0 = _.is(v, $any) ? null : void 0;
    if (_tmp0 !== void 0) {result = {"_variant": "any"};};
    if (result === void 0) {
        if (_.Record.isRecord<_.Value<_embedded>, _.Tuple<_.Value<_embedded>>, _embedded>(v)) {
            let _tmp1: (null) | undefined;
            _tmp1 = _.is(v.label, $atom) ? null : void 0;
            if (_tmp1 !== void 0) {
                let _tmp2: (AtomKind) | undefined;
                _tmp2 = toAtomKind(v[0]);
                if (_tmp2 !== void 0) {result = {"_variant": "atom", "atomKind": _tmp2};};
            };
        };
        if (result === void 0) {
            if (_.Record.isRecord<_.Value<_embedded>, _.Tuple<_.Value<_embedded>>, _embedded>(v)) {
                let _tmp3: (null) | undefined;
                _tmp3 = _.is(v.label, $embedded) ? null : void 0;
                if (_tmp3 !== void 0) {
                    let _tmp4: (SimplePattern<_embedded>) | undefined;
                    _tmp4 = toSimplePattern(v[0]);
                    if (_tmp4 !== void 0) {result = {"_variant": "embedded", "interface": _tmp4};};
                };
            };
            if (result === void 0) {
                if (_.Record.isRecord<_.Value<_embedded>, _.Tuple<_.Value<_embedded>>, _embedded>(v)) {
                    let _tmp5: (null) | undefined;
                    _tmp5 = _.is(v.label, $lit) ? null : void 0;
                    if (_tmp5 !== void 0) {
                        let _tmp6: (_.Value<_embedded>) | undefined;
                        _tmp6 = v[0];
                        if (_tmp6 !== void 0) {result = {"_variant": "lit", "value": _tmp6};};
                    };
                };
                if (result === void 0) {
                    if (_.Record.isRecord<_.Value<_embedded>, _.Tuple<_.Value<_embedded>>, _embedded>(v)) {
                        let _tmp7: (null) | undefined;
                        _tmp7 = _.is(v.label, $seqof) ? null : void 0;
                        if (_tmp7 !== void 0) {
                            let _tmp8: (SimplePattern<_embedded>) | undefined;
                            _tmp8 = toSimplePattern(v[0]);
                            if (_tmp8 !== void 0) {result = {"_variant": "seqof", "pattern": _tmp8};};
                        };
                    };
                    if (result === void 0) {
                        if (_.Record.isRecord<_.Value<_embedded>, _.Tuple<_.Value<_embedded>>, _embedded>(v)) {
                            let _tmp9: (null) | undefined;
                            _tmp9 = _.is(v.label, $setof) ? null : void 0;
                            if (_tmp9 !== void 0) {
                                let _tmp10: (SimplePattern<_embedded>) | undefined;
                                _tmp10 = toSimplePattern(v[0]);
                                if (_tmp10 !== void 0) {result = {"_variant": "setof", "pattern": _tmp10};};
                            };
                        };
                        if (result === void 0) {
                            if (_.Record.isRecord<_.Value<_embedded>, _.Tuple<_.Value<_embedded>>, _embedded>(v)) {
                                let _tmp11: (null) | undefined;
                                _tmp11 = _.is(v.label, $dictof) ? null : void 0;
                                if (_tmp11 !== void 0) {
                                    let _tmp12: (SimplePattern<_embedded>) | undefined;
                                    _tmp12 = toSimplePattern(v[0]);
                                    if (_tmp12 !== void 0) {
                                        let _tmp13: (SimplePattern<_embedded>) | undefined;
                                        _tmp13 = toSimplePattern(v[1]);
                                        if (_tmp13 !== void 0) {result = {"_variant": "dictof", "key": _tmp12, "value": _tmp13};};
                                    };
                                };
                            };
                            if (result === void 0) {
                                let _tmp14: (Ref) | undefined;
                                _tmp14 = toRef(v);
                                if (_tmp14 !== void 0) {result = {"_variant": "Ref", "value": _tmp14};};
                            };
                        };
                    };
                };
            };
        };
    };
    return result;
}

export function fromSimplePattern<_embedded = _.GenericEmbedded>(_v: SimplePattern<_embedded>): _.Value<_embedded> {
    switch (_v._variant) {
        case "any": {return $any;};
        case "atom": {return _.Record($atom, [fromAtomKind<_embedded>(_v["atomKind"])]);};
        case "embedded": {return _.Record($embedded, [fromSimplePattern<_embedded>(_v["interface"])]);};
        case "lit": {return _.Record($lit, [_v["value"]]);};
        case "seqof": {return _.Record($seqof, [fromSimplePattern<_embedded>(_v["pattern"])]);};
        case "setof": {return _.Record($setof, [fromSimplePattern<_embedded>(_v["pattern"])]);};
        case "dictof": {
            return _.Record(
                $dictof,
                [
                    fromSimplePattern<_embedded>(_v["key"]),
                    fromSimplePattern<_embedded>(_v["value"])
                ]
            );
        };
        case "Ref": {return fromRef<_embedded>(_v.value);};
    };
}

export function asCompoundPattern<_embedded = _.GenericEmbedded>(v: _.Value<_embedded>): CompoundPattern<_embedded> {
    let result = toCompoundPattern(v);
    if (result === void 0) throw new TypeError(`Invalid CompoundPattern: ${_.stringify(v)}`);
    return result;
}

export function toCompoundPattern<_embedded = _.GenericEmbedded>(v: _.Value<_embedded>): undefined | CompoundPattern<_embedded> {
    let result: undefined | CompoundPattern<_embedded>;
    if (_.Record.isRecord<_.Value<_embedded>, _.Tuple<_.Value<_embedded>>, _embedded>(v)) {
        let _tmp0: (null) | undefined;
        _tmp0 = _.is(v.label, $rec) ? null : void 0;
        if (_tmp0 !== void 0) {
            let _tmp1: (NamedPattern<_embedded>) | undefined;
            _tmp1 = toNamedPattern(v[0]);
            if (_tmp1 !== void 0) {
                let _tmp2: (NamedPattern<_embedded>) | undefined;
                _tmp2 = toNamedPattern(v[1]);
                if (_tmp2 !== void 0) {result = {"_variant": "rec", "label": _tmp1, "fields": _tmp2};};
            };
        };
    };
    if (result === void 0) {
        if (_.Record.isRecord<_.Value<_embedded>, _.Tuple<_.Value<_embedded>>, _embedded>(v)) {
            let _tmp3: (null) | undefined;
            _tmp3 = _.is(v.label, $tuple) ? null : void 0;
            if (_tmp3 !== void 0) {
                let _tmp4: (Array<NamedPattern<_embedded>>) | undefined;
                _tmp4 = void 0;
                if (_.Array.isArray(v[0])) {
                    _tmp4 = [];
                    for (const _tmp5 of v[0]) {
                        let _tmp6: (NamedPattern<_embedded>) | undefined;
                        _tmp6 = toNamedPattern(_tmp5);
                        if (_tmp6 !== void 0) {_tmp4.push(_tmp6); continue;};
                        _tmp4 = void 0;
                        break;
                    };
                };
                if (_tmp4 !== void 0) {result = {"_variant": "tuple", "patterns": _tmp4};};
            };
        };
        if (result === void 0) {
            if (_.Record.isRecord<_.Value<_embedded>, _.Tuple<_.Value<_embedded>>, _embedded>(v)) {
                let _tmp7: (null) | undefined;
                _tmp7 = _.is(v.label, $tuplePrefix) ? null : void 0;
                if (_tmp7 !== void 0) {
                    let _tmp8: (Array<NamedPattern<_embedded>>) | undefined;
                    _tmp8 = void 0;
                    if (_.Array.isArray(v[0])) {
                        _tmp8 = [];
                        for (const _tmp9 of v[0]) {
                            let _tmp10: (NamedPattern<_embedded>) | undefined;
                            _tmp10 = toNamedPattern(_tmp9);
                            if (_tmp10 !== void 0) {_tmp8.push(_tmp10); continue;};
                            _tmp8 = void 0;
                            break;
                        };
                    };
                    if (_tmp8 !== void 0) {
                        let _tmp11: (NamedSimplePattern<_embedded>) | undefined;
                        _tmp11 = toNamedSimplePattern(v[1]);
                        if (_tmp11 !== void 0) {result = {"_variant": "tuplePrefix", "fixed": _tmp8, "variable": _tmp11};};
                    };
                };
            };
            if (result === void 0) {
                if (_.Record.isRecord<_.Value<_embedded>, _.Tuple<_.Value<_embedded>>, _embedded>(v)) {
                    let _tmp12: (null) | undefined;
                    _tmp12 = _.is(v.label, $dict) ? null : void 0;
                    if (_tmp12 !== void 0) {
                        let _tmp13: (DictionaryEntries<_embedded>) | undefined;
                        _tmp13 = toDictionaryEntries(v[0]);
                        if (_tmp13 !== void 0) {result = {"_variant": "dict", "entries": _tmp13};};
                    };
                };
            };
        };
    };
    return result;
}

export function fromCompoundPattern<_embedded = _.GenericEmbedded>(_v: CompoundPattern<_embedded>): _.Value<_embedded> {
    switch (_v._variant) {
        case "rec": {
            return _.Record(
                $rec,
                [
                    fromNamedPattern<_embedded>(_v["label"]),
                    fromNamedPattern<_embedded>(_v["fields"])
                ]
            );
        };
        case "tuple": {
            return _.Record($tuple, [_v["patterns"].map(v => fromNamedPattern<_embedded>(v))]);
        };
        case "tuplePrefix": {
            return _.Record(
                $tuplePrefix,
                [
                    _v["fixed"].map(v => fromNamedPattern<_embedded>(v)),
                    fromNamedSimplePattern<_embedded>(_v["variable"])
                ]
            );
        };
        case "dict": {return _.Record($dict, [fromDictionaryEntries<_embedded>(_v["entries"])]);};
    };
}

export function asDictionaryEntries<_embedded = _.GenericEmbedded>(v: _.Value<_embedded>): DictionaryEntries<_embedded> {
    let result = toDictionaryEntries(v);
    if (result === void 0) throw new TypeError(`Invalid DictionaryEntries: ${_.stringify(v)}`);
    return result;
}

export function toDictionaryEntries<_embedded = _.GenericEmbedded>(v: _.Value<_embedded>): undefined | DictionaryEntries<_embedded> {
    let _tmp0: (_.KeyedDictionary<_.Value<_embedded>, NamedSimplePattern<_embedded>, _embedded>) | undefined;
    let result: undefined | DictionaryEntries<_embedded>;
    _tmp0 = void 0;
    if (_.Dictionary.isDictionary<_embedded>(v)) {
        _tmp0 = new _.KeyedDictionary();
        for (const [_tmp2, _tmp1] of v) {
            let _tmp3: (_.Value<_embedded>) | undefined;
            _tmp3 = _tmp2;
            if (_tmp3 !== void 0) {
                let _tmp4: (NamedSimplePattern<_embedded>) | undefined;
                _tmp4 = toNamedSimplePattern(_tmp1);
                if (_tmp4 !== void 0) {_tmp0.set(_tmp3, _tmp4); continue;};
            };
            _tmp0 = void 0;
            break;
        };
    };
    if (_tmp0 !== void 0) {result = _tmp0;};
    return result;
}

export function fromDictionaryEntries<_embedded = _.GenericEmbedded>(_v: DictionaryEntries<_embedded>): _.Value<_embedded> {
    return new _.Dictionary<_embedded>(
        _.Array.from(_v.entries()).map(([k, v]) => [k, fromNamedSimplePattern<_embedded>(v)])
    );
}

export function asAtomKind<_embedded = _.GenericEmbedded>(v: _.Value<_embedded>): AtomKind {
    let result = toAtomKind(v);
    if (result === void 0) throw new TypeError(`Invalid AtomKind: ${_.stringify(v)}`);
    return result;
}

export function toAtomKind<_embedded = _.GenericEmbedded>(v: _.Value<_embedded>): undefined | AtomKind {
    let _tmp0: (null) | undefined;
    let result: undefined | AtomKind;
    _tmp0 = _.is(v, $Boolean) ? null : void 0;
    if (_tmp0 !== void 0) {result = {"_variant": "Boolean"};};
    if (result === void 0) {
        let _tmp1: (null) | undefined;
        _tmp1 = _.is(v, $Float) ? null : void 0;
        if (_tmp1 !== void 0) {result = {"_variant": "Float"};};
        if (result === void 0) {
            let _tmp2: (null) | undefined;
            _tmp2 = _.is(v, $Double) ? null : void 0;
            if (_tmp2 !== void 0) {result = {"_variant": "Double"};};
            if (result === void 0) {
                let _tmp3: (null) | undefined;
                _tmp3 = _.is(v, $SignedInteger) ? null : void 0;
                if (_tmp3 !== void 0) {result = {"_variant": "SignedInteger"};};
                if (result === void 0) {
                    let _tmp4: (null) | undefined;
                    _tmp4 = _.is(v, $String) ? null : void 0;
                    if (_tmp4 !== void 0) {result = {"_variant": "String"};};
                    if (result === void 0) {
                        let _tmp5: (null) | undefined;
                        _tmp5 = _.is(v, $ByteString) ? null : void 0;
                        if (_tmp5 !== void 0) {result = {"_variant": "ByteString"};};
                        if (result === void 0) {
                            let _tmp6: (null) | undefined;
                            _tmp6 = _.is(v, $Symbol) ? null : void 0;
                            if (_tmp6 !== void 0) {result = {"_variant": "Symbol"};};
                        };
                    };
                };
            };
        };
    };
    return result;
}

export function fromAtomKind<_embedded = _.GenericEmbedded>(_v: AtomKind): _.Value<_embedded> {
    switch (_v._variant) {
        case "Boolean": {return $Boolean;};
        case "Float": {return $Float;};
        case "Double": {return $Double;};
        case "SignedInteger": {return $SignedInteger;};
        case "String": {return $String;};
        case "ByteString": {return $ByteString;};
        case "Symbol": {return $Symbol;};
    };
}

export function asNamedAlternative<_embedded = _.GenericEmbedded>(v: _.Value<_embedded>): NamedAlternative<_embedded> {
    let result = toNamedAlternative(v);
    if (result === void 0) throw new TypeError(`Invalid NamedAlternative: ${_.stringify(v)}`);
    return result;
}

export function toNamedAlternative<_embedded = _.GenericEmbedded>(v: _.Value<_embedded>): undefined | NamedAlternative<_embedded> {
    let result: undefined | NamedAlternative<_embedded>;
    if (_.Array.isArray(v) && v.length === 2) {
        let _tmp0: (string) | undefined;
        _tmp0 = typeof v[0] === 'string' ? v[0] : void 0;
        if (_tmp0 !== void 0) {
            let _tmp1: (Pattern<_embedded>) | undefined;
            _tmp1 = toPattern(v[1]);
            if (_tmp1 !== void 0) {result = {"variantLabel": _tmp0, "pattern": _tmp1};};
        };
    };
    return result;
}

export function fromNamedAlternative<_embedded = _.GenericEmbedded>(_v: NamedAlternative<_embedded>): _.Value<_embedded> {return [_v["variantLabel"], fromPattern<_embedded>(_v["pattern"])];}

export function asNamedSimplePattern<_embedded = _.GenericEmbedded>(v: _.Value<_embedded>): NamedSimplePattern<_embedded> {
    let result = toNamedSimplePattern(v);
    if (result === void 0) throw new TypeError(`Invalid NamedSimplePattern: ${_.stringify(v)}`);
    return result;
}

export function toNamedSimplePattern<_embedded = _.GenericEmbedded>(v: _.Value<_embedded>): undefined | NamedSimplePattern<_embedded> {
    let _tmp0: (Binding<_embedded>) | undefined;
    let result: undefined | NamedSimplePattern<_embedded>;
    _tmp0 = toBinding(v);
    if (_tmp0 !== void 0) {result = {"_variant": "named", "value": _tmp0};};
    if (result === void 0) {
        let _tmp1: (SimplePattern<_embedded>) | undefined;
        _tmp1 = toSimplePattern(v);
        if (_tmp1 !== void 0) {result = {"_variant": "anonymous", "value": _tmp1};};
    };
    return result;
}

export function fromNamedSimplePattern<_embedded = _.GenericEmbedded>(_v: NamedSimplePattern<_embedded>): _.Value<_embedded> {
    switch (_v._variant) {
        case "named": {return fromBinding<_embedded>(_v.value);};
        case "anonymous": {return fromSimplePattern<_embedded>(_v.value);};
    };
}

export function asNamedPattern<_embedded = _.GenericEmbedded>(v: _.Value<_embedded>): NamedPattern<_embedded> {
    let result = toNamedPattern(v);
    if (result === void 0) throw new TypeError(`Invalid NamedPattern: ${_.stringify(v)}`);
    return result;
}

export function toNamedPattern<_embedded = _.GenericEmbedded>(v: _.Value<_embedded>): undefined | NamedPattern<_embedded> {
    let _tmp0: (Binding<_embedded>) | undefined;
    let result: undefined | NamedPattern<_embedded>;
    _tmp0 = toBinding(v);
    if (_tmp0 !== void 0) {result = {"_variant": "named", "value": _tmp0};};
    if (result === void 0) {
        let _tmp1: (Pattern<_embedded>) | undefined;
        _tmp1 = toPattern(v);
        if (_tmp1 !== void 0) {result = {"_variant": "anonymous", "value": _tmp1};};
    };
    return result;
}

export function fromNamedPattern<_embedded = _.GenericEmbedded>(_v: NamedPattern<_embedded>): _.Value<_embedded> {
    switch (_v._variant) {
        case "named": {return fromBinding<_embedded>(_v.value);};
        case "anonymous": {return fromPattern<_embedded>(_v.value);};
    };
}

export function asBinding<_embedded = _.GenericEmbedded>(v: _.Value<_embedded>): Binding<_embedded> {
    let result = toBinding(v);
    if (result === void 0) throw new TypeError(`Invalid Binding: ${_.stringify(v)}`);
    return result;
}

export function toBinding<_embedded = _.GenericEmbedded>(v: _.Value<_embedded>): undefined | Binding<_embedded> {
    let result: undefined | Binding<_embedded>;
    if (_.Record.isRecord<_.Value<_embedded>, _.Tuple<_.Value<_embedded>>, _embedded>(v)) {
        let _tmp0: (null) | undefined;
        _tmp0 = _.is(v.label, $named) ? null : void 0;
        if (_tmp0 !== void 0) {
            let _tmp1: (symbol) | undefined;
            _tmp1 = typeof v[0] === 'symbol' ? v[0] : void 0;
            if (_tmp1 !== void 0) {
                let _tmp2: (SimplePattern<_embedded>) | undefined;
                _tmp2 = toSimplePattern(v[1]);
                if (_tmp2 !== void 0) {result = {"name": _tmp1, "pattern": _tmp2};};
            };
        };
    };
    return result;
}

export function fromBinding<_embedded = _.GenericEmbedded>(_v: Binding<_embedded>): _.Value<_embedded> {
    return _.Record($named, [_v["name"], fromSimplePattern<_embedded>(_v["pattern"])]);
}

export function asRef<_embedded = _.GenericEmbedded>(v: _.Value<_embedded>): Ref {
    let result = toRef(v);
    if (result === void 0) throw new TypeError(`Invalid Ref: ${_.stringify(v)}`);
    return result;
}

export function toRef<_embedded = _.GenericEmbedded>(v: _.Value<_embedded>): undefined | Ref {
    let result: undefined | Ref;
    if (_.Record.isRecord<_.Value<_embedded>, _.Tuple<_.Value<_embedded>>, _embedded>(v)) {
        let _tmp0: (null) | undefined;
        _tmp0 = _.is(v.label, $ref) ? null : void 0;
        if (_tmp0 !== void 0) {
            let _tmp1: (ModulePath) | undefined;
            _tmp1 = toModulePath(v[0]);
            if (_tmp1 !== void 0) {
                let _tmp2: (symbol) | undefined;
                _tmp2 = typeof v[1] === 'symbol' ? v[1] : void 0;
                if (_tmp2 !== void 0) {result = {"module": _tmp1, "name": _tmp2};};
            };
        };
    };
    return result;
}

export function fromRef<_embedded = _.GenericEmbedded>(_v: Ref): _.Value<_embedded> {return _.Record($ref, [fromModulePath<_embedded>(_v["module"]), _v["name"]]);}

export function asModulePath<_embedded = _.GenericEmbedded>(v: _.Value<_embedded>): ModulePath {
    let result = toModulePath(v);
    if (result === void 0) throw new TypeError(`Invalid ModulePath: ${_.stringify(v)}`);
    return result;
}

export function toModulePath<_embedded = _.GenericEmbedded>(v: _.Value<_embedded>): undefined | ModulePath {
    let _tmp0: (Array<symbol>) | undefined;
    let result: undefined | ModulePath;
    _tmp0 = void 0;
    if (_.Array.isArray(v)) {
        _tmp0 = [];
        for (const _tmp1 of v) {
            let _tmp2: (symbol) | undefined;
            _tmp2 = typeof _tmp1 === 'symbol' ? _tmp1 : void 0;
            if (_tmp2 !== void 0) {_tmp0.push(_tmp2); continue;};
            _tmp0 = void 0;
            break;
        };
    };
    if (_tmp0 !== void 0) {result = _tmp0;};
    return result;
}

export function fromModulePath<_embedded = _.GenericEmbedded>(_v: ModulePath): _.Value<_embedded> {return _v.map(v => v);}

