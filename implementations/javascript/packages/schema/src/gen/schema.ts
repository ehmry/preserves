import * as _ from "@preserves/core";

export const $1 = 1;
export const $Boolean = Symbol.for("Boolean");
export const $ByteString = Symbol.for("ByteString");
export const $Double = Symbol.for("Double");
export const $Float = Symbol.for("Float");
export const $SignedInteger = Symbol.for("SignedInteger");
export const $String = Symbol.for("String");
export const $Symbol = Symbol.for("Symbol");
export const $and = Symbol.for("and");
export const $any = Symbol.for("any");
export const $atom = Symbol.for("atom");
export const $bundle = Symbol.for("bundle");
export const $definitions = Symbol.for("definitions");
export const $dict = Symbol.for("dict");
export const $dictof = Symbol.for("dictof");
export const $embedded = Symbol.for("embedded");
export const $embeddedType = Symbol.for("embeddedType");
export const $lit = Symbol.for("lit");
export const $named = Symbol.for("named");
export const $or = Symbol.for("or");
export const $rec = Symbol.for("rec");
export const $ref = Symbol.for("ref");
export const $schema = Symbol.for("schema");
export const $seqof = Symbol.for("seqof");
export const $setof = Symbol.for("setof");
export const $tuple = Symbol.for("tuple");
export const $tuplePrefix = Symbol.for("tuplePrefix");
export const $version = Symbol.for("version");
export const __lit6 = false;

export type _embedded = any;

export type _val = _.Value<_embedded>;

export type Bundle = {"modules": Modules};

export type Modules = _.KeyedDictionary<ModulePath, Schema, _embedded>;

export type Schema = {
    "version": Version,
    "embeddedType": EmbeddedTypeName,
    "definitions": Definitions
};

export type Version = null;

export type EmbeddedTypeName = ({"_variant": "Ref", "value": Ref} | {"_variant": "false"});

export type Definitions = _.KeyedDictionary<symbol, Definition, _embedded>;

export type Definition = (
    {
        "_variant": "or",
        "pattern0": NamedAlternative,
        "pattern1": NamedAlternative,
        "patternN": Array<NamedAlternative>
    } |
    {
        "_variant": "and",
        "pattern0": NamedPattern,
        "pattern1": NamedPattern,
        "patternN": Array<NamedPattern>
    } |
    {"_variant": "Pattern", "value": Pattern}
);

export type Pattern = (
    {"_variant": "SimplePattern", "value": SimplePattern} |
    {"_variant": "CompoundPattern", "value": CompoundPattern}
);

export type SimplePattern = (
    {"_variant": "any"} |
    {"_variant": "atom", "atomKind": AtomKind} |
    {"_variant": "embedded", "interface": SimplePattern} |
    {"_variant": "lit", "value": _val} |
    {"_variant": "seqof", "pattern": SimplePattern} |
    {"_variant": "setof", "pattern": SimplePattern} |
    {"_variant": "dictof", "key": SimplePattern, "value": SimplePattern} |
    {"_variant": "Ref", "value": Ref}
);

export type CompoundPattern = (
    {"_variant": "rec", "label": NamedPattern, "fields": NamedPattern} |
    {"_variant": "tuple", "patterns": Array<NamedPattern>} |
    {
        "_variant": "tuplePrefix",
        "fixed": Array<NamedPattern>,
        "variable": NamedSimplePattern
    } |
    {"_variant": "dict", "entries": DictionaryEntries}
);

export type DictionaryEntries = _.KeyedDictionary<_val, NamedSimplePattern, _embedded>;

export type AtomKind = (
    {"_variant": "Boolean"} |
    {"_variant": "Float"} |
    {"_variant": "Double"} |
    {"_variant": "SignedInteger"} |
    {"_variant": "String"} |
    {"_variant": "ByteString"} |
    {"_variant": "Symbol"}
);

export type NamedAlternative = {"variantLabel": string, "pattern": Pattern};

export type NamedSimplePattern = (
    {"_variant": "named", "value": Binding} |
    {"_variant": "anonymous", "value": SimplePattern}
);

export type NamedPattern = (
    {"_variant": "named", "value": Binding} |
    {"_variant": "anonymous", "value": Pattern}
);

export type Binding = {"name": symbol, "pattern": SimplePattern};

export type Ref = {"module": ModulePath, "name": symbol};

export type ModulePath = Array<symbol>;


export function Bundle(modules: Modules): Bundle {return {"modules": modules};}

export function Modules(value: _.KeyedDictionary<ModulePath, Schema, _embedded>): Modules {return value;}

export function Schema(
    {version, embeddedType, definitions}: {version: Version, embeddedType: EmbeddedTypeName, definitions: Definitions}
): Schema {
    return {"version": version, "embeddedType": embeddedType, "definitions": definitions};
}

export function Version(): Version {return null;}

export namespace EmbeddedTypeName {
    export function Ref(value: Ref): EmbeddedTypeName {return {"_variant": "Ref", "value": value};};
    export function $false(): EmbeddedTypeName {return {"_variant": "false"};};
}

export function Definitions(value: _.KeyedDictionary<symbol, Definition, _embedded>): Definitions {return value;}

export namespace Definition {
    export function or(
        {pattern0, pattern1, patternN}: {
            pattern0: NamedAlternative,
            pattern1: NamedAlternative,
            patternN: Array<NamedAlternative>
        }
    ): Definition {
        return {
            "_variant": "or",
            "pattern0": pattern0,
            "pattern1": pattern1,
            "patternN": patternN
        };
    };
    export function and(
        {pattern0, pattern1, patternN}: {pattern0: NamedPattern, pattern1: NamedPattern, patternN: Array<NamedPattern>}
    ): Definition {
        return {
            "_variant": "and",
            "pattern0": pattern0,
            "pattern1": pattern1,
            "patternN": patternN
        };
    };
    export function Pattern(value: Pattern): Definition {return {"_variant": "Pattern", "value": value};};
}

export namespace Pattern {
    export function SimplePattern(value: SimplePattern): Pattern {return {"_variant": "SimplePattern", "value": value};};
    export function CompoundPattern(value: CompoundPattern): Pattern {return {"_variant": "CompoundPattern", "value": value};};
}

export namespace SimplePattern {
    export function any(): SimplePattern {return {"_variant": "any"};};
    export function atom(atomKind: AtomKind): SimplePattern {return {"_variant": "atom", "atomKind": atomKind};};
    export function embedded($interface: SimplePattern): SimplePattern {return {"_variant": "embedded", "interface": $interface};};
    export function lit(value: _val): SimplePattern {return {"_variant": "lit", "value": value};};
    export function seqof(pattern: SimplePattern): SimplePattern {return {"_variant": "seqof", "pattern": pattern};};
    export function setof(pattern: SimplePattern): SimplePattern {return {"_variant": "setof", "pattern": pattern};};
    export function dictof({key, value}: {key: SimplePattern, value: SimplePattern}): SimplePattern {return {"_variant": "dictof", "key": key, "value": value};};
    export function Ref(value: Ref): SimplePattern {return {"_variant": "Ref", "value": value};};
}

export namespace CompoundPattern {
    export function rec({label, fields}: {label: NamedPattern, fields: NamedPattern}): CompoundPattern {return {"_variant": "rec", "label": label, "fields": fields};};
    export function tuple(patterns: Array<NamedPattern>): CompoundPattern {return {"_variant": "tuple", "patterns": patterns};};
    export function tuplePrefix({fixed, variable}: {fixed: Array<NamedPattern>, variable: NamedSimplePattern}): CompoundPattern {return {"_variant": "tuplePrefix", "fixed": fixed, "variable": variable};};
    export function dict(entries: DictionaryEntries): CompoundPattern {return {"_variant": "dict", "entries": entries};};
}

export function DictionaryEntries(value: _.KeyedDictionary<_val, NamedSimplePattern, _embedded>): DictionaryEntries {return value;}

export namespace AtomKind {
    export function Boolean(): AtomKind {return {"_variant": "Boolean"};};
    export function Float(): AtomKind {return {"_variant": "Float"};};
    export function Double(): AtomKind {return {"_variant": "Double"};};
    export function SignedInteger(): AtomKind {return {"_variant": "SignedInteger"};};
    export function String(): AtomKind {return {"_variant": "String"};};
    export function ByteString(): AtomKind {return {"_variant": "ByteString"};};
    export function Symbol(): AtomKind {return {"_variant": "Symbol"};};
}

export function NamedAlternative({variantLabel, pattern}: {variantLabel: string, pattern: Pattern}): NamedAlternative {return {"variantLabel": variantLabel, "pattern": pattern};}

export namespace NamedSimplePattern {
    export function named(value: Binding): NamedSimplePattern {return {"_variant": "named", "value": value};};
    export function anonymous(value: SimplePattern): NamedSimplePattern {return {"_variant": "anonymous", "value": value};};
}

export namespace NamedPattern {
    export function named(value: Binding): NamedPattern {return {"_variant": "named", "value": value};};
    export function anonymous(value: Pattern): NamedPattern {return {"_variant": "anonymous", "value": value};};
}

export function Binding({name, pattern}: {name: symbol, pattern: SimplePattern}): Binding {return {"name": name, "pattern": pattern};}

export function Ref({module, name}: {module: ModulePath, name: symbol}): Ref {return {"module": module, "name": name};}

export function ModulePath(value: Array<symbol>): ModulePath {return value;}

export function asBundle(v: _val): Bundle {
    let result = toBundle(v);
    if (result === void 0) throw new TypeError(`Invalid Bundle: ${_.stringify(v)}`);
    return result;
}

export function toBundle(v: _val): undefined | Bundle {
    let result: undefined | Bundle;
    if (_.Record.isRecord<_val, _.Tuple<_val>, _embedded>(v)) {
        let _tmp0: (null) | undefined;
        _tmp0 = _.is(v.label, $bundle) ? null : void 0;
        if (_tmp0 !== void 0) {
            let _tmp1: (Modules) | undefined;
            _tmp1 = toModules(v[0]);
            if (_tmp1 !== void 0) {result = {"modules": _tmp1};};
        };
    };
    return result;
}

export function fromBundle(_v: Bundle): _val {return _.Record($bundle, [fromModules(_v["modules"])]);}

export function asModules(v: _val): Modules {
    let result = toModules(v);
    if (result === void 0) throw new TypeError(`Invalid Modules: ${_.stringify(v)}`);
    return result;
}

export function toModules(v: _val): undefined | Modules {
    let _tmp0: (_.KeyedDictionary<ModulePath, Schema, _embedded>) | undefined;
    let result: undefined | Modules;
    _tmp0 = void 0;
    if (_.Dictionary.isDictionary<_embedded>(v)) {
        _tmp0 = new _.KeyedDictionary();
        for (const [_tmp2, _tmp1] of v) {
            let _tmp3: (ModulePath) | undefined;
            _tmp3 = toModulePath(_tmp2);
            if (_tmp3 !== void 0) {
                let _tmp4: (Schema) | undefined;
                _tmp4 = toSchema(_tmp1);
                if (_tmp4 !== void 0) {_tmp0.set(_tmp3, _tmp4); continue;};
            };
            _tmp0 = void 0;
            break;
        };
    };
    if (_tmp0 !== void 0) {result = _tmp0;};
    return result;
}

export function fromModules(_v: Modules): _val {
    return new _.Dictionary<_embedded>(_.Array.from(_v.entries()).map(([k, v]) => [fromModulePath(k), fromSchema(v)]));
}

export function asSchema(v: _val): Schema {
    let result = toSchema(v);
    if (result === void 0) throw new TypeError(`Invalid Schema: ${_.stringify(v)}`);
    return result;
}

export function toSchema(v: _val): undefined | Schema {
    let result: undefined | Schema;
    if (_.Record.isRecord<_val, _.Tuple<_val>, _embedded>(v)) {
        let _tmp0: (null) | undefined;
        _tmp0 = _.is(v.label, $schema) ? null : void 0;
        if (_tmp0 !== void 0) {
            if (_.Dictionary.isDictionary<_embedded>(v[0])) {
                let _tmp1: (_val) | undefined;
                if ((_tmp1 = v[0].get($version)) !== void 0) {
                    let _tmp2: (Version) | undefined;
                    _tmp2 = toVersion(_tmp1);
                    if (_tmp2 !== void 0) {
                        let _tmp3: (_val) | undefined;
                        if ((_tmp3 = v[0].get($embeddedType)) !== void 0) {
                            let _tmp4: (EmbeddedTypeName) | undefined;
                            _tmp4 = toEmbeddedTypeName(_tmp3);
                            if (_tmp4 !== void 0) {
                                let _tmp5: (_val) | undefined;
                                if ((_tmp5 = v[0].get($definitions)) !== void 0) {
                                    let _tmp6: (Definitions) | undefined;
                                    _tmp6 = toDefinitions(_tmp5);
                                    if (_tmp6 !== void 0) {result = {"version": _tmp2, "embeddedType": _tmp4, "definitions": _tmp6};};
                                };
                            };
                        };
                    };
                };
            };
        };
    };
    return result;
}

export function fromSchema(_v: Schema): _val {
    return _.Record(
        $schema,
        [
            new _.Dictionary<_embedded>(
                [
                    [$version, fromVersion(_v["version"])],
                    [$embeddedType, fromEmbeddedTypeName(_v["embeddedType"])],
                    [$definitions, fromDefinitions(_v["definitions"])]
                ]
            )
        ]
    );
}

export function asVersion(v: _val): Version {
    let result = toVersion(v);
    if (result === void 0) throw new TypeError(`Invalid Version: ${_.stringify(v)}`);
    return result;
}

export function toVersion(v: _val): undefined | Version {
    let _tmp0: (null) | undefined;
    let result: undefined | Version;
    _tmp0 = _.is(v, $1) ? null : void 0;
    if (_tmp0 !== void 0) {result = _tmp0;};
    return result;
}

export function fromVersion(_v: Version): _val {return $1;}

export function asEmbeddedTypeName(v: _val): EmbeddedTypeName {
    let result = toEmbeddedTypeName(v);
    if (result === void 0) throw new TypeError(`Invalid EmbeddedTypeName: ${_.stringify(v)}`);
    return result;
}

export function toEmbeddedTypeName(v: _val): undefined | EmbeddedTypeName {
    let _tmp0: (Ref) | undefined;
    let result: undefined | EmbeddedTypeName;
    _tmp0 = toRef(v);
    if (_tmp0 !== void 0) {result = {"_variant": "Ref", "value": _tmp0};};
    if (result === void 0) {
        let _tmp1: (null) | undefined;
        _tmp1 = _.is(v, __lit6) ? null : void 0;
        if (_tmp1 !== void 0) {result = {"_variant": "false"};};
    };
    return result;
}

export function fromEmbeddedTypeName(_v: EmbeddedTypeName): _val {
    switch (_v._variant) {case "Ref": {return fromRef(_v.value);}; case "false": {return __lit6;};};
}

export function asDefinitions(v: _val): Definitions {
    let result = toDefinitions(v);
    if (result === void 0) throw new TypeError(`Invalid Definitions: ${_.stringify(v)}`);
    return result;
}

export function toDefinitions(v: _val): undefined | Definitions {
    let _tmp0: (_.KeyedDictionary<symbol, Definition, _embedded>) | undefined;
    let result: undefined | Definitions;
    _tmp0 = void 0;
    if (_.Dictionary.isDictionary<_embedded>(v)) {
        _tmp0 = new _.KeyedDictionary();
        for (const [_tmp2, _tmp1] of v) {
            let _tmp3: (symbol) | undefined;
            _tmp3 = typeof _tmp2 === 'symbol' ? _tmp2 : void 0;
            if (_tmp3 !== void 0) {
                let _tmp4: (Definition) | undefined;
                _tmp4 = toDefinition(_tmp1);
                if (_tmp4 !== void 0) {_tmp0.set(_tmp3, _tmp4); continue;};
            };
            _tmp0 = void 0;
            break;
        };
    };
    if (_tmp0 !== void 0) {result = _tmp0;};
    return result;
}

export function fromDefinitions(_v: Definitions): _val {
    return new _.Dictionary<_embedded>(_.Array.from(_v.entries()).map(([k, v]) => [k, fromDefinition(v)]));
}

export function asDefinition(v: _val): Definition {
    let result = toDefinition(v);
    if (result === void 0) throw new TypeError(`Invalid Definition: ${_.stringify(v)}`);
    return result;
}

export function toDefinition(v: _val): undefined | Definition {
    let result: undefined | Definition;
    if (_.Record.isRecord<_val, _.Tuple<_val>, _embedded>(v)) {
        let _tmp0: (null) | undefined;
        _tmp0 = _.is(v.label, $or) ? null : void 0;
        if (_tmp0 !== void 0) {
            if (_.Array.isArray(v[0]) && v[0].length >= 2) {
                let _tmp1: (NamedAlternative) | undefined;
                _tmp1 = toNamedAlternative(v[0][0]);
                if (_tmp1 !== void 0) {
                    let _tmp2: (NamedAlternative) | undefined;
                    _tmp2 = toNamedAlternative(v[0][1]);
                    if (_tmp2 !== void 0) {
                        let _tmp3: (Array<_val>) | undefined;
                        let _tmp4: (Array<NamedAlternative>) | undefined;
                        _tmp3 = v[0].slice(2);
                        _tmp4 = [];
                        for (const _tmp5 of _tmp3) {
                            let _tmp6: (NamedAlternative) | undefined;
                            _tmp6 = toNamedAlternative(_tmp5);
                            if (_tmp6 !== void 0) {_tmp4.push(_tmp6); continue;};
                            _tmp4 = void 0;
                            break;
                        };
                        if (_tmp4 !== void 0) {
                            result = {"_variant": "or", "pattern0": _tmp1, "pattern1": _tmp2, "patternN": _tmp4};
                        };
                    };
                };
            };
        };
    };
    if (result === void 0) {
        if (_.Record.isRecord<_val, _.Tuple<_val>, _embedded>(v)) {
            let _tmp7: (null) | undefined;
            _tmp7 = _.is(v.label, $and) ? null : void 0;
            if (_tmp7 !== void 0) {
                if (_.Array.isArray(v[0]) && v[0].length >= 2) {
                    let _tmp8: (NamedPattern) | undefined;
                    _tmp8 = toNamedPattern(v[0][0]);
                    if (_tmp8 !== void 0) {
                        let _tmp9: (NamedPattern) | undefined;
                        _tmp9 = toNamedPattern(v[0][1]);
                        if (_tmp9 !== void 0) {
                            let _tmp10: (Array<_val>) | undefined;
                            let _tmp11: (Array<NamedPattern>) | undefined;
                            _tmp10 = v[0].slice(2);
                            _tmp11 = [];
                            for (const _tmp12 of _tmp10) {
                                let _tmp13: (NamedPattern) | undefined;
                                _tmp13 = toNamedPattern(_tmp12);
                                if (_tmp13 !== void 0) {_tmp11.push(_tmp13); continue;};
                                _tmp11 = void 0;
                                break;
                            };
                            if (_tmp11 !== void 0) {
                                result = {"_variant": "and", "pattern0": _tmp8, "pattern1": _tmp9, "patternN": _tmp11};
                            };
                        };
                    };
                };
            };
        };
        if (result === void 0) {
            let _tmp14: (Pattern) | undefined;
            _tmp14 = toPattern(v);
            if (_tmp14 !== void 0) {result = {"_variant": "Pattern", "value": _tmp14};};
        };
    };
    return result;
}

export function fromDefinition(_v: Definition): _val {
    switch (_v._variant) {
        case "or": {
            return _.Record(
                $or,
                [
                    [
                        fromNamedAlternative(_v["pattern0"]),
                        fromNamedAlternative(_v["pattern1"]),
                        ... _v["patternN"].map(v => fromNamedAlternative(v))
                    ]
                ]
            );
        };
        case "and": {
            return _.Record(
                $and,
                [
                    [
                        fromNamedPattern(_v["pattern0"]),
                        fromNamedPattern(_v["pattern1"]),
                        ... _v["patternN"].map(v => fromNamedPattern(v))
                    ]
                ]
            );
        };
        case "Pattern": {return fromPattern(_v.value);};
    };
}

export function asPattern(v: _val): Pattern {
    let result = toPattern(v);
    if (result === void 0) throw new TypeError(`Invalid Pattern: ${_.stringify(v)}`);
    return result;
}

export function toPattern(v: _val): undefined | Pattern {
    let _tmp0: (SimplePattern) | undefined;
    let result: undefined | Pattern;
    _tmp0 = toSimplePattern(v);
    if (_tmp0 !== void 0) {result = {"_variant": "SimplePattern", "value": _tmp0};};
    if (result === void 0) {
        let _tmp1: (CompoundPattern) | undefined;
        _tmp1 = toCompoundPattern(v);
        if (_tmp1 !== void 0) {result = {"_variant": "CompoundPattern", "value": _tmp1};};
    };
    return result;
}

export function fromPattern(_v: Pattern): _val {
    switch (_v._variant) {
        case "SimplePattern": {return fromSimplePattern(_v.value);};
        case "CompoundPattern": {return fromCompoundPattern(_v.value);};
    };
}

export function asSimplePattern(v: _val): SimplePattern {
    let result = toSimplePattern(v);
    if (result === void 0) throw new TypeError(`Invalid SimplePattern: ${_.stringify(v)}`);
    return result;
}

export function toSimplePattern(v: _val): undefined | SimplePattern {
    let _tmp0: (null) | undefined;
    let result: undefined | SimplePattern;
    _tmp0 = _.is(v, $any) ? null : void 0;
    if (_tmp0 !== void 0) {result = {"_variant": "any"};};
    if (result === void 0) {
        if (_.Record.isRecord<_val, _.Tuple<_val>, _embedded>(v)) {
            let _tmp1: (null) | undefined;
            _tmp1 = _.is(v.label, $atom) ? null : void 0;
            if (_tmp1 !== void 0) {
                let _tmp2: (AtomKind) | undefined;
                _tmp2 = toAtomKind(v[0]);
                if (_tmp2 !== void 0) {result = {"_variant": "atom", "atomKind": _tmp2};};
            };
        };
        if (result === void 0) {
            if (_.Record.isRecord<_val, _.Tuple<_val>, _embedded>(v)) {
                let _tmp3: (null) | undefined;
                _tmp3 = _.is(v.label, $embedded) ? null : void 0;
                if (_tmp3 !== void 0) {
                    let _tmp4: (SimplePattern) | undefined;
                    _tmp4 = toSimplePattern(v[0]);
                    if (_tmp4 !== void 0) {result = {"_variant": "embedded", "interface": _tmp4};};
                };
            };
            if (result === void 0) {
                if (_.Record.isRecord<_val, _.Tuple<_val>, _embedded>(v)) {
                    let _tmp5: (null) | undefined;
                    _tmp5 = _.is(v.label, $lit) ? null : void 0;
                    if (_tmp5 !== void 0) {
                        let _tmp6: (_val) | undefined;
                        _tmp6 = v[0];
                        if (_tmp6 !== void 0) {result = {"_variant": "lit", "value": _tmp6};};
                    };
                };
                if (result === void 0) {
                    if (_.Record.isRecord<_val, _.Tuple<_val>, _embedded>(v)) {
                        let _tmp7: (null) | undefined;
                        _tmp7 = _.is(v.label, $seqof) ? null : void 0;
                        if (_tmp7 !== void 0) {
                            let _tmp8: (SimplePattern) | undefined;
                            _tmp8 = toSimplePattern(v[0]);
                            if (_tmp8 !== void 0) {result = {"_variant": "seqof", "pattern": _tmp8};};
                        };
                    };
                    if (result === void 0) {
                        if (_.Record.isRecord<_val, _.Tuple<_val>, _embedded>(v)) {
                            let _tmp9: (null) | undefined;
                            _tmp9 = _.is(v.label, $setof) ? null : void 0;
                            if (_tmp9 !== void 0) {
                                let _tmp10: (SimplePattern) | undefined;
                                _tmp10 = toSimplePattern(v[0]);
                                if (_tmp10 !== void 0) {result = {"_variant": "setof", "pattern": _tmp10};};
                            };
                        };
                        if (result === void 0) {
                            if (_.Record.isRecord<_val, _.Tuple<_val>, _embedded>(v)) {
                                let _tmp11: (null) | undefined;
                                _tmp11 = _.is(v.label, $dictof) ? null : void 0;
                                if (_tmp11 !== void 0) {
                                    let _tmp12: (SimplePattern) | undefined;
                                    _tmp12 = toSimplePattern(v[0]);
                                    if (_tmp12 !== void 0) {
                                        let _tmp13: (SimplePattern) | undefined;
                                        _tmp13 = toSimplePattern(v[1]);
                                        if (_tmp13 !== void 0) {result = {"_variant": "dictof", "key": _tmp12, "value": _tmp13};};
                                    };
                                };
                            };
                            if (result === void 0) {
                                let _tmp14: (Ref) | undefined;
                                _tmp14 = toRef(v);
                                if (_tmp14 !== void 0) {result = {"_variant": "Ref", "value": _tmp14};};
                            };
                        };
                    };
                };
            };
        };
    };
    return result;
}

export function fromSimplePattern(_v: SimplePattern): _val {
    switch (_v._variant) {
        case "any": {return $any;};
        case "atom": {return _.Record($atom, [fromAtomKind(_v["atomKind"])]);};
        case "embedded": {return _.Record($embedded, [fromSimplePattern(_v["interface"])]);};
        case "lit": {return _.Record($lit, [_v["value"]]);};
        case "seqof": {return _.Record($seqof, [fromSimplePattern(_v["pattern"])]);};
        case "setof": {return _.Record($setof, [fromSimplePattern(_v["pattern"])]);};
        case "dictof": {
            return _.Record($dictof, [fromSimplePattern(_v["key"]), fromSimplePattern(_v["value"])]);
        };
        case "Ref": {return fromRef(_v.value);};
    };
}

export function asCompoundPattern(v: _val): CompoundPattern {
    let result = toCompoundPattern(v);
    if (result === void 0) throw new TypeError(`Invalid CompoundPattern: ${_.stringify(v)}`);
    return result;
}

export function toCompoundPattern(v: _val): undefined | CompoundPattern {
    let result: undefined | CompoundPattern;
    if (_.Record.isRecord<_val, _.Tuple<_val>, _embedded>(v)) {
        let _tmp0: (null) | undefined;
        _tmp0 = _.is(v.label, $rec) ? null : void 0;
        if (_tmp0 !== void 0) {
            let _tmp1: (NamedPattern) | undefined;
            _tmp1 = toNamedPattern(v[0]);
            if (_tmp1 !== void 0) {
                let _tmp2: (NamedPattern) | undefined;
                _tmp2 = toNamedPattern(v[1]);
                if (_tmp2 !== void 0) {result = {"_variant": "rec", "label": _tmp1, "fields": _tmp2};};
            };
        };
    };
    if (result === void 0) {
        if (_.Record.isRecord<_val, _.Tuple<_val>, _embedded>(v)) {
            let _tmp3: (null) | undefined;
            _tmp3 = _.is(v.label, $tuple) ? null : void 0;
            if (_tmp3 !== void 0) {
                let _tmp4: (Array<NamedPattern>) | undefined;
                _tmp4 = void 0;
                if (_.Array.isArray(v[0])) {
                    _tmp4 = [];
                    for (const _tmp5 of v[0]) {
                        let _tmp6: (NamedPattern) | undefined;
                        _tmp6 = toNamedPattern(_tmp5);
                        if (_tmp6 !== void 0) {_tmp4.push(_tmp6); continue;};
                        _tmp4 = void 0;
                        break;
                    };
                };
                if (_tmp4 !== void 0) {result = {"_variant": "tuple", "patterns": _tmp4};};
            };
        };
        if (result === void 0) {
            if (_.Record.isRecord<_val, _.Tuple<_val>, _embedded>(v)) {
                let _tmp7: (null) | undefined;
                _tmp7 = _.is(v.label, $tuplePrefix) ? null : void 0;
                if (_tmp7 !== void 0) {
                    let _tmp8: (Array<NamedPattern>) | undefined;
                    _tmp8 = void 0;
                    if (_.Array.isArray(v[0])) {
                        _tmp8 = [];
                        for (const _tmp9 of v[0]) {
                            let _tmp10: (NamedPattern) | undefined;
                            _tmp10 = toNamedPattern(_tmp9);
                            if (_tmp10 !== void 0) {_tmp8.push(_tmp10); continue;};
                            _tmp8 = void 0;
                            break;
                        };
                    };
                    if (_tmp8 !== void 0) {
                        let _tmp11: (NamedSimplePattern) | undefined;
                        _tmp11 = toNamedSimplePattern(v[1]);
                        if (_tmp11 !== void 0) {result = {"_variant": "tuplePrefix", "fixed": _tmp8, "variable": _tmp11};};
                    };
                };
            };
            if (result === void 0) {
                if (_.Record.isRecord<_val, _.Tuple<_val>, _embedded>(v)) {
                    let _tmp12: (null) | undefined;
                    _tmp12 = _.is(v.label, $dict) ? null : void 0;
                    if (_tmp12 !== void 0) {
                        let _tmp13: (DictionaryEntries) | undefined;
                        _tmp13 = toDictionaryEntries(v[0]);
                        if (_tmp13 !== void 0) {result = {"_variant": "dict", "entries": _tmp13};};
                    };
                };
            };
        };
    };
    return result;
}

export function fromCompoundPattern(_v: CompoundPattern): _val {
    switch (_v._variant) {
        case "rec": {
            return _.Record($rec, [fromNamedPattern(_v["label"]), fromNamedPattern(_v["fields"])]);
        };
        case "tuple": {return _.Record($tuple, [_v["patterns"].map(v => fromNamedPattern(v))]);};
        case "tuplePrefix": {
            return _.Record(
                $tuplePrefix,
                [
                    _v["fixed"].map(v => fromNamedPattern(v)),
                    fromNamedSimplePattern(_v["variable"])
                ]
            );
        };
        case "dict": {return _.Record($dict, [fromDictionaryEntries(_v["entries"])]);};
    };
}

export function asDictionaryEntries(v: _val): DictionaryEntries {
    let result = toDictionaryEntries(v);
    if (result === void 0) throw new TypeError(`Invalid DictionaryEntries: ${_.stringify(v)}`);
    return result;
}

export function toDictionaryEntries(v: _val): undefined | DictionaryEntries {
    let _tmp0: (_.KeyedDictionary<_val, NamedSimplePattern, _embedded>) | undefined;
    let result: undefined | DictionaryEntries;
    _tmp0 = void 0;
    if (_.Dictionary.isDictionary<_embedded>(v)) {
        _tmp0 = new _.KeyedDictionary();
        for (const [_tmp2, _tmp1] of v) {
            let _tmp3: (_val) | undefined;
            _tmp3 = _tmp2;
            if (_tmp3 !== void 0) {
                let _tmp4: (NamedSimplePattern) | undefined;
                _tmp4 = toNamedSimplePattern(_tmp1);
                if (_tmp4 !== void 0) {_tmp0.set(_tmp3, _tmp4); continue;};
            };
            _tmp0 = void 0;
            break;
        };
    };
    if (_tmp0 !== void 0) {result = _tmp0;};
    return result;
}

export function fromDictionaryEntries(_v: DictionaryEntries): _val {
    return new _.Dictionary<_embedded>(_.Array.from(_v.entries()).map(([k, v]) => [k, fromNamedSimplePattern(v)]));
}

export function asAtomKind(v: _val): AtomKind {
    let result = toAtomKind(v);
    if (result === void 0) throw new TypeError(`Invalid AtomKind: ${_.stringify(v)}`);
    return result;
}

export function toAtomKind(v: _val): undefined | AtomKind {
    let _tmp0: (null) | undefined;
    let result: undefined | AtomKind;
    _tmp0 = _.is(v, $Boolean) ? null : void 0;
    if (_tmp0 !== void 0) {result = {"_variant": "Boolean"};};
    if (result === void 0) {
        let _tmp1: (null) | undefined;
        _tmp1 = _.is(v, $Float) ? null : void 0;
        if (_tmp1 !== void 0) {result = {"_variant": "Float"};};
        if (result === void 0) {
            let _tmp2: (null) | undefined;
            _tmp2 = _.is(v, $Double) ? null : void 0;
            if (_tmp2 !== void 0) {result = {"_variant": "Double"};};
            if (result === void 0) {
                let _tmp3: (null) | undefined;
                _tmp3 = _.is(v, $SignedInteger) ? null : void 0;
                if (_tmp3 !== void 0) {result = {"_variant": "SignedInteger"};};
                if (result === void 0) {
                    let _tmp4: (null) | undefined;
                    _tmp4 = _.is(v, $String) ? null : void 0;
                    if (_tmp4 !== void 0) {result = {"_variant": "String"};};
                    if (result === void 0) {
                        let _tmp5: (null) | undefined;
                        _tmp5 = _.is(v, $ByteString) ? null : void 0;
                        if (_tmp5 !== void 0) {result = {"_variant": "ByteString"};};
                        if (result === void 0) {
                            let _tmp6: (null) | undefined;
                            _tmp6 = _.is(v, $Symbol) ? null : void 0;
                            if (_tmp6 !== void 0) {result = {"_variant": "Symbol"};};
                        };
                    };
                };
            };
        };
    };
    return result;
}

export function fromAtomKind(_v: AtomKind): _val {
    switch (_v._variant) {
        case "Boolean": {return $Boolean;};
        case "Float": {return $Float;};
        case "Double": {return $Double;};
        case "SignedInteger": {return $SignedInteger;};
        case "String": {return $String;};
        case "ByteString": {return $ByteString;};
        case "Symbol": {return $Symbol;};
    };
}

export function asNamedAlternative(v: _val): NamedAlternative {
    let result = toNamedAlternative(v);
    if (result === void 0) throw new TypeError(`Invalid NamedAlternative: ${_.stringify(v)}`);
    return result;
}

export function toNamedAlternative(v: _val): undefined | NamedAlternative {
    let result: undefined | NamedAlternative;
    if (_.Array.isArray(v) && v.length === 2) {
        let _tmp0: (string) | undefined;
        _tmp0 = typeof v[0] === 'string' ? v[0] : void 0;
        if (_tmp0 !== void 0) {
            let _tmp1: (Pattern) | undefined;
            _tmp1 = toPattern(v[1]);
            if (_tmp1 !== void 0) {result = {"variantLabel": _tmp0, "pattern": _tmp1};};
        };
    };
    return result;
}

export function fromNamedAlternative(_v: NamedAlternative): _val {return [_v["variantLabel"], fromPattern(_v["pattern"])];}

export function asNamedSimplePattern(v: _val): NamedSimplePattern {
    let result = toNamedSimplePattern(v);
    if (result === void 0) throw new TypeError(`Invalid NamedSimplePattern: ${_.stringify(v)}`);
    return result;
}

export function toNamedSimplePattern(v: _val): undefined | NamedSimplePattern {
    let _tmp0: (Binding) | undefined;
    let result: undefined | NamedSimplePattern;
    _tmp0 = toBinding(v);
    if (_tmp0 !== void 0) {result = {"_variant": "named", "value": _tmp0};};
    if (result === void 0) {
        let _tmp1: (SimplePattern) | undefined;
        _tmp1 = toSimplePattern(v);
        if (_tmp1 !== void 0) {result = {"_variant": "anonymous", "value": _tmp1};};
    };
    return result;
}

export function fromNamedSimplePattern(_v: NamedSimplePattern): _val {
    switch (_v._variant) {
        case "named": {return fromBinding(_v.value);};
        case "anonymous": {return fromSimplePattern(_v.value);};
    };
}

export function asNamedPattern(v: _val): NamedPattern {
    let result = toNamedPattern(v);
    if (result === void 0) throw new TypeError(`Invalid NamedPattern: ${_.stringify(v)}`);
    return result;
}

export function toNamedPattern(v: _val): undefined | NamedPattern {
    let _tmp0: (Binding) | undefined;
    let result: undefined | NamedPattern;
    _tmp0 = toBinding(v);
    if (_tmp0 !== void 0) {result = {"_variant": "named", "value": _tmp0};};
    if (result === void 0) {
        let _tmp1: (Pattern) | undefined;
        _tmp1 = toPattern(v);
        if (_tmp1 !== void 0) {result = {"_variant": "anonymous", "value": _tmp1};};
    };
    return result;
}

export function fromNamedPattern(_v: NamedPattern): _val {
    switch (_v._variant) {
        case "named": {return fromBinding(_v.value);};
        case "anonymous": {return fromPattern(_v.value);};
    };
}

export function asBinding(v: _val): Binding {
    let result = toBinding(v);
    if (result === void 0) throw new TypeError(`Invalid Binding: ${_.stringify(v)}`);
    return result;
}

export function toBinding(v: _val): undefined | Binding {
    let result: undefined | Binding;
    if (_.Record.isRecord<_val, _.Tuple<_val>, _embedded>(v)) {
        let _tmp0: (null) | undefined;
        _tmp0 = _.is(v.label, $named) ? null : void 0;
        if (_tmp0 !== void 0) {
            let _tmp1: (symbol) | undefined;
            _tmp1 = typeof v[0] === 'symbol' ? v[0] : void 0;
            if (_tmp1 !== void 0) {
                let _tmp2: (SimplePattern) | undefined;
                _tmp2 = toSimplePattern(v[1]);
                if (_tmp2 !== void 0) {result = {"name": _tmp1, "pattern": _tmp2};};
            };
        };
    };
    return result;
}

export function fromBinding(_v: Binding): _val {return _.Record($named, [_v["name"], fromSimplePattern(_v["pattern"])]);}

export function asRef(v: _val): Ref {
    let result = toRef(v);
    if (result === void 0) throw new TypeError(`Invalid Ref: ${_.stringify(v)}`);
    return result;
}

export function toRef(v: _val): undefined | Ref {
    let result: undefined | Ref;
    if (_.Record.isRecord<_val, _.Tuple<_val>, _embedded>(v)) {
        let _tmp0: (null) | undefined;
        _tmp0 = _.is(v.label, $ref) ? null : void 0;
        if (_tmp0 !== void 0) {
            let _tmp1: (ModulePath) | undefined;
            _tmp1 = toModulePath(v[0]);
            if (_tmp1 !== void 0) {
                let _tmp2: (symbol) | undefined;
                _tmp2 = typeof v[1] === 'symbol' ? v[1] : void 0;
                if (_tmp2 !== void 0) {result = {"module": _tmp1, "name": _tmp2};};
            };
        };
    };
    return result;
}

export function fromRef(_v: Ref): _val {return _.Record($ref, [fromModulePath(_v["module"]), _v["name"]]);}

export function asModulePath(v: _val): ModulePath {
    let result = toModulePath(v);
    if (result === void 0) throw new TypeError(`Invalid ModulePath: ${_.stringify(v)}`);
    return result;
}

export function toModulePath(v: _val): undefined | ModulePath {
    let _tmp0: (Array<symbol>) | undefined;
    let result: undefined | ModulePath;
    _tmp0 = void 0;
    if (_.Array.isArray(v)) {
        _tmp0 = [];
        for (const _tmp1 of v) {
            let _tmp2: (symbol) | undefined;
            _tmp2 = typeof _tmp1 === 'symbol' ? _tmp1 : void 0;
            if (_tmp2 !== void 0) {_tmp0.push(_tmp2); continue;};
            _tmp0 = void 0;
            break;
        };
    };
    if (_tmp0 !== void 0) {result = _tmp0;};
    return result;
}

export function fromModulePath(_v: ModulePath): _val {return _v.map(v => v);}

