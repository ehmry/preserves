use preserves_schema::compiler::*;

use std::io::Error;
use std::path::PathBuf;

fn main() -> Result<(), Error> {
    let buildroot = PathBuf::from(std::env::var_os("OUT_DIR").unwrap());

    let mut gen_dir = buildroot.clone();
    gen_dir.push("src/schemas");

    let mut c = CompilerConfig::new(gen_dir, "crate::schemas".to_owned());

    let inputs = expand_inputs(&vec!["path.bin".to_owned()])?;
    c.load_schemas_and_bundles(&inputs)?;

    compile(&c)
}
