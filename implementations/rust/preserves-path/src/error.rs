use std::io;

use thiserror::Error;

#[derive(Error, Debug)]
pub enum CompilationError {
    #[error(transparent)]
    IoError(#[from] io::Error),
    #[error("Cannot mix binary operators")]
    MixedOperators,
    #[error("Invalid step")]
    InvalidStep,
    #[error(transparent)]
    RegexError(#[from] regex::Error),
}
