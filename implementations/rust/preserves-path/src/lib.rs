pub mod error;
pub mod parse;
pub mod path;
pub mod predicate;

pub mod schemas {
    include!(concat!(env!("OUT_DIR"), "/src/schemas/mod.rs"));
}

pub mod step;

pub use error::CompilationError;

pub use parse::parse_selector;
pub use parse::parse_predicate;

pub use schemas::path::Predicate;
pub use schemas::path::Selector;
pub use schemas::path::Step;

pub use step::Node;
