use preserves::value::IOValue;

use std::rc::Rc;

pub enum Path {
    Root,
    Step(IOValue, Rc<Path>),
}

impl Path {
    pub fn root() -> Rc<Self> {
        Rc::new(Path::Root)
    }

    pub fn step(self: &Rc<Self>, v: &IOValue) -> Rc<Self> {
        Rc::new(Path::Step(v.clone(), Rc::clone(self)))
    }
}
