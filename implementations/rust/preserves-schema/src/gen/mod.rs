pub mod schema;

use crate::support as _support;
use _support::preserves;

#[allow(non_snake_case)]
pub struct Language<N: preserves::value::NestedValue> {
    pub LIT_15_FALSE: N /* #f */,
    pub LIT_28_1: N /* 1 */,
    pub LIT_0_BOOLEAN: N /* Boolean */,
    pub LIT_5_BYTE_STRING: N /* ByteString */,
    pub LIT_2_DOUBLE: N /* Double */,
    pub LIT_1_FLOAT: N /* Float */,
    pub LIT_3_SIGNED_INTEGER: N /* SignedInteger */,
    pub LIT_4_STRING: N /* String */,
    pub LIT_6_SYMBOL: N /* Symbol */,
    pub LIT_14_AND: N /* and */,
    pub LIT_21_ANY: N /* any */,
    pub LIT_22_ATOM: N /* atom */,
    pub LIT_8_BUNDLE: N /* bundle */,
    pub LIT_18_DEFINITIONS: N /* definitions */,
    pub LIT_12_DICT: N /* dict */,
    pub LIT_27_DICTOF: N /* dictof */,
    pub LIT_23_EMBEDDED: N /* embedded */,
    pub LIT_19_EMBEDDED_TYPE: N /* embeddedType */,
    pub LIT_24_LIT: N /* lit */,
    pub LIT_7_NAMED: N /* named */,
    pub LIT_13_OR: N /* or */,
    pub LIT_9_REC: N /* rec */,
    pub LIT_16_REF: N /* ref */,
    pub LIT_17_SCHEMA: N /* schema */,
    pub LIT_25_SEQOF: N /* seqof */,
    pub LIT_26_SETOF: N /* setof */,
    pub LIT_10_TUPLE: N /* tuple */,
    pub LIT_11_TUPLE_PREFIX: N /* tuplePrefix */,
    pub LIT_20_VERSION: N /* version */
}

impl<N: preserves::value::NestedValue> Default for Language<N> {
    fn default() -> Self {
        Language {
            LIT_15_FALSE: /* #f */ _support::decode_lit(&vec![128]).unwrap(),
            LIT_28_1: /* 1 */ _support::decode_lit(&vec![145]).unwrap(),
            LIT_0_BOOLEAN: /* Boolean */ _support::decode_lit(&vec![179, 7, 66, 111, 111, 108, 101, 97, 110]).unwrap(),
            LIT_5_BYTE_STRING: /* ByteString */ _support::decode_lit(&vec![179, 10, 66, 121, 116, 101, 83, 116, 114, 105, 110, 103]).unwrap(),
            LIT_2_DOUBLE: /* Double */ _support::decode_lit(&vec![179, 6, 68, 111, 117, 98, 108, 101]).unwrap(),
            LIT_1_FLOAT: /* Float */ _support::decode_lit(&vec![179, 5, 70, 108, 111, 97, 116]).unwrap(),
            LIT_3_SIGNED_INTEGER: /* SignedInteger */ _support::decode_lit(&vec![179, 13, 83, 105, 103, 110, 101, 100, 73, 110, 116, 101, 103, 101, 114]).unwrap(),
            LIT_4_STRING: /* String */ _support::decode_lit(&vec![179, 6, 83, 116, 114, 105, 110, 103]).unwrap(),
            LIT_6_SYMBOL: /* Symbol */ _support::decode_lit(&vec![179, 6, 83, 121, 109, 98, 111, 108]).unwrap(),
            LIT_14_AND: /* and */ _support::decode_lit(&vec![179, 3, 97, 110, 100]).unwrap(),
            LIT_21_ANY: /* any */ _support::decode_lit(&vec![179, 3, 97, 110, 121]).unwrap(),
            LIT_22_ATOM: /* atom */ _support::decode_lit(&vec![179, 4, 97, 116, 111, 109]).unwrap(),
            LIT_8_BUNDLE: /* bundle */ _support::decode_lit(&vec![179, 6, 98, 117, 110, 100, 108, 101]).unwrap(),
            LIT_18_DEFINITIONS: /* definitions */ _support::decode_lit(&vec![179, 11, 100, 101, 102, 105, 110, 105, 116, 105, 111, 110, 115]).unwrap(),
            LIT_12_DICT: /* dict */ _support::decode_lit(&vec![179, 4, 100, 105, 99, 116]).unwrap(),
            LIT_27_DICTOF: /* dictof */ _support::decode_lit(&vec![179, 6, 100, 105, 99, 116, 111, 102]).unwrap(),
            LIT_23_EMBEDDED: /* embedded */ _support::decode_lit(&vec![179, 8, 101, 109, 98, 101, 100, 100, 101, 100]).unwrap(),
            LIT_19_EMBEDDED_TYPE: /* embeddedType */ _support::decode_lit(&vec![179, 12, 101, 109, 98, 101, 100, 100, 101, 100, 84, 121, 112, 101]).unwrap(),
            LIT_24_LIT: /* lit */ _support::decode_lit(&vec![179, 3, 108, 105, 116]).unwrap(),
            LIT_7_NAMED: /* named */ _support::decode_lit(&vec![179, 5, 110, 97, 109, 101, 100]).unwrap(),
            LIT_13_OR: /* or */ _support::decode_lit(&vec![179, 2, 111, 114]).unwrap(),
            LIT_9_REC: /* rec */ _support::decode_lit(&vec![179, 3, 114, 101, 99]).unwrap(),
            LIT_16_REF: /* ref */ _support::decode_lit(&vec![179, 3, 114, 101, 102]).unwrap(),
            LIT_17_SCHEMA: /* schema */ _support::decode_lit(&vec![179, 6, 115, 99, 104, 101, 109, 97]).unwrap(),
            LIT_25_SEQOF: /* seqof */ _support::decode_lit(&vec![179, 5, 115, 101, 113, 111, 102]).unwrap(),
            LIT_26_SETOF: /* setof */ _support::decode_lit(&vec![179, 5, 115, 101, 116, 111, 102]).unwrap(),
            LIT_10_TUPLE: /* tuple */ _support::decode_lit(&vec![179, 5, 116, 117, 112, 108, 101]).unwrap(),
            LIT_11_TUPLE_PREFIX: /* tuplePrefix */ _support::decode_lit(&vec![179, 11, 116, 117, 112, 108, 101, 80, 114, 101, 102, 105, 120]).unwrap(),
            LIT_20_VERSION: /* version */ _support::decode_lit(&vec![179, 7, 118, 101, 114, 115, 105, 111, 110]).unwrap()
        }
    }
}
