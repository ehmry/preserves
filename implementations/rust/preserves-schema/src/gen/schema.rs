#![allow(unused_parens)]
#![allow(unused_imports)]

use std::convert::TryFrom;
use crate::support as _support;
use _support::Deserialize;
use _support::Parse;
use _support::Unparse;
use _support::preserves;
use preserves::value::Domain;
use preserves::value::NestedValue;

#[derive(Debug, PartialOrd, Ord, PartialEq, Eq, Clone, Hash)]
pub enum AtomKind {
    Boolean,
    Float,
    Double,
    SignedInteger,
    String,
    ByteString,
    Symbol
}

impl preserves::value::Domain for AtomKind {}

fn read_atom_kind_boolean<'de, _Value: preserves::value::NestedValue, R: _support::Reader<'de, _Value>>(r: &mut R) -> std::result::Result<AtomKind, _support::ParseError> {
    match r.next_token(true)? {
        preserves::value::Token::Atom(v) => match v.value() {
            preserves::value::Value::Symbol(w) if w == "Boolean" => {}
            _ => return Err(_support::ParseError::conformance_error("schema.AtomKind::Boolean"))?,
        }
        _ => return Err(_support::ParseError::conformance_error("schema.AtomKind::Boolean"))?,
    }
    let _tmp0 = ();
    Ok(AtomKind::Boolean)
}

fn read_atom_kind_float<'de, _Value: preserves::value::NestedValue, R: _support::Reader<'de, _Value>>(r: &mut R) -> std::result::Result<AtomKind, _support::ParseError> {
    match r.next_token(true)? {
        preserves::value::Token::Atom(v) => match v.value() {
            preserves::value::Value::Symbol(w) if w == "Float" => {}
            _ => return Err(_support::ParseError::conformance_error("schema.AtomKind::Float"))?,
        }
        _ => return Err(_support::ParseError::conformance_error("schema.AtomKind::Float"))?,
    }
    let _tmp0 = ();
    Ok(AtomKind::Float)
}

fn read_atom_kind_double<'de, _Value: preserves::value::NestedValue, R: _support::Reader<'de, _Value>>(r: &mut R) -> std::result::Result<AtomKind, _support::ParseError> {
    match r.next_token(true)? {
        preserves::value::Token::Atom(v) => match v.value() {
            preserves::value::Value::Symbol(w) if w == "Double" => {}
            _ => return Err(_support::ParseError::conformance_error("schema.AtomKind::Double"))?,
        }
        _ => return Err(_support::ParseError::conformance_error("schema.AtomKind::Double"))?,
    }
    let _tmp0 = ();
    Ok(AtomKind::Double)
}

fn read_atom_kind_signed_integer<'de, _Value: preserves::value::NestedValue, R: _support::Reader<'de, _Value>>(r: &mut R) -> std::result::Result<AtomKind, _support::ParseError> {
    match r.next_token(true)? {
        preserves::value::Token::Atom(v) => match v.value() {
            preserves::value::Value::Symbol(w) if w == "SignedInteger" => {}
            _ => return Err(_support::ParseError::conformance_error("schema.AtomKind::SignedInteger"))?,
        }
        _ => return Err(_support::ParseError::conformance_error("schema.AtomKind::SignedInteger"))?,
    }
    let _tmp0 = ();
    Ok(AtomKind::SignedInteger)
}

fn read_atom_kind_string<'de, _Value: preserves::value::NestedValue, R: _support::Reader<'de, _Value>>(r: &mut R) -> std::result::Result<AtomKind, _support::ParseError> {
    match r.next_token(true)? {
        preserves::value::Token::Atom(v) => match v.value() {
            preserves::value::Value::Symbol(w) if w == "String" => {}
            _ => return Err(_support::ParseError::conformance_error("schema.AtomKind::String"))?,
        }
        _ => return Err(_support::ParseError::conformance_error("schema.AtomKind::String"))?,
    }
    let _tmp0 = ();
    Ok(AtomKind::String)
}

fn read_atom_kind_byte_string<'de, _Value: preserves::value::NestedValue, R: _support::Reader<'de, _Value>>(r: &mut R) -> std::result::Result<AtomKind, _support::ParseError> {
    match r.next_token(true)? {
        preserves::value::Token::Atom(v) => match v.value() {
            preserves::value::Value::Symbol(w) if w == "ByteString" => {}
            _ => return Err(_support::ParseError::conformance_error("schema.AtomKind::ByteString"))?,
        }
        _ => return Err(_support::ParseError::conformance_error("schema.AtomKind::ByteString"))?,
    }
    let _tmp0 = ();
    Ok(AtomKind::ByteString)
}

fn read_atom_kind_symbol<'de, _Value: preserves::value::NestedValue, R: _support::Reader<'de, _Value>>(r: &mut R) -> std::result::Result<AtomKind, _support::ParseError> {
    match r.next_token(true)? {
        preserves::value::Token::Atom(v) => match v.value() {
            preserves::value::Value::Symbol(w) if w == "Symbol" => {}
            _ => return Err(_support::ParseError::conformance_error("schema.AtomKind::Symbol"))?,
        }
        _ => return Err(_support::ParseError::conformance_error("schema.AtomKind::Symbol"))?,
    }
    let _tmp0 = ();
    Ok(AtomKind::Symbol)
}

impl<'de, _Value: preserves::value::NestedValue, R: _support::Reader<'de, _Value>> _support::Deserialize<'de, _Value, R> for AtomKind {
    fn deserialize(r: &mut R) -> std::result::Result<Self, _support::ParseError> {
        let _mark = r.mark()?;
        match read_atom_kind_boolean(r) { Err(e) if e.is_conformance_error() => r.restore(&_mark)?, result => return result }
        match read_atom_kind_float(r) { Err(e) if e.is_conformance_error() => r.restore(&_mark)?, result => return result }
        match read_atom_kind_double(r) { Err(e) if e.is_conformance_error() => r.restore(&_mark)?, result => return result }
        match read_atom_kind_signed_integer(r) { Err(e) if e.is_conformance_error() => r.restore(&_mark)?, result => return result }
        match read_atom_kind_string(r) { Err(e) if e.is_conformance_error() => r.restore(&_mark)?, result => return result }
        match read_atom_kind_byte_string(r) { Err(e) if e.is_conformance_error() => r.restore(&_mark)?, result => return result }
        match read_atom_kind_symbol(r) { Err(e) if e.is_conformance_error() => r.restore(&_mark)?, result => return result }
        Err(_support::ParseError::conformance_error("schema.AtomKind"))
    }
}

fn parse_atom_kind_boolean<
    'a,
    _L: Copy + Into<&'a crate::gen::Language<_Value>>,
    _Value: preserves::value::NestedValue + 'a
>(_ctxt: _L, value: &_Value) -> std::result::Result<AtomKind, _support::ParseError> {
    if value != &<_L as Into<&'a crate::gen::Language<_Value>>>::into(_ctxt).LIT_0_BOOLEAN { return Err(_support::ParseError::conformance_error("schema.AtomKind::Boolean")); }
    let _tmp0 = ();
    Ok(AtomKind::Boolean)
}

fn parse_atom_kind_float<
    'a,
    _L: Copy + Into<&'a crate::gen::Language<_Value>>,
    _Value: preserves::value::NestedValue + 'a
>(_ctxt: _L, value: &_Value) -> std::result::Result<AtomKind, _support::ParseError> {
    if value != &<_L as Into<&'a crate::gen::Language<_Value>>>::into(_ctxt).LIT_1_FLOAT { return Err(_support::ParseError::conformance_error("schema.AtomKind::Float")); }
    let _tmp0 = ();
    Ok(AtomKind::Float)
}

fn parse_atom_kind_double<
    'a,
    _L: Copy + Into<&'a crate::gen::Language<_Value>>,
    _Value: preserves::value::NestedValue + 'a
>(_ctxt: _L, value: &_Value) -> std::result::Result<AtomKind, _support::ParseError> {
    if value != &<_L as Into<&'a crate::gen::Language<_Value>>>::into(_ctxt).LIT_2_DOUBLE { return Err(_support::ParseError::conformance_error("schema.AtomKind::Double")); }
    let _tmp0 = ();
    Ok(AtomKind::Double)
}

fn parse_atom_kind_signed_integer<
    'a,
    _L: Copy + Into<&'a crate::gen::Language<_Value>>,
    _Value: preserves::value::NestedValue + 'a
>(_ctxt: _L, value: &_Value) -> std::result::Result<AtomKind, _support::ParseError> {
    if value != &<_L as Into<&'a crate::gen::Language<_Value>>>::into(_ctxt).LIT_3_SIGNED_INTEGER { return Err(_support::ParseError::conformance_error("schema.AtomKind::SignedInteger")); }
    let _tmp0 = ();
    Ok(AtomKind::SignedInteger)
}

fn parse_atom_kind_string<
    'a,
    _L: Copy + Into<&'a crate::gen::Language<_Value>>,
    _Value: preserves::value::NestedValue + 'a
>(_ctxt: _L, value: &_Value) -> std::result::Result<AtomKind, _support::ParseError> {
    if value != &<_L as Into<&'a crate::gen::Language<_Value>>>::into(_ctxt).LIT_4_STRING { return Err(_support::ParseError::conformance_error("schema.AtomKind::String")); }
    let _tmp0 = ();
    Ok(AtomKind::String)
}

fn parse_atom_kind_byte_string<
    'a,
    _L: Copy + Into<&'a crate::gen::Language<_Value>>,
    _Value: preserves::value::NestedValue + 'a
>(_ctxt: _L, value: &_Value) -> std::result::Result<AtomKind, _support::ParseError> {
    if value != &<_L as Into<&'a crate::gen::Language<_Value>>>::into(_ctxt).LIT_5_BYTE_STRING { return Err(_support::ParseError::conformance_error("schema.AtomKind::ByteString")); }
    let _tmp0 = ();
    Ok(AtomKind::ByteString)
}

fn parse_atom_kind_symbol<
    'a,
    _L: Copy + Into<&'a crate::gen::Language<_Value>>,
    _Value: preserves::value::NestedValue + 'a
>(_ctxt: _L, value: &_Value) -> std::result::Result<AtomKind, _support::ParseError> {
    if value != &<_L as Into<&'a crate::gen::Language<_Value>>>::into(_ctxt).LIT_6_SYMBOL { return Err(_support::ParseError::conformance_error("schema.AtomKind::Symbol")); }
    let _tmp0 = ();
    Ok(AtomKind::Symbol)
}

impl<
    'a,
    _L: Copy + Into<&'a crate::gen::Language<_Value>>,
    _Value: preserves::value::NestedValue + 'a
> _support::Parse<_L, _Value> for AtomKind {
    fn parse(_ctxt: _L, value: &_Value) -> std::result::Result<Self, _support::ParseError> {
        if let Ok(r) = parse_atom_kind_boolean(_ctxt, value) { return Ok(r); }
        if let Ok(r) = parse_atom_kind_float(_ctxt, value) { return Ok(r); }
        if let Ok(r) = parse_atom_kind_double(_ctxt, value) { return Ok(r); }
        if let Ok(r) = parse_atom_kind_signed_integer(_ctxt, value) { return Ok(r); }
        if let Ok(r) = parse_atom_kind_string(_ctxt, value) { return Ok(r); }
        if let Ok(r) = parse_atom_kind_byte_string(_ctxt, value) { return Ok(r); }
        if let Ok(r) = parse_atom_kind_symbol(_ctxt, value) { return Ok(r); }
        Err(_support::ParseError::conformance_error("schema.AtomKind"))
    }
}

impl<
    'a,
    _L: Copy + Into<&'a crate::gen::Language<_Value>>,
    _Value: preserves::value::NestedValue + 'a
> _support::Unparse<_L, _Value> for AtomKind {
    fn unparse(&self, _ctxt: _L) -> _Value {
        match self {
            AtomKind::Boolean => (&<_L as Into<&'a crate::gen::Language<_Value>>>::into(_ctxt).LIT_0_BOOLEAN).clone(),
            AtomKind::Float => (&<_L as Into<&'a crate::gen::Language<_Value>>>::into(_ctxt).LIT_1_FLOAT).clone(),
            AtomKind::Double => (&<_L as Into<&'a crate::gen::Language<_Value>>>::into(_ctxt).LIT_2_DOUBLE).clone(),
            AtomKind::SignedInteger => (
                &<_L as Into<&'a crate::gen::Language<_Value>>>::into(_ctxt).LIT_3_SIGNED_INTEGER
            ).clone(),
            AtomKind::String => (&<_L as Into<&'a crate::gen::Language<_Value>>>::into(_ctxt).LIT_4_STRING).clone(),
            AtomKind::ByteString => (&<_L as Into<&'a crate::gen::Language<_Value>>>::into(_ctxt).LIT_5_BYTE_STRING).clone(),
            AtomKind::Symbol => (&<_L as Into<&'a crate::gen::Language<_Value>>>::into(_ctxt).LIT_6_SYMBOL).clone(),
        }
    }
}

#[derive(Debug, PartialOrd, Ord, PartialEq, Eq, Clone, Hash)]
pub struct Binding<_Value: preserves::value::NestedValue = preserves::value::IOValue> {
    pub name: std::string::String,
    pub pattern: SimplePattern<_Value>
}

impl<_Value: preserves::value::NestedValue> preserves::value::Domain for Binding<_Value> {}

impl<'de, _Value: preserves::value::NestedValue, R: _support::Reader<'de, _Value>> _support::Deserialize<'de, _Value, R> for Binding<_Value> {
    fn deserialize(r: &mut R) -> std::result::Result<Self, _support::ParseError> {
        r.open_record(None)?;
        let mut _tmp0 = _support::B::Type::default();
        _tmp0.shift(Some(_support::B::Item::RecordLabel));
        r.boundary(&_tmp0)?;
        match r.next_token(true)? {
            preserves::value::Token::Atom(v) => match v.value() {
                preserves::value::Value::Symbol(w) if w == "named" => {}
                _ => return Err(_support::ParseError::conformance_error("schema.Binding"))?,
            }
            _ => return Err(_support::ParseError::conformance_error("schema.Binding"))?,
        }
        let _tmp1 = ();
        _tmp0.shift(Some(_support::B::Item::RecordField));
        r.boundary(&_tmp0)?;
        let _tmp2 = r.next_symbol()?.into_owned();
        _tmp0.shift(Some(_support::B::Item::RecordField));
        r.boundary(&_tmp0)?;
        let _tmp3 = SimplePattern::deserialize(r)?;
        r.ensure_complete(_tmp0, &_support::B::Item::RecordField)?;
        Ok(Binding {name: _tmp2, pattern: _tmp3})
    }
}

impl<
    'a,
    _L: Copy + Into<&'a crate::gen::Language<_Value>>,
    _Value: preserves::value::NestedValue + 'a
> _support::Parse<_L, _Value> for Binding<_Value> {
    fn parse(_ctxt: _L, value: &_Value) -> std::result::Result<Self, _support::ParseError> {
        let _tmp0 = value.value().to_record(None)?;
        if _tmp0.label() != &<_L as Into<&'a crate::gen::Language<_Value>>>::into(_ctxt).LIT_7_NAMED { return Err(_support::ParseError::conformance_error("schema.Binding")); }
        let _tmp1 = ();
        if _tmp0.fields().len() < 2 { return Err(_support::ParseError::conformance_error("schema.Binding")); }
        let _tmp2 = (&_tmp0.fields()[0]).value().to_symbol()?;
        let _tmp3 = SimplePattern::parse(_ctxt, (&_tmp0.fields()[1]))?;
        Ok(Binding {name: _tmp2.clone(), pattern: _tmp3})
    }
}

impl<
    'a,
    _L: Copy + Into<&'a crate::gen::Language<_Value>>,
    _Value: preserves::value::NestedValue + 'a
> _support::Unparse<_L, _Value> for Binding<_Value> {
    fn unparse(&self, _ctxt: _L) -> _Value {
        let Binding {name: _tmp0, pattern: _tmp1} = self;
        {
            let mut _tmp2 = preserves::value::Record(vec![(&<_L as Into<&'a crate::gen::Language<_Value>>>::into(_ctxt).LIT_7_NAMED).clone()]);
            _tmp2.fields_vec_mut().push(preserves::value::Value::symbol(_tmp0).wrap());
            _tmp2.fields_vec_mut().push(_tmp1.unparse(_ctxt));
            _tmp2.finish().wrap()
        }
    }
}

#[derive(Debug, PartialOrd, Ord, PartialEq, Eq, Clone, Hash)]
pub struct Bundle<_Value: preserves::value::NestedValue = preserves::value::IOValue> {
    pub modules: Modules<_Value>
}

impl<_Value: preserves::value::NestedValue> preserves::value::Domain for Bundle<_Value> {}

impl<'de, _Value: preserves::value::NestedValue, R: _support::Reader<'de, _Value>> _support::Deserialize<'de, _Value, R> for Bundle<_Value> {
    fn deserialize(r: &mut R) -> std::result::Result<Self, _support::ParseError> {
        r.open_record(None)?;
        let mut _tmp0 = _support::B::Type::default();
        _tmp0.shift(Some(_support::B::Item::RecordLabel));
        r.boundary(&_tmp0)?;
        match r.next_token(true)? {
            preserves::value::Token::Atom(v) => match v.value() {
                preserves::value::Value::Symbol(w) if w == "bundle" => {}
                _ => return Err(_support::ParseError::conformance_error("schema.Bundle"))?,
            }
            _ => return Err(_support::ParseError::conformance_error("schema.Bundle"))?,
        }
        let _tmp1 = ();
        _tmp0.shift(Some(_support::B::Item::RecordField));
        r.boundary(&_tmp0)?;
        let _tmp2 = Modules::deserialize(r)?;
        r.ensure_complete(_tmp0, &_support::B::Item::RecordField)?;
        Ok(Bundle {modules: _tmp2})
    }
}

impl<
    'a,
    _L: Copy + Into<&'a crate::gen::Language<_Value>>,
    _Value: preserves::value::NestedValue + 'a
> _support::Parse<_L, _Value> for Bundle<_Value> {
    fn parse(_ctxt: _L, value: &_Value) -> std::result::Result<Self, _support::ParseError> {
        let _tmp0 = value.value().to_record(None)?;
        if _tmp0.label() != &<_L as Into<&'a crate::gen::Language<_Value>>>::into(_ctxt).LIT_8_BUNDLE { return Err(_support::ParseError::conformance_error("schema.Bundle")); }
        let _tmp1 = ();
        if _tmp0.fields().len() < 1 { return Err(_support::ParseError::conformance_error("schema.Bundle")); }
        let _tmp2 = Modules::parse(_ctxt, (&_tmp0.fields()[0]))?;
        Ok(Bundle {modules: _tmp2})
    }
}

impl<
    'a,
    _L: Copy + Into<&'a crate::gen::Language<_Value>>,
    _Value: preserves::value::NestedValue + 'a
> _support::Unparse<_L, _Value> for Bundle<_Value> {
    fn unparse(&self, _ctxt: _L) -> _Value {
        let Bundle {modules: _tmp0} = self;
        {
            let mut _tmp1 = preserves::value::Record(vec![(&<_L as Into<&'a crate::gen::Language<_Value>>>::into(_ctxt).LIT_8_BUNDLE).clone()]);
            _tmp1.fields_vec_mut().push(_tmp0.unparse(_ctxt));
            _tmp1.finish().wrap()
        }
    }
}

#[derive(Debug, PartialOrd, Ord, PartialEq, Eq, Clone, Hash)]
pub enum CompoundPattern<_Value: preserves::value::NestedValue = preserves::value::IOValue> {
    Rec {
        label: std::boxed::Box<NamedPattern<_Value>>,
        fields: std::boxed::Box<NamedPattern<_Value>>
    },
    Tuple {
        patterns: std::vec::Vec<NamedPattern<_Value>>
    },
    TuplePrefix {
        fixed: std::vec::Vec<NamedPattern<_Value>>,
        variable: std::boxed::Box<NamedSimplePattern<_Value>>
    },
    Dict {
        entries: std::boxed::Box<DictionaryEntries<_Value>>
    }
}

impl<_Value: preserves::value::NestedValue> preserves::value::Domain for CompoundPattern<_Value> {}

fn read_compound_pattern_rec<'de, _Value: preserves::value::NestedValue, R: _support::Reader<'de, _Value>>(r: &mut R) -> std::result::Result<CompoundPattern<_Value>, _support::ParseError> {
    r.open_record(None)?;
    let mut _tmp0 = _support::B::Type::default();
    _tmp0.shift(Some(_support::B::Item::RecordLabel));
    r.boundary(&_tmp0)?;
    match r.next_token(true)? {
        preserves::value::Token::Atom(v) => match v.value() {
            preserves::value::Value::Symbol(w) if w == "rec" => {}
            _ => return Err(_support::ParseError::conformance_error("schema.CompoundPattern::rec"))?,
        }
        _ => return Err(_support::ParseError::conformance_error("schema.CompoundPattern::rec"))?,
    }
    let _tmp1 = ();
    _tmp0.shift(Some(_support::B::Item::RecordField));
    r.boundary(&_tmp0)?;
    let _tmp2 = NamedPattern::deserialize(r)?;
    _tmp0.shift(Some(_support::B::Item::RecordField));
    r.boundary(&_tmp0)?;
    let _tmp3 = NamedPattern::deserialize(r)?;
    r.ensure_complete(_tmp0, &_support::B::Item::RecordField)?;
    Ok(CompoundPattern::Rec {label: std::boxed::Box::new(_tmp2), fields: std::boxed::Box::new(_tmp3)})
}

fn read_compound_pattern_tuple<'de, _Value: preserves::value::NestedValue, R: _support::Reader<'de, _Value>>(r: &mut R) -> std::result::Result<CompoundPattern<_Value>, _support::ParseError> {
    r.open_record(None)?;
    let mut _tmp0 = _support::B::Type::default();
    _tmp0.shift(Some(_support::B::Item::RecordLabel));
    r.boundary(&_tmp0)?;
    match r.next_token(true)? {
        preserves::value::Token::Atom(v) => match v.value() {
            preserves::value::Value::Symbol(w) if w == "tuple" => {}
            _ => return Err(_support::ParseError::conformance_error("schema.CompoundPattern::tuple"))?,
        }
        _ => return Err(_support::ParseError::conformance_error("schema.CompoundPattern::tuple"))?,
    }
    let _tmp1 = ();
    _tmp0.shift(Some(_support::B::Item::RecordField));
    r.boundary(&_tmp0)?;
    r.open_sequence()?;
    let mut _tmp4 = _support::B::Type::default();
    let mut _tmp3 = std::vec::Vec::new();
    while !r.close_compound(&mut _tmp4, &_support::B::Item::SequenceValue)? {
        _tmp4.shift(Some(_support::B::Item::SequenceValue));
        r.boundary(&_tmp4)?;
        let _tmp5 = NamedPattern::deserialize(r)?;
        _tmp3.push(_tmp5);
    }
    let _tmp2 = _tmp3;
    r.ensure_complete(_tmp0, &_support::B::Item::RecordField)?;
    Ok(CompoundPattern::Tuple {patterns: _tmp2})
}

fn read_compound_pattern_tuple_prefix<'de, _Value: preserves::value::NestedValue, R: _support::Reader<'de, _Value>>(r: &mut R) -> std::result::Result<CompoundPattern<_Value>, _support::ParseError> {
    r.open_record(None)?;
    let mut _tmp0 = _support::B::Type::default();
    _tmp0.shift(Some(_support::B::Item::RecordLabel));
    r.boundary(&_tmp0)?;
    match r.next_token(true)? {
        preserves::value::Token::Atom(v) => match v.value() {
            preserves::value::Value::Symbol(w) if w == "tuplePrefix" => {}
            _ => return Err(_support::ParseError::conformance_error("schema.CompoundPattern::tuplePrefix"))?,
        }
        _ => return Err(_support::ParseError::conformance_error("schema.CompoundPattern::tuplePrefix"))?,
    }
    let _tmp1 = ();
    _tmp0.shift(Some(_support::B::Item::RecordField));
    r.boundary(&_tmp0)?;
    r.open_sequence()?;
    let mut _tmp4 = _support::B::Type::default();
    let mut _tmp3 = std::vec::Vec::new();
    while !r.close_compound(&mut _tmp4, &_support::B::Item::SequenceValue)? {
        _tmp4.shift(Some(_support::B::Item::SequenceValue));
        r.boundary(&_tmp4)?;
        let _tmp5 = NamedPattern::deserialize(r)?;
        _tmp3.push(_tmp5);
    }
    let _tmp2 = _tmp3;
    _tmp0.shift(Some(_support::B::Item::RecordField));
    r.boundary(&_tmp0)?;
    let _tmp6 = NamedSimplePattern::deserialize(r)?;
    r.ensure_complete(_tmp0, &_support::B::Item::RecordField)?;
    Ok(CompoundPattern::TuplePrefix {fixed: _tmp2, variable: std::boxed::Box::new(_tmp6)})
}

fn read_compound_pattern_dict<'de, _Value: preserves::value::NestedValue, R: _support::Reader<'de, _Value>>(r: &mut R) -> std::result::Result<CompoundPattern<_Value>, _support::ParseError> {
    r.open_record(None)?;
    let mut _tmp0 = _support::B::Type::default();
    _tmp0.shift(Some(_support::B::Item::RecordLabel));
    r.boundary(&_tmp0)?;
    match r.next_token(true)? {
        preserves::value::Token::Atom(v) => match v.value() {
            preserves::value::Value::Symbol(w) if w == "dict" => {}
            _ => return Err(_support::ParseError::conformance_error("schema.CompoundPattern::dict"))?,
        }
        _ => return Err(_support::ParseError::conformance_error("schema.CompoundPattern::dict"))?,
    }
    let _tmp1 = ();
    _tmp0.shift(Some(_support::B::Item::RecordField));
    r.boundary(&_tmp0)?;
    let _tmp2 = DictionaryEntries::deserialize(r)?;
    r.ensure_complete(_tmp0, &_support::B::Item::RecordField)?;
    Ok(CompoundPattern::Dict {entries: std::boxed::Box::new(_tmp2)})
}

impl<'de, _Value: preserves::value::NestedValue, R: _support::Reader<'de, _Value>> _support::Deserialize<'de, _Value, R> for CompoundPattern<_Value> {
    fn deserialize(r: &mut R) -> std::result::Result<Self, _support::ParseError> {
        let _mark = r.mark()?;
        match read_compound_pattern_rec(r) { Err(e) if e.is_conformance_error() => r.restore(&_mark)?, result => return result }
        match read_compound_pattern_tuple(r) { Err(e) if e.is_conformance_error() => r.restore(&_mark)?, result => return result }
        match read_compound_pattern_tuple_prefix(r) { Err(e) if e.is_conformance_error() => r.restore(&_mark)?, result => return result }
        match read_compound_pattern_dict(r) { Err(e) if e.is_conformance_error() => r.restore(&_mark)?, result => return result }
        Err(_support::ParseError::conformance_error("schema.CompoundPattern"))
    }
}

fn parse_compound_pattern_rec<
    'a,
    _L: Copy + Into<&'a crate::gen::Language<_Value>>,
    _Value: preserves::value::NestedValue + 'a
>(_ctxt: _L, value: &_Value) -> std::result::Result<CompoundPattern<_Value>, _support::ParseError> {
    let _tmp0 = value.value().to_record(None)?;
    if _tmp0.label() != &<_L as Into<&'a crate::gen::Language<_Value>>>::into(_ctxt).LIT_9_REC { return Err(_support::ParseError::conformance_error("schema.CompoundPattern::rec")); }
    let _tmp1 = ();
    if _tmp0.fields().len() < 2 { return Err(_support::ParseError::conformance_error("schema.CompoundPattern::rec")); }
    let _tmp2 = NamedPattern::parse(_ctxt, (&_tmp0.fields()[0]))?;
    let _tmp3 = NamedPattern::parse(_ctxt, (&_tmp0.fields()[1]))?;
    Ok(CompoundPattern::Rec {label: std::boxed::Box::new(_tmp2), fields: std::boxed::Box::new(_tmp3)})
}

fn parse_compound_pattern_tuple<
    'a,
    _L: Copy + Into<&'a crate::gen::Language<_Value>>,
    _Value: preserves::value::NestedValue + 'a
>(_ctxt: _L, value: &_Value) -> std::result::Result<CompoundPattern<_Value>, _support::ParseError> {
    let _tmp0 = value.value().to_record(None)?;
    if _tmp0.label() != &<_L as Into<&'a crate::gen::Language<_Value>>>::into(_ctxt).LIT_10_TUPLE { return Err(_support::ParseError::conformance_error("schema.CompoundPattern::tuple")); }
    let _tmp1 = ();
    if _tmp0.fields().len() < 1 { return Err(_support::ParseError::conformance_error("schema.CompoundPattern::tuple")); }
    let _tmp3 = (&_tmp0.fields()[0]).value().to_sequence()?;
    let mut _tmp2 = std::vec::Vec::new();
    for _tmp4 in &_tmp3[0..] {
        let _tmp5 = NamedPattern::parse(_ctxt, _tmp4)?;
        _tmp2.push(_tmp5);
    }
    Ok(CompoundPattern::Tuple {patterns: _tmp2})
}

fn parse_compound_pattern_tuple_prefix<
    'a,
    _L: Copy + Into<&'a crate::gen::Language<_Value>>,
    _Value: preserves::value::NestedValue + 'a
>(_ctxt: _L, value: &_Value) -> std::result::Result<CompoundPattern<_Value>, _support::ParseError> {
    let _tmp0 = value.value().to_record(None)?;
    if _tmp0.label() != &<_L as Into<&'a crate::gen::Language<_Value>>>::into(_ctxt).LIT_11_TUPLE_PREFIX { return Err(_support::ParseError::conformance_error("schema.CompoundPattern::tuplePrefix")); }
    let _tmp1 = ();
    if _tmp0.fields().len() < 2 { return Err(_support::ParseError::conformance_error("schema.CompoundPattern::tuplePrefix")); }
    let _tmp3 = (&_tmp0.fields()[0]).value().to_sequence()?;
    let mut _tmp2 = std::vec::Vec::new();
    for _tmp4 in &_tmp3[0..] {
        let _tmp5 = NamedPattern::parse(_ctxt, _tmp4)?;
        _tmp2.push(_tmp5);
    }
    let _tmp6 = NamedSimplePattern::parse(_ctxt, (&_tmp0.fields()[1]))?;
    Ok(CompoundPattern::TuplePrefix {fixed: _tmp2, variable: std::boxed::Box::new(_tmp6)})
}

fn parse_compound_pattern_dict<
    'a,
    _L: Copy + Into<&'a crate::gen::Language<_Value>>,
    _Value: preserves::value::NestedValue + 'a
>(_ctxt: _L, value: &_Value) -> std::result::Result<CompoundPattern<_Value>, _support::ParseError> {
    let _tmp0 = value.value().to_record(None)?;
    if _tmp0.label() != &<_L as Into<&'a crate::gen::Language<_Value>>>::into(_ctxt).LIT_12_DICT { return Err(_support::ParseError::conformance_error("schema.CompoundPattern::dict")); }
    let _tmp1 = ();
    if _tmp0.fields().len() < 1 { return Err(_support::ParseError::conformance_error("schema.CompoundPattern::dict")); }
    let _tmp2 = DictionaryEntries::parse(_ctxt, (&_tmp0.fields()[0]))?;
    Ok(CompoundPattern::Dict {entries: std::boxed::Box::new(_tmp2)})
}

impl<
    'a,
    _L: Copy + Into<&'a crate::gen::Language<_Value>>,
    _Value: preserves::value::NestedValue + 'a
> _support::Parse<_L, _Value> for CompoundPattern<_Value> {
    fn parse(_ctxt: _L, value: &_Value) -> std::result::Result<Self, _support::ParseError> {
        if let Ok(r) = parse_compound_pattern_rec(_ctxt, value) { return Ok(r); }
        if let Ok(r) = parse_compound_pattern_tuple(_ctxt, value) { return Ok(r); }
        if let Ok(r) = parse_compound_pattern_tuple_prefix(_ctxt, value) { return Ok(r); }
        if let Ok(r) = parse_compound_pattern_dict(_ctxt, value) { return Ok(r); }
        Err(_support::ParseError::conformance_error("schema.CompoundPattern"))
    }
}

impl<
    'a,
    _L: Copy + Into<&'a crate::gen::Language<_Value>>,
    _Value: preserves::value::NestedValue + 'a
> _support::Unparse<_L, _Value> for CompoundPattern<_Value> {
    fn unparse(&self, _ctxt: _L) -> _Value {
        match self {
            CompoundPattern::Rec {label: _tmp0, fields: _tmp1} => {
                let mut _tmp2 = preserves::value::Record(vec![(&<_L as Into<&'a crate::gen::Language<_Value>>>::into(_ctxt).LIT_9_REC).clone()]);
                _tmp2.fields_vec_mut().push(_tmp0.as_ref().unparse(_ctxt));
                _tmp2.fields_vec_mut().push(_tmp1.as_ref().unparse(_ctxt));
                _tmp2.finish().wrap()
            },
            CompoundPattern::Tuple {patterns: _tmp0} => {
                let mut _tmp1 = preserves::value::Record(vec![(&<_L as Into<&'a crate::gen::Language<_Value>>>::into(_ctxt).LIT_10_TUPLE).clone()]);
                _tmp1.fields_vec_mut().push(
                    {
                        let mut _tmp2 = std::vec::Vec::new();
                        for _tmp3 in _tmp0 {
                            _tmp2.push(_tmp3.unparse(_ctxt));
                        }
                        preserves::value::Value::Sequence(_tmp2).wrap()
                    }
                );
                _tmp1.finish().wrap()
            },
            CompoundPattern::TuplePrefix {fixed: _tmp0, variable: _tmp1} => {
                let mut _tmp2 = preserves::value::Record(vec![(
                    &<_L as Into<&'a crate::gen::Language<_Value>>>::into(_ctxt).LIT_11_TUPLE_PREFIX
                ).clone()]);
                _tmp2.fields_vec_mut().push(
                    {
                        let mut _tmp3 = std::vec::Vec::new();
                        for _tmp4 in _tmp0 {
                            _tmp3.push(_tmp4.unparse(_ctxt));
                        }
                        preserves::value::Value::Sequence(_tmp3).wrap()
                    }
                );
                _tmp2.fields_vec_mut().push(_tmp1.as_ref().unparse(_ctxt));
                _tmp2.finish().wrap()
            },
            CompoundPattern::Dict {entries: _tmp0} => {
                let mut _tmp1 = preserves::value::Record(vec![(&<_L as Into<&'a crate::gen::Language<_Value>>>::into(_ctxt).LIT_12_DICT).clone()]);
                _tmp1.fields_vec_mut().push(_tmp0.as_ref().unparse(_ctxt));
                _tmp1.finish().wrap()
            },
        }
    }
}

#[derive(Debug, PartialOrd, Ord, PartialEq, Eq, Clone, Hash)]
pub enum Definition<_Value: preserves::value::NestedValue = preserves::value::IOValue> {
    Or {
        pattern_0: std::boxed::Box<NamedAlternative<_Value>>,
        pattern_1: std::boxed::Box<NamedAlternative<_Value>>,
        pattern_n: std::vec::Vec<NamedAlternative<_Value>>
    },
    And {
        pattern_0: std::boxed::Box<NamedPattern<_Value>>,
        pattern_1: std::boxed::Box<NamedPattern<_Value>>,
        pattern_n: std::vec::Vec<NamedPattern<_Value>>
    },
    Pattern(std::boxed::Box<Pattern<_Value>>)
}

impl<_Value: preserves::value::NestedValue> preserves::value::Domain for Definition<_Value> {}

fn read_definition_or<'de, _Value: preserves::value::NestedValue, R: _support::Reader<'de, _Value>>(r: &mut R) -> std::result::Result<Definition<_Value>, _support::ParseError> {
    r.open_record(None)?;
    let mut _tmp0 = _support::B::Type::default();
    _tmp0.shift(Some(_support::B::Item::RecordLabel));
    r.boundary(&_tmp0)?;
    match r.next_token(true)? {
        preserves::value::Token::Atom(v) => match v.value() {
            preserves::value::Value::Symbol(w) if w == "or" => {}
            _ => return Err(_support::ParseError::conformance_error("schema.Definition::or"))?,
        }
        _ => return Err(_support::ParseError::conformance_error("schema.Definition::or"))?,
    }
    let _tmp1 = ();
    _tmp0.shift(Some(_support::B::Item::RecordField));
    r.boundary(&_tmp0)?;
    r.open_sequence()?;
    let mut _tmp2 = _support::B::Type::default();
    _tmp2.shift(Some(_support::B::Item::SequenceValue));
    r.boundary(&_tmp2)?;
    let _tmp3 = NamedAlternative::deserialize(r)?;
    _tmp2.shift(Some(_support::B::Item::SequenceValue));
    r.boundary(&_tmp2)?;
    let _tmp4 = NamedAlternative::deserialize(r)?;
    let mut _tmp6 = std::vec::Vec::new();
    while !r.close_compound(&mut _tmp2, &_support::B::Item::SequenceValue)? {
        _tmp2.shift(Some(_support::B::Item::SequenceValue));
        r.boundary(&_tmp2)?;
        let _tmp7 = NamedAlternative::deserialize(r)?;
        _tmp6.push(_tmp7);
    }
    let _tmp5 = _tmp6;
    r.ensure_complete(_tmp0, &_support::B::Item::RecordField)?;
    Ok(Definition::Or {
        pattern_0: std::boxed::Box::new(_tmp3),
        pattern_1: std::boxed::Box::new(_tmp4),
        pattern_n: _tmp5
    })
}

fn read_definition_and<'de, _Value: preserves::value::NestedValue, R: _support::Reader<'de, _Value>>(r: &mut R) -> std::result::Result<Definition<_Value>, _support::ParseError> {
    r.open_record(None)?;
    let mut _tmp0 = _support::B::Type::default();
    _tmp0.shift(Some(_support::B::Item::RecordLabel));
    r.boundary(&_tmp0)?;
    match r.next_token(true)? {
        preserves::value::Token::Atom(v) => match v.value() {
            preserves::value::Value::Symbol(w) if w == "and" => {}
            _ => return Err(_support::ParseError::conformance_error("schema.Definition::and"))?,
        }
        _ => return Err(_support::ParseError::conformance_error("schema.Definition::and"))?,
    }
    let _tmp1 = ();
    _tmp0.shift(Some(_support::B::Item::RecordField));
    r.boundary(&_tmp0)?;
    r.open_sequence()?;
    let mut _tmp2 = _support::B::Type::default();
    _tmp2.shift(Some(_support::B::Item::SequenceValue));
    r.boundary(&_tmp2)?;
    let _tmp3 = NamedPattern::deserialize(r)?;
    _tmp2.shift(Some(_support::B::Item::SequenceValue));
    r.boundary(&_tmp2)?;
    let _tmp4 = NamedPattern::deserialize(r)?;
    let mut _tmp6 = std::vec::Vec::new();
    while !r.close_compound(&mut _tmp2, &_support::B::Item::SequenceValue)? {
        _tmp2.shift(Some(_support::B::Item::SequenceValue));
        r.boundary(&_tmp2)?;
        let _tmp7 = NamedPattern::deserialize(r)?;
        _tmp6.push(_tmp7);
    }
    let _tmp5 = _tmp6;
    r.ensure_complete(_tmp0, &_support::B::Item::RecordField)?;
    Ok(Definition::And {
        pattern_0: std::boxed::Box::new(_tmp3),
        pattern_1: std::boxed::Box::new(_tmp4),
        pattern_n: _tmp5
    })
}

fn read_definition_pattern<'de, _Value: preserves::value::NestedValue, R: _support::Reader<'de, _Value>>(r: &mut R) -> std::result::Result<Definition<_Value>, _support::ParseError> {
    let _tmp0 = Pattern::deserialize(r)?;
    Ok(Definition::Pattern(std::boxed::Box::new(_tmp0)))
}

impl<'de, _Value: preserves::value::NestedValue, R: _support::Reader<'de, _Value>> _support::Deserialize<'de, _Value, R> for Definition<_Value> {
    fn deserialize(r: &mut R) -> std::result::Result<Self, _support::ParseError> {
        let _mark = r.mark()?;
        match read_definition_or(r) { Err(e) if e.is_conformance_error() => r.restore(&_mark)?, result => return result }
        match read_definition_and(r) { Err(e) if e.is_conformance_error() => r.restore(&_mark)?, result => return result }
        match read_definition_pattern(r) { Err(e) if e.is_conformance_error() => r.restore(&_mark)?, result => return result }
        Err(_support::ParseError::conformance_error("schema.Definition"))
    }
}

fn parse_definition_or<
    'a,
    _L: Copy + Into<&'a crate::gen::Language<_Value>>,
    _Value: preserves::value::NestedValue + 'a
>(_ctxt: _L, value: &_Value) -> std::result::Result<Definition<_Value>, _support::ParseError> {
    let _tmp0 = value.value().to_record(None)?;
    if _tmp0.label() != &<_L as Into<&'a crate::gen::Language<_Value>>>::into(_ctxt).LIT_13_OR { return Err(_support::ParseError::conformance_error("schema.Definition::or")); }
    let _tmp1 = ();
    if _tmp0.fields().len() < 1 { return Err(_support::ParseError::conformance_error("schema.Definition::or")); }
    let _tmp2 = (&_tmp0.fields()[0]).value().to_sequence()?;
    if _tmp2.len() < 2 { return Err(_support::ParseError::conformance_error("schema.Definition::or")); }
    let _tmp3 = NamedAlternative::parse(_ctxt, (&_tmp2[0]))?;
    let _tmp4 = NamedAlternative::parse(_ctxt, (&_tmp2[1]))?;
    let mut _tmp5 = std::vec::Vec::new();
    for _tmp6 in &_tmp2[2..] {
        let _tmp7 = NamedAlternative::parse(_ctxt, _tmp6)?;
        _tmp5.push(_tmp7);
    }
    Ok(Definition::Or {
        pattern_0: std::boxed::Box::new(_tmp3),
        pattern_1: std::boxed::Box::new(_tmp4),
        pattern_n: _tmp5
    })
}

fn parse_definition_and<
    'a,
    _L: Copy + Into<&'a crate::gen::Language<_Value>>,
    _Value: preserves::value::NestedValue + 'a
>(_ctxt: _L, value: &_Value) -> std::result::Result<Definition<_Value>, _support::ParseError> {
    let _tmp0 = value.value().to_record(None)?;
    if _tmp0.label() != &<_L as Into<&'a crate::gen::Language<_Value>>>::into(_ctxt).LIT_14_AND { return Err(_support::ParseError::conformance_error("schema.Definition::and")); }
    let _tmp1 = ();
    if _tmp0.fields().len() < 1 { return Err(_support::ParseError::conformance_error("schema.Definition::and")); }
    let _tmp2 = (&_tmp0.fields()[0]).value().to_sequence()?;
    if _tmp2.len() < 2 { return Err(_support::ParseError::conformance_error("schema.Definition::and")); }
    let _tmp3 = NamedPattern::parse(_ctxt, (&_tmp2[0]))?;
    let _tmp4 = NamedPattern::parse(_ctxt, (&_tmp2[1]))?;
    let mut _tmp5 = std::vec::Vec::new();
    for _tmp6 in &_tmp2[2..] {
        let _tmp7 = NamedPattern::parse(_ctxt, _tmp6)?;
        _tmp5.push(_tmp7);
    }
    Ok(Definition::And {
        pattern_0: std::boxed::Box::new(_tmp3),
        pattern_1: std::boxed::Box::new(_tmp4),
        pattern_n: _tmp5
    })
}

fn parse_definition_pattern<
    'a,
    _L: Copy + Into<&'a crate::gen::Language<_Value>>,
    _Value: preserves::value::NestedValue + 'a
>(_ctxt: _L, value: &_Value) -> std::result::Result<Definition<_Value>, _support::ParseError> {
    let _tmp0 = Pattern::parse(_ctxt, value)?;
    Ok(Definition::Pattern(std::boxed::Box::new(_tmp0)))
}

impl<
    'a,
    _L: Copy + Into<&'a crate::gen::Language<_Value>>,
    _Value: preserves::value::NestedValue + 'a
> _support::Parse<_L, _Value> for Definition<_Value> {
    fn parse(_ctxt: _L, value: &_Value) -> std::result::Result<Self, _support::ParseError> {
        if let Ok(r) = parse_definition_or(_ctxt, value) { return Ok(r); }
        if let Ok(r) = parse_definition_and(_ctxt, value) { return Ok(r); }
        if let Ok(r) = parse_definition_pattern(_ctxt, value) { return Ok(r); }
        Err(_support::ParseError::conformance_error("schema.Definition"))
    }
}

impl<
    'a,
    _L: Copy + Into<&'a crate::gen::Language<_Value>>,
    _Value: preserves::value::NestedValue + 'a
> _support::Unparse<_L, _Value> for Definition<_Value> {
    fn unparse(&self, _ctxt: _L) -> _Value {
        match self {
            Definition::Or {pattern_0: _tmp0, pattern_1: _tmp1, pattern_n: _tmp2} => {
                let mut _tmp3 = preserves::value::Record(vec![(&<_L as Into<&'a crate::gen::Language<_Value>>>::into(_ctxt).LIT_13_OR).clone()]);
                _tmp3.fields_vec_mut().push(
                    {
                        let mut _tmp4 = std::vec::Vec::new();
                        _tmp4.push(_tmp0.as_ref().unparse(_ctxt));
                        _tmp4.push(_tmp1.as_ref().unparse(_ctxt));
                        for _tmp5 in _tmp2 {
                            _tmp4.push(_tmp5.unparse(_ctxt));
                        }
                        preserves::value::Value::Sequence(_tmp4).wrap()
                    }
                );
                _tmp3.finish().wrap()
            },
            Definition::And {pattern_0: _tmp0, pattern_1: _tmp1, pattern_n: _tmp2} => {
                let mut _tmp3 = preserves::value::Record(vec![(&<_L as Into<&'a crate::gen::Language<_Value>>>::into(_ctxt).LIT_14_AND).clone()]);
                _tmp3.fields_vec_mut().push(
                    {
                        let mut _tmp4 = std::vec::Vec::new();
                        _tmp4.push(_tmp0.as_ref().unparse(_ctxt));
                        _tmp4.push(_tmp1.as_ref().unparse(_ctxt));
                        for _tmp5 in _tmp2 {
                            _tmp4.push(_tmp5.unparse(_ctxt));
                        }
                        preserves::value::Value::Sequence(_tmp4).wrap()
                    }
                );
                _tmp3.finish().wrap()
            },
            Definition::Pattern(_tmp0) => _tmp0.as_ref().unparse(_ctxt),
        }
    }
}

#[derive(Debug, PartialOrd, Ord, PartialEq, Eq, Clone, Hash)]
pub struct Definitions<_Value: preserves::value::NestedValue = preserves::value::IOValue>(pub preserves::value::Map<std::string::String, Definition<_Value>>);

impl<_Value: preserves::value::NestedValue> preserves::value::Domain for Definitions<_Value> {}

impl<'de, _Value: preserves::value::NestedValue, R: _support::Reader<'de, _Value>> _support::Deserialize<'de, _Value, R> for Definitions<_Value> {
    fn deserialize(r: &mut R) -> std::result::Result<Self, _support::ParseError> {
        r.open_dictionary()?;
        let mut _tmp2 = _support::B::Type::default();
        let mut _tmp1 = preserves::value::Map::new();
        while !r.close_compound(&mut _tmp2, &_support::B::Item::DictionaryKey)? {
            _tmp2.shift(Some(_support::B::Item::DictionaryKey));
            r.boundary(&_tmp2)?;
            let _tmp3 = r.next_symbol()?.into_owned();
            _tmp2.shift(Some(_support::B::Item::DictionaryValue));
            r.boundary(&_tmp2)?;
            let _tmp4 = Definition::deserialize(r)?;
            _tmp1.insert(_tmp3, _tmp4);
        }
        let _tmp0 = _tmp1;
        Ok(Definitions(_tmp0))
    }
}

impl<
    'a,
    _L: Copy + Into<&'a crate::gen::Language<_Value>>,
    _Value: preserves::value::NestedValue + 'a
> _support::Parse<_L, _Value> for Definitions<_Value> {
    fn parse(_ctxt: _L, value: &_Value) -> std::result::Result<Self, _support::ParseError> {
        let mut _tmp0 = preserves::value::Map::new();
        for (_tmp1, _tmp2) in value.value().to_dictionary()? {
            let _tmp3 = _tmp1.value().to_symbol()?;
            let _tmp4 = Definition::parse(_ctxt, _tmp2)?;
            _tmp0.insert(_tmp3.clone(), _tmp4);
        }
        Ok(Definitions(_tmp0))
    }
}

impl<
    'a,
    _L: Copy + Into<&'a crate::gen::Language<_Value>>,
    _Value: preserves::value::NestedValue + 'a
> _support::Unparse<_L, _Value> for Definitions<_Value> {
    fn unparse(&self, _ctxt: _L) -> _Value {
        let Definitions(_tmp0) = self;
        preserves::value::Value::Dictionary(_tmp0.iter().map(|(_tmp1, _tmp2)| (preserves::value::Value::symbol(_tmp1).wrap(), _tmp2.unparse(_ctxt))).collect()).wrap()
    }
}

#[derive(Debug, PartialOrd, Ord, PartialEq, Eq, Clone, Hash)]
pub struct DictionaryEntries<_Value: preserves::value::NestedValue = preserves::value::IOValue>(pub preserves::value::Map<_Value, NamedSimplePattern<_Value>>);

impl<_Value: preserves::value::NestedValue> preserves::value::Domain for DictionaryEntries<_Value> {}

impl<'de, _Value: preserves::value::NestedValue, R: _support::Reader<'de, _Value>> _support::Deserialize<'de, _Value, R> for DictionaryEntries<_Value> {
    fn deserialize(r: &mut R) -> std::result::Result<Self, _support::ParseError> {
        r.open_dictionary()?;
        let mut _tmp2 = _support::B::Type::default();
        let mut _tmp1 = preserves::value::Map::new();
        while !r.close_compound(&mut _tmp2, &_support::B::Item::DictionaryKey)? {
            _tmp2.shift(Some(_support::B::Item::DictionaryKey));
            r.boundary(&_tmp2)?;
            let _tmp3 = r.demand_next(true)?;
            _tmp2.shift(Some(_support::B::Item::DictionaryValue));
            r.boundary(&_tmp2)?;
            let _tmp4 = NamedSimplePattern::deserialize(r)?;
            _tmp1.insert(_tmp3, _tmp4);
        }
        let _tmp0 = _tmp1;
        Ok(DictionaryEntries(_tmp0))
    }
}

impl<
    'a,
    _L: Copy + Into<&'a crate::gen::Language<_Value>>,
    _Value: preserves::value::NestedValue + 'a
> _support::Parse<_L, _Value> for DictionaryEntries<_Value> {
    fn parse(_ctxt: _L, value: &_Value) -> std::result::Result<Self, _support::ParseError> {
        let mut _tmp0 = preserves::value::Map::new();
        for (_tmp1, _tmp2) in value.value().to_dictionary()? {
            let _tmp3 = _tmp1;
            let _tmp4 = NamedSimplePattern::parse(_ctxt, _tmp2)?;
            _tmp0.insert(_tmp3.clone(), _tmp4);
        }
        Ok(DictionaryEntries(_tmp0))
    }
}

impl<
    'a,
    _L: Copy + Into<&'a crate::gen::Language<_Value>>,
    _Value: preserves::value::NestedValue + 'a
> _support::Unparse<_L, _Value> for DictionaryEntries<_Value> {
    fn unparse(&self, _ctxt: _L) -> _Value {
        let DictionaryEntries(_tmp0) = self;
        preserves::value::Value::Dictionary(_tmp0.iter().map(|(_tmp1, _tmp2)| (_tmp1.clone(), _tmp2.unparse(_ctxt))).collect()).wrap()
    }
}

#[derive(Debug, PartialOrd, Ord, PartialEq, Eq, Clone, Hash)]
pub enum EmbeddedTypeName {
    Ref(std::boxed::Box<Ref>),
    False
}

impl preserves::value::Domain for EmbeddedTypeName {}

fn read_embedded_type_name_ref<'de, _Value: preserves::value::NestedValue, R: _support::Reader<'de, _Value>>(r: &mut R) -> std::result::Result<EmbeddedTypeName, _support::ParseError> {
    let _tmp0 = Ref::deserialize(r)?;
    Ok(EmbeddedTypeName::Ref(std::boxed::Box::new(_tmp0)))
}

fn read_embedded_type_name_false<'de, _Value: preserves::value::NestedValue, R: _support::Reader<'de, _Value>>(r: &mut R) -> std::result::Result<EmbeddedTypeName, _support::ParseError> {
    match r.next_token(true)? {
        preserves::value::Token::Atom(v) => match v.value() {
            preserves::value::Value::Boolean(w) if !*w => {}
            _ => return Err(_support::ParseError::conformance_error("schema.EmbeddedTypeName::false"))?,
        }
        _ => return Err(_support::ParseError::conformance_error("schema.EmbeddedTypeName::false"))?,
    }
    let _tmp0 = ();
    Ok(EmbeddedTypeName::False)
}

impl<'de, _Value: preserves::value::NestedValue, R: _support::Reader<'de, _Value>> _support::Deserialize<'de, _Value, R> for EmbeddedTypeName {
    fn deserialize(r: &mut R) -> std::result::Result<Self, _support::ParseError> {
        let _mark = r.mark()?;
        match read_embedded_type_name_ref(r) { Err(e) if e.is_conformance_error() => r.restore(&_mark)?, result => return result }
        match read_embedded_type_name_false(r) { Err(e) if e.is_conformance_error() => r.restore(&_mark)?, result => return result }
        Err(_support::ParseError::conformance_error("schema.EmbeddedTypeName"))
    }
}

fn parse_embedded_type_name_ref<
    'a,
    _L: Copy + Into<&'a crate::gen::Language<_Value>>,
    _Value: preserves::value::NestedValue + 'a
>(_ctxt: _L, value: &_Value) -> std::result::Result<EmbeddedTypeName, _support::ParseError> {
    let _tmp0 = Ref::parse(_ctxt, value)?;
    Ok(EmbeddedTypeName::Ref(std::boxed::Box::new(_tmp0)))
}

fn parse_embedded_type_name_false<
    'a,
    _L: Copy + Into<&'a crate::gen::Language<_Value>>,
    _Value: preserves::value::NestedValue + 'a
>(_ctxt: _L, value: &_Value) -> std::result::Result<EmbeddedTypeName, _support::ParseError> {
    if value != &<_L as Into<&'a crate::gen::Language<_Value>>>::into(_ctxt).LIT_15_FALSE { return Err(_support::ParseError::conformance_error("schema.EmbeddedTypeName::false")); }
    let _tmp0 = ();
    Ok(EmbeddedTypeName::False)
}

impl<
    'a,
    _L: Copy + Into<&'a crate::gen::Language<_Value>>,
    _Value: preserves::value::NestedValue + 'a
> _support::Parse<_L, _Value> for EmbeddedTypeName {
    fn parse(_ctxt: _L, value: &_Value) -> std::result::Result<Self, _support::ParseError> {
        if let Ok(r) = parse_embedded_type_name_ref(_ctxt, value) { return Ok(r); }
        if let Ok(r) = parse_embedded_type_name_false(_ctxt, value) { return Ok(r); }
        Err(_support::ParseError::conformance_error("schema.EmbeddedTypeName"))
    }
}

impl<
    'a,
    _L: Copy + Into<&'a crate::gen::Language<_Value>>,
    _Value: preserves::value::NestedValue + 'a
> _support::Unparse<_L, _Value> for EmbeddedTypeName {
    fn unparse(&self, _ctxt: _L) -> _Value {
        match self {
            EmbeddedTypeName::Ref(_tmp0) => _tmp0.as_ref().unparse(_ctxt),
            EmbeddedTypeName::False => (&<_L as Into<&'a crate::gen::Language<_Value>>>::into(_ctxt).LIT_15_FALSE).clone(),
        }
    }
}

#[derive(Debug, PartialOrd, Ord, PartialEq, Eq, Clone, Hash)]
pub struct ModulePath(pub std::vec::Vec<std::string::String>);

impl preserves::value::Domain for ModulePath {}

impl<'de, _Value: preserves::value::NestedValue, R: _support::Reader<'de, _Value>> _support::Deserialize<'de, _Value, R> for ModulePath {
    fn deserialize(r: &mut R) -> std::result::Result<Self, _support::ParseError> {
        r.open_sequence()?;
        let mut _tmp2 = _support::B::Type::default();
        let mut _tmp1 = std::vec::Vec::new();
        while !r.close_compound(&mut _tmp2, &_support::B::Item::SequenceValue)? {
            _tmp2.shift(Some(_support::B::Item::SequenceValue));
            r.boundary(&_tmp2)?;
            let _tmp3 = r.next_symbol()?.into_owned();
            _tmp1.push(_tmp3);
        }
        let _tmp0 = _tmp1;
        Ok(ModulePath(_tmp0))
    }
}

impl<
    'a,
    _L: Copy + Into<&'a crate::gen::Language<_Value>>,
    _Value: preserves::value::NestedValue + 'a
> _support::Parse<_L, _Value> for ModulePath {
    fn parse(_ctxt: _L, value: &_Value) -> std::result::Result<Self, _support::ParseError> {
        let _tmp1 = value.value().to_sequence()?;
        let mut _tmp0 = std::vec::Vec::new();
        for _tmp2 in &_tmp1[0..] {
            let _tmp3 = _tmp2.value().to_symbol()?;
            _tmp0.push(_tmp3.clone());
        }
        Ok(ModulePath(_tmp0))
    }
}

impl<
    'a,
    _L: Copy + Into<&'a crate::gen::Language<_Value>>,
    _Value: preserves::value::NestedValue + 'a
> _support::Unparse<_L, _Value> for ModulePath {
    fn unparse(&self, _ctxt: _L) -> _Value {
        let ModulePath(_tmp0) = self;
        {
            let mut _tmp1 = std::vec::Vec::new();
            for _tmp2 in _tmp0 {
                _tmp1.push(preserves::value::Value::symbol(_tmp2).wrap());
            }
            preserves::value::Value::Sequence(_tmp1).wrap()
        }
    }
}

#[derive(Debug, PartialOrd, Ord, PartialEq, Eq, Clone, Hash)]
pub struct Modules<_Value: preserves::value::NestedValue = preserves::value::IOValue>(pub preserves::value::Map<ModulePath, Schema<_Value>>);

impl<_Value: preserves::value::NestedValue> preserves::value::Domain for Modules<_Value> {}

impl<'de, _Value: preserves::value::NestedValue, R: _support::Reader<'de, _Value>> _support::Deserialize<'de, _Value, R> for Modules<_Value> {
    fn deserialize(r: &mut R) -> std::result::Result<Self, _support::ParseError> {
        r.open_dictionary()?;
        let mut _tmp2 = _support::B::Type::default();
        let mut _tmp1 = preserves::value::Map::new();
        while !r.close_compound(&mut _tmp2, &_support::B::Item::DictionaryKey)? {
            _tmp2.shift(Some(_support::B::Item::DictionaryKey));
            r.boundary(&_tmp2)?;
            let _tmp3 = ModulePath::deserialize(r)?;
            _tmp2.shift(Some(_support::B::Item::DictionaryValue));
            r.boundary(&_tmp2)?;
            let _tmp4 = Schema::deserialize(r)?;
            _tmp1.insert(_tmp3, _tmp4);
        }
        let _tmp0 = _tmp1;
        Ok(Modules(_tmp0))
    }
}

impl<
    'a,
    _L: Copy + Into<&'a crate::gen::Language<_Value>>,
    _Value: preserves::value::NestedValue + 'a
> _support::Parse<_L, _Value> for Modules<_Value> {
    fn parse(_ctxt: _L, value: &_Value) -> std::result::Result<Self, _support::ParseError> {
        let mut _tmp0 = preserves::value::Map::new();
        for (_tmp1, _tmp2) in value.value().to_dictionary()? {
            let _tmp3 = ModulePath::parse(_ctxt, _tmp1)?;
            let _tmp4 = Schema::parse(_ctxt, _tmp2)?;
            _tmp0.insert(_tmp3, _tmp4);
        }
        Ok(Modules(_tmp0))
    }
}

impl<
    'a,
    _L: Copy + Into<&'a crate::gen::Language<_Value>>,
    _Value: preserves::value::NestedValue + 'a
> _support::Unparse<_L, _Value> for Modules<_Value> {
    fn unparse(&self, _ctxt: _L) -> _Value {
        let Modules(_tmp0) = self;
        preserves::value::Value::Dictionary(_tmp0.iter().map(|(_tmp1, _tmp2)| (_tmp1.unparse(_ctxt), _tmp2.unparse(_ctxt))).collect()).wrap()
    }
}

#[derive(Debug, PartialOrd, Ord, PartialEq, Eq, Clone, Hash)]
pub struct NamedAlternative<_Value: preserves::value::NestedValue = preserves::value::IOValue> {
    pub variant_label: std::string::String,
    pub pattern: Pattern<_Value>
}

impl<_Value: preserves::value::NestedValue> preserves::value::Domain for NamedAlternative<_Value> {}

impl<'de, _Value: preserves::value::NestedValue, R: _support::Reader<'de, _Value>> _support::Deserialize<'de, _Value, R> for NamedAlternative<_Value> {
    fn deserialize(r: &mut R) -> std::result::Result<Self, _support::ParseError> {
        r.open_sequence()?;
        let mut _tmp0 = _support::B::Type::default();
        _tmp0.shift(Some(_support::B::Item::SequenceValue));
        r.boundary(&_tmp0)?;
        let _tmp1 = r.next_str()?.into_owned();
        _tmp0.shift(Some(_support::B::Item::SequenceValue));
        r.boundary(&_tmp0)?;
        let _tmp2 = Pattern::deserialize(r)?;
        r.ensure_complete(_tmp0, &_support::B::Item::SequenceValue)?;
        Ok(NamedAlternative {variant_label: _tmp1, pattern: _tmp2})
    }
}

impl<
    'a,
    _L: Copy + Into<&'a crate::gen::Language<_Value>>,
    _Value: preserves::value::NestedValue + 'a
> _support::Parse<_L, _Value> for NamedAlternative<_Value> {
    fn parse(_ctxt: _L, value: &_Value) -> std::result::Result<Self, _support::ParseError> {
        let _tmp0 = value.value().to_sequence()?;
        if _tmp0.len() < 2 { return Err(_support::ParseError::conformance_error("schema.NamedAlternative")); }
        let _tmp1 = (&_tmp0[0]).value().to_string()?;
        let _tmp2 = Pattern::parse(_ctxt, (&_tmp0[1]))?;
        Ok(NamedAlternative {variant_label: _tmp1.clone(), pattern: _tmp2})
    }
}

impl<
    'a,
    _L: Copy + Into<&'a crate::gen::Language<_Value>>,
    _Value: preserves::value::NestedValue + 'a
> _support::Unparse<_L, _Value> for NamedAlternative<_Value> {
    fn unparse(&self, _ctxt: _L) -> _Value {
        let NamedAlternative {variant_label: _tmp0, pattern: _tmp1} = self;
        {
            let mut _tmp2 = std::vec::Vec::new();
            _tmp2.push(preserves::value::Value::from(_tmp0).wrap());
            _tmp2.push(_tmp1.unparse(_ctxt));
            preserves::value::Value::Sequence(_tmp2).wrap()
        }
    }
}

#[derive(Debug, PartialOrd, Ord, PartialEq, Eq, Clone, Hash)]
pub enum NamedPattern<_Value: preserves::value::NestedValue = preserves::value::IOValue> {
    Named(std::boxed::Box<Binding<_Value>>),
    Anonymous(std::boxed::Box<Pattern<_Value>>)
}

impl<_Value: preserves::value::NestedValue> preserves::value::Domain for NamedPattern<_Value> {}

fn read_named_pattern_named<'de, _Value: preserves::value::NestedValue, R: _support::Reader<'de, _Value>>(r: &mut R) -> std::result::Result<NamedPattern<_Value>, _support::ParseError> {
    let _tmp0 = Binding::deserialize(r)?;
    Ok(NamedPattern::Named(std::boxed::Box::new(_tmp0)))
}

fn read_named_pattern_anonymous<'de, _Value: preserves::value::NestedValue, R: _support::Reader<'de, _Value>>(r: &mut R) -> std::result::Result<NamedPattern<_Value>, _support::ParseError> {
    let _tmp0 = Pattern::deserialize(r)?;
    Ok(NamedPattern::Anonymous(std::boxed::Box::new(_tmp0)))
}

impl<'de, _Value: preserves::value::NestedValue, R: _support::Reader<'de, _Value>> _support::Deserialize<'de, _Value, R> for NamedPattern<_Value> {
    fn deserialize(r: &mut R) -> std::result::Result<Self, _support::ParseError> {
        let _mark = r.mark()?;
        match read_named_pattern_named(r) { Err(e) if e.is_conformance_error() => r.restore(&_mark)?, result => return result }
        match read_named_pattern_anonymous(r) { Err(e) if e.is_conformance_error() => r.restore(&_mark)?, result => return result }
        Err(_support::ParseError::conformance_error("schema.NamedPattern"))
    }
}

fn parse_named_pattern_named<
    'a,
    _L: Copy + Into<&'a crate::gen::Language<_Value>>,
    _Value: preserves::value::NestedValue + 'a
>(_ctxt: _L, value: &_Value) -> std::result::Result<NamedPattern<_Value>, _support::ParseError> {
    let _tmp0 = Binding::parse(_ctxt, value)?;
    Ok(NamedPattern::Named(std::boxed::Box::new(_tmp0)))
}

fn parse_named_pattern_anonymous<
    'a,
    _L: Copy + Into<&'a crate::gen::Language<_Value>>,
    _Value: preserves::value::NestedValue + 'a
>(_ctxt: _L, value: &_Value) -> std::result::Result<NamedPattern<_Value>, _support::ParseError> {
    let _tmp0 = Pattern::parse(_ctxt, value)?;
    Ok(NamedPattern::Anonymous(std::boxed::Box::new(_tmp0)))
}

impl<
    'a,
    _L: Copy + Into<&'a crate::gen::Language<_Value>>,
    _Value: preserves::value::NestedValue + 'a
> _support::Parse<_L, _Value> for NamedPattern<_Value> {
    fn parse(_ctxt: _L, value: &_Value) -> std::result::Result<Self, _support::ParseError> {
        if let Ok(r) = parse_named_pattern_named(_ctxt, value) { return Ok(r); }
        if let Ok(r) = parse_named_pattern_anonymous(_ctxt, value) { return Ok(r); }
        Err(_support::ParseError::conformance_error("schema.NamedPattern"))
    }
}

impl<
    'a,
    _L: Copy + Into<&'a crate::gen::Language<_Value>>,
    _Value: preserves::value::NestedValue + 'a
> _support::Unparse<_L, _Value> for NamedPattern<_Value> {
    fn unparse(&self, _ctxt: _L) -> _Value {
        match self {
            NamedPattern::Named(_tmp0) => _tmp0.as_ref().unparse(_ctxt),
            NamedPattern::Anonymous(_tmp0) => _tmp0.as_ref().unparse(_ctxt),
        }
    }
}

#[derive(Debug, PartialOrd, Ord, PartialEq, Eq, Clone, Hash)]
pub enum NamedSimplePattern<_Value: preserves::value::NestedValue = preserves::value::IOValue> {
    Named(std::boxed::Box<Binding<_Value>>),
    Anonymous(std::boxed::Box<SimplePattern<_Value>>)
}

impl<_Value: preserves::value::NestedValue> preserves::value::Domain for NamedSimplePattern<_Value> {}

fn read_named_simple_pattern_named<'de, _Value: preserves::value::NestedValue, R: _support::Reader<'de, _Value>>(r: &mut R) -> std::result::Result<NamedSimplePattern<_Value>, _support::ParseError> {
    let _tmp0 = Binding::deserialize(r)?;
    Ok(NamedSimplePattern::Named(std::boxed::Box::new(_tmp0)))
}

fn read_named_simple_pattern_anonymous<'de, _Value: preserves::value::NestedValue, R: _support::Reader<'de, _Value>>(r: &mut R) -> std::result::Result<NamedSimplePattern<_Value>, _support::ParseError> {
    let _tmp0 = SimplePattern::deserialize(r)?;
    Ok(NamedSimplePattern::Anonymous(std::boxed::Box::new(_tmp0)))
}

impl<'de, _Value: preserves::value::NestedValue, R: _support::Reader<'de, _Value>> _support::Deserialize<'de, _Value, R> for NamedSimplePattern<_Value> {
    fn deserialize(r: &mut R) -> std::result::Result<Self, _support::ParseError> {
        let _mark = r.mark()?;
        match read_named_simple_pattern_named(r) { Err(e) if e.is_conformance_error() => r.restore(&_mark)?, result => return result }
        match read_named_simple_pattern_anonymous(r) { Err(e) if e.is_conformance_error() => r.restore(&_mark)?, result => return result }
        Err(_support::ParseError::conformance_error("schema.NamedSimplePattern"))
    }
}

fn parse_named_simple_pattern_named<
    'a,
    _L: Copy + Into<&'a crate::gen::Language<_Value>>,
    _Value: preserves::value::NestedValue + 'a
>(_ctxt: _L, value: &_Value) -> std::result::Result<NamedSimplePattern<_Value>, _support::ParseError> {
    let _tmp0 = Binding::parse(_ctxt, value)?;
    Ok(NamedSimplePattern::Named(std::boxed::Box::new(_tmp0)))
}

fn parse_named_simple_pattern_anonymous<
    'a,
    _L: Copy + Into<&'a crate::gen::Language<_Value>>,
    _Value: preserves::value::NestedValue + 'a
>(_ctxt: _L, value: &_Value) -> std::result::Result<NamedSimplePattern<_Value>, _support::ParseError> {
    let _tmp0 = SimplePattern::parse(_ctxt, value)?;
    Ok(NamedSimplePattern::Anonymous(std::boxed::Box::new(_tmp0)))
}

impl<
    'a,
    _L: Copy + Into<&'a crate::gen::Language<_Value>>,
    _Value: preserves::value::NestedValue + 'a
> _support::Parse<_L, _Value> for NamedSimplePattern<_Value> {
    fn parse(_ctxt: _L, value: &_Value) -> std::result::Result<Self, _support::ParseError> {
        if let Ok(r) = parse_named_simple_pattern_named(_ctxt, value) { return Ok(r); }
        if let Ok(r) = parse_named_simple_pattern_anonymous(_ctxt, value) { return Ok(r); }
        Err(_support::ParseError::conformance_error("schema.NamedSimplePattern"))
    }
}

impl<
    'a,
    _L: Copy + Into<&'a crate::gen::Language<_Value>>,
    _Value: preserves::value::NestedValue + 'a
> _support::Unparse<_L, _Value> for NamedSimplePattern<_Value> {
    fn unparse(&self, _ctxt: _L) -> _Value {
        match self {
            NamedSimplePattern::Named(_tmp0) => _tmp0.as_ref().unparse(_ctxt),
            NamedSimplePattern::Anonymous(_tmp0) => _tmp0.as_ref().unparse(_ctxt),
        }
    }
}

#[derive(Debug, PartialOrd, Ord, PartialEq, Eq, Clone, Hash)]
pub enum Pattern<_Value: preserves::value::NestedValue = preserves::value::IOValue> {
    SimplePattern(std::boxed::Box<SimplePattern<_Value>>),
    CompoundPattern(std::boxed::Box<CompoundPattern<_Value>>)
}

impl<_Value: preserves::value::NestedValue> preserves::value::Domain for Pattern<_Value> {}

fn read_pattern_simple_pattern<'de, _Value: preserves::value::NestedValue, R: _support::Reader<'de, _Value>>(r: &mut R) -> std::result::Result<Pattern<_Value>, _support::ParseError> {
    let _tmp0 = SimplePattern::deserialize(r)?;
    Ok(Pattern::SimplePattern(std::boxed::Box::new(_tmp0)))
}

fn read_pattern_compound_pattern<'de, _Value: preserves::value::NestedValue, R: _support::Reader<'de, _Value>>(r: &mut R) -> std::result::Result<Pattern<_Value>, _support::ParseError> {
    let _tmp0 = CompoundPattern::deserialize(r)?;
    Ok(Pattern::CompoundPattern(std::boxed::Box::new(_tmp0)))
}

impl<'de, _Value: preserves::value::NestedValue, R: _support::Reader<'de, _Value>> _support::Deserialize<'de, _Value, R> for Pattern<_Value> {
    fn deserialize(r: &mut R) -> std::result::Result<Self, _support::ParseError> {
        let _mark = r.mark()?;
        match read_pattern_simple_pattern(r) { Err(e) if e.is_conformance_error() => r.restore(&_mark)?, result => return result }
        match read_pattern_compound_pattern(r) { Err(e) if e.is_conformance_error() => r.restore(&_mark)?, result => return result }
        Err(_support::ParseError::conformance_error("schema.Pattern"))
    }
}

fn parse_pattern_simple_pattern<
    'a,
    _L: Copy + Into<&'a crate::gen::Language<_Value>>,
    _Value: preserves::value::NestedValue + 'a
>(_ctxt: _L, value: &_Value) -> std::result::Result<Pattern<_Value>, _support::ParseError> {
    let _tmp0 = SimplePattern::parse(_ctxt, value)?;
    Ok(Pattern::SimplePattern(std::boxed::Box::new(_tmp0)))
}

fn parse_pattern_compound_pattern<
    'a,
    _L: Copy + Into<&'a crate::gen::Language<_Value>>,
    _Value: preserves::value::NestedValue + 'a
>(_ctxt: _L, value: &_Value) -> std::result::Result<Pattern<_Value>, _support::ParseError> {
    let _tmp0 = CompoundPattern::parse(_ctxt, value)?;
    Ok(Pattern::CompoundPattern(std::boxed::Box::new(_tmp0)))
}

impl<
    'a,
    _L: Copy + Into<&'a crate::gen::Language<_Value>>,
    _Value: preserves::value::NestedValue + 'a
> _support::Parse<_L, _Value> for Pattern<_Value> {
    fn parse(_ctxt: _L, value: &_Value) -> std::result::Result<Self, _support::ParseError> {
        if let Ok(r) = parse_pattern_simple_pattern(_ctxt, value) { return Ok(r); }
        if let Ok(r) = parse_pattern_compound_pattern(_ctxt, value) { return Ok(r); }
        Err(_support::ParseError::conformance_error("schema.Pattern"))
    }
}

impl<
    'a,
    _L: Copy + Into<&'a crate::gen::Language<_Value>>,
    _Value: preserves::value::NestedValue + 'a
> _support::Unparse<_L, _Value> for Pattern<_Value> {
    fn unparse(&self, _ctxt: _L) -> _Value {
        match self {
            Pattern::SimplePattern(_tmp0) => _tmp0.as_ref().unparse(_ctxt),
            Pattern::CompoundPattern(_tmp0) => _tmp0.as_ref().unparse(_ctxt),
        }
    }
}

#[derive(Debug, PartialOrd, Ord, PartialEq, Eq, Clone, Hash)]
pub struct Ref {
    pub module: ModulePath,
    pub name: std::string::String
}

impl preserves::value::Domain for Ref {}

impl<'de, _Value: preserves::value::NestedValue, R: _support::Reader<'de, _Value>> _support::Deserialize<'de, _Value, R> for Ref {
    fn deserialize(r: &mut R) -> std::result::Result<Self, _support::ParseError> {
        r.open_record(None)?;
        let mut _tmp0 = _support::B::Type::default();
        _tmp0.shift(Some(_support::B::Item::RecordLabel));
        r.boundary(&_tmp0)?;
        match r.next_token(true)? {
            preserves::value::Token::Atom(v) => match v.value() {
                preserves::value::Value::Symbol(w) if w == "ref" => {}
                _ => return Err(_support::ParseError::conformance_error("schema.Ref"))?,
            }
            _ => return Err(_support::ParseError::conformance_error("schema.Ref"))?,
        }
        let _tmp1 = ();
        _tmp0.shift(Some(_support::B::Item::RecordField));
        r.boundary(&_tmp0)?;
        let _tmp2 = ModulePath::deserialize(r)?;
        _tmp0.shift(Some(_support::B::Item::RecordField));
        r.boundary(&_tmp0)?;
        let _tmp3 = r.next_symbol()?.into_owned();
        r.ensure_complete(_tmp0, &_support::B::Item::RecordField)?;
        Ok(Ref {module: _tmp2, name: _tmp3})
    }
}

impl<
    'a,
    _L: Copy + Into<&'a crate::gen::Language<_Value>>,
    _Value: preserves::value::NestedValue + 'a
> _support::Parse<_L, _Value> for Ref {
    fn parse(_ctxt: _L, value: &_Value) -> std::result::Result<Self, _support::ParseError> {
        let _tmp0 = value.value().to_record(None)?;
        if _tmp0.label() != &<_L as Into<&'a crate::gen::Language<_Value>>>::into(_ctxt).LIT_16_REF { return Err(_support::ParseError::conformance_error("schema.Ref")); }
        let _tmp1 = ();
        if _tmp0.fields().len() < 2 { return Err(_support::ParseError::conformance_error("schema.Ref")); }
        let _tmp2 = ModulePath::parse(_ctxt, (&_tmp0.fields()[0]))?;
        let _tmp3 = (&_tmp0.fields()[1]).value().to_symbol()?;
        Ok(Ref {module: _tmp2, name: _tmp3.clone()})
    }
}

impl<
    'a,
    _L: Copy + Into<&'a crate::gen::Language<_Value>>,
    _Value: preserves::value::NestedValue + 'a
> _support::Unparse<_L, _Value> for Ref {
    fn unparse(&self, _ctxt: _L) -> _Value {
        let Ref {module: _tmp0, name: _tmp1} = self;
        {
            let mut _tmp2 = preserves::value::Record(vec![(&<_L as Into<&'a crate::gen::Language<_Value>>>::into(_ctxt).LIT_16_REF).clone()]);
            _tmp2.fields_vec_mut().push(_tmp0.unparse(_ctxt));
            _tmp2.fields_vec_mut().push(preserves::value::Value::symbol(_tmp1).wrap());
            _tmp2.finish().wrap()
        }
    }
}

#[derive(Debug, PartialOrd, Ord, PartialEq, Eq, Clone, Hash)]
pub struct Schema<_Value: preserves::value::NestedValue = preserves::value::IOValue> {
    pub definitions: Definitions<_Value>,
    pub embedded_type: EmbeddedTypeName,
    pub version: Version
}

impl<_Value: preserves::value::NestedValue> preserves::value::Domain for Schema<_Value> {}

impl<'de, _Value: preserves::value::NestedValue, R: _support::Reader<'de, _Value>> _support::Deserialize<'de, _Value, R> for Schema<_Value> {
    fn deserialize(r: &mut R) -> std::result::Result<Self, _support::ParseError> {
        r.open_record(None)?;
        let mut _tmp0 = _support::B::Type::default();
        _tmp0.shift(Some(_support::B::Item::RecordLabel));
        r.boundary(&_tmp0)?;
        match r.next_token(true)? {
            preserves::value::Token::Atom(v) => match v.value() {
                preserves::value::Value::Symbol(w) if w == "schema" => {}
                _ => return Err(_support::ParseError::conformance_error("schema.Schema"))?,
            }
            _ => return Err(_support::ParseError::conformance_error("schema.Schema"))?,
        }
        let _tmp1 = ();
        _tmp0.shift(Some(_support::B::Item::RecordField));
        r.boundary(&_tmp0)?;
        r.open_dictionary()?;
        let mut _tmp2 = _support::B::Type::default();
        let mut _tmp3 = None;
        let mut _tmp4 = None;
        let mut _tmp5 = None;
        while !r.close_compound(&mut _tmp2, &_support::B::Item::DictionaryKey)? {
            _tmp2.shift(Some(_support::B::Item::DictionaryKey));
            r.boundary(&_tmp2)?;
            match r.next_token(true)? {
                preserves::value::Token::Atom(v) => match v.value() {
                    preserves::value::Value::Symbol(w) if w == "definitions" => {
                        _tmp2.shift(Some(_support::B::Item::DictionaryValue));
                        r.boundary(&_tmp2)?;
                        _tmp3 = Some(Definitions::deserialize(r)?);
                        continue;
                    }
                    preserves::value::Value::Symbol(w) if w == "embeddedType" => {
                        _tmp2.shift(Some(_support::B::Item::DictionaryValue));
                        r.boundary(&_tmp2)?;
                        _tmp4 = Some(EmbeddedTypeName::deserialize(r)?);
                        continue;
                    }
                    preserves::value::Value::Symbol(w) if w == "version" => {
                        _tmp2.shift(Some(_support::B::Item::DictionaryValue));
                        r.boundary(&_tmp2)?;
                        _tmp5 = Some(Version::deserialize(r)?);
                        continue;
                    }
                    _ => return Err(_support::ParseError::conformance_error("schema.Schema"))?,
                }
                _ => return Err(_support::ParseError::conformance_error("schema.Schema"))?,
            }
        }
        r.ensure_complete(_tmp0, &_support::B::Item::RecordField)?;
        Ok(Schema {
            definitions: _tmp3.ok_or_else(|| _support::ParseError::conformance_error("schema.Schema"))?,
            embedded_type: _tmp4.ok_or_else(|| _support::ParseError::conformance_error("schema.Schema"))?,
            version: _tmp5.ok_or_else(|| _support::ParseError::conformance_error("schema.Schema"))?
        })
    }
}

impl<
    'a,
    _L: Copy + Into<&'a crate::gen::Language<_Value>>,
    _Value: preserves::value::NestedValue + 'a
> _support::Parse<_L, _Value> for Schema<_Value> {
    fn parse(_ctxt: _L, value: &_Value) -> std::result::Result<Self, _support::ParseError> {
        let _tmp0 = value.value().to_record(None)?;
        if _tmp0.label() != &<_L as Into<&'a crate::gen::Language<_Value>>>::into(_ctxt).LIT_17_SCHEMA { return Err(_support::ParseError::conformance_error("schema.Schema")); }
        let _tmp1 = ();
        if _tmp0.fields().len() < 1 { return Err(_support::ParseError::conformance_error("schema.Schema")); }
        let _tmp2 = (&_tmp0.fields()[0]).value().to_dictionary()?;
        let _tmp3 = _tmp2.get(
            &<_L as Into<&'a crate::gen::Language<_Value>>>::into(_ctxt).LIT_18_DEFINITIONS
        ).ok_or_else(|| _support::ParseError::conformance_error("schema.Schema"))?;
        let _tmp4 = Definitions::parse(_ctxt, _tmp3)?;
        let _tmp5 = _tmp2.get(
            &<_L as Into<&'a crate::gen::Language<_Value>>>::into(_ctxt).LIT_19_EMBEDDED_TYPE
        ).ok_or_else(|| _support::ParseError::conformance_error("schema.Schema"))?;
        let _tmp6 = EmbeddedTypeName::parse(_ctxt, _tmp5)?;
        let _tmp7 = _tmp2.get(&<_L as Into<&'a crate::gen::Language<_Value>>>::into(_ctxt).LIT_20_VERSION).ok_or_else(|| _support::ParseError::conformance_error("schema.Schema"))?;
        let _tmp8 = Version::parse(_ctxt, _tmp7)?;
        Ok(Schema {definitions: _tmp4, embedded_type: _tmp6, version: _tmp8})
    }
}

impl<
    'a,
    _L: Copy + Into<&'a crate::gen::Language<_Value>>,
    _Value: preserves::value::NestedValue + 'a
> _support::Unparse<_L, _Value> for Schema<_Value> {
    fn unparse(&self, _ctxt: _L) -> _Value {
        let Schema {definitions: _tmp0, embedded_type: _tmp1, version: _tmp2} = self;
        {
            let mut _tmp3 = preserves::value::Record(vec![(&<_L as Into<&'a crate::gen::Language<_Value>>>::into(_ctxt).LIT_17_SCHEMA).clone()]);
            _tmp3.fields_vec_mut().push(
                {
                    let mut _tmp4 = preserves::value::Map::new();
                    _tmp4.insert(
                        (
                            &<_L as Into<&'a crate::gen::Language<_Value>>>::into(_ctxt).LIT_18_DEFINITIONS
                        ).clone(),
                        _tmp0.unparse(_ctxt)
                    );
                    _tmp4.insert(
                        (
                            &<_L as Into<&'a crate::gen::Language<_Value>>>::into(_ctxt).LIT_19_EMBEDDED_TYPE
                        ).clone(),
                        _tmp1.unparse(_ctxt)
                    );
                    _tmp4.insert(
                        (&<_L as Into<&'a crate::gen::Language<_Value>>>::into(_ctxt).LIT_20_VERSION).clone(),
                        _tmp2.unparse(_ctxt)
                    );
                    preserves::value::Value::Dictionary(_tmp4).wrap()
                }
            );
            _tmp3.finish().wrap()
        }
    }
}

#[derive(Debug, PartialOrd, Ord, PartialEq, Eq, Clone, Hash)]
pub enum SimplePattern<_Value: preserves::value::NestedValue = preserves::value::IOValue> {
    Any,
    Atom {
        atom_kind: std::boxed::Box<AtomKind>
    },
    Embedded {
        interface: std::boxed::Box<SimplePattern<_Value>>
    },
    Lit {
        value: _Value
    },
    Seqof {
        pattern: std::boxed::Box<SimplePattern<_Value>>
    },
    Setof {
        pattern: std::boxed::Box<SimplePattern<_Value>>
    },
    Dictof {
        key: std::boxed::Box<SimplePattern<_Value>>,
        value: std::boxed::Box<SimplePattern<_Value>>
    },
    Ref(std::boxed::Box<Ref>)
}

impl<_Value: preserves::value::NestedValue> preserves::value::Domain for SimplePattern<_Value> {}

fn read_simple_pattern_any<'de, _Value: preserves::value::NestedValue, R: _support::Reader<'de, _Value>>(r: &mut R) -> std::result::Result<SimplePattern<_Value>, _support::ParseError> {
    match r.next_token(true)? {
        preserves::value::Token::Atom(v) => match v.value() {
            preserves::value::Value::Symbol(w) if w == "any" => {}
            _ => return Err(_support::ParseError::conformance_error("schema.SimplePattern::any"))?,
        }
        _ => return Err(_support::ParseError::conformance_error("schema.SimplePattern::any"))?,
    }
    let _tmp0 = ();
    Ok(SimplePattern::Any)
}

fn read_simple_pattern_atom<'de, _Value: preserves::value::NestedValue, R: _support::Reader<'de, _Value>>(r: &mut R) -> std::result::Result<SimplePattern<_Value>, _support::ParseError> {
    r.open_record(None)?;
    let mut _tmp0 = _support::B::Type::default();
    _tmp0.shift(Some(_support::B::Item::RecordLabel));
    r.boundary(&_tmp0)?;
    match r.next_token(true)? {
        preserves::value::Token::Atom(v) => match v.value() {
            preserves::value::Value::Symbol(w) if w == "atom" => {}
            _ => return Err(_support::ParseError::conformance_error("schema.SimplePattern::atom"))?,
        }
        _ => return Err(_support::ParseError::conformance_error("schema.SimplePattern::atom"))?,
    }
    let _tmp1 = ();
    _tmp0.shift(Some(_support::B::Item::RecordField));
    r.boundary(&_tmp0)?;
    let _tmp2 = AtomKind::deserialize(r)?;
    r.ensure_complete(_tmp0, &_support::B::Item::RecordField)?;
    Ok(SimplePattern::Atom {atom_kind: std::boxed::Box::new(_tmp2)})
}

fn read_simple_pattern_embedded<'de, _Value: preserves::value::NestedValue, R: _support::Reader<'de, _Value>>(r: &mut R) -> std::result::Result<SimplePattern<_Value>, _support::ParseError> {
    r.open_record(None)?;
    let mut _tmp0 = _support::B::Type::default();
    _tmp0.shift(Some(_support::B::Item::RecordLabel));
    r.boundary(&_tmp0)?;
    match r.next_token(true)? {
        preserves::value::Token::Atom(v) => match v.value() {
            preserves::value::Value::Symbol(w) if w == "embedded" => {}
            _ => return Err(_support::ParseError::conformance_error("schema.SimplePattern::embedded"))?,
        }
        _ => return Err(_support::ParseError::conformance_error("schema.SimplePattern::embedded"))?,
    }
    let _tmp1 = ();
    _tmp0.shift(Some(_support::B::Item::RecordField));
    r.boundary(&_tmp0)?;
    let _tmp2 = SimplePattern::deserialize(r)?;
    r.ensure_complete(_tmp0, &_support::B::Item::RecordField)?;
    Ok(SimplePattern::Embedded {interface: std::boxed::Box::new(_tmp2)})
}

fn read_simple_pattern_lit<'de, _Value: preserves::value::NestedValue, R: _support::Reader<'de, _Value>>(r: &mut R) -> std::result::Result<SimplePattern<_Value>, _support::ParseError> {
    r.open_record(None)?;
    let mut _tmp0 = _support::B::Type::default();
    _tmp0.shift(Some(_support::B::Item::RecordLabel));
    r.boundary(&_tmp0)?;
    match r.next_token(true)? {
        preserves::value::Token::Atom(v) => match v.value() {
            preserves::value::Value::Symbol(w) if w == "lit" => {}
            _ => return Err(_support::ParseError::conformance_error("schema.SimplePattern::lit"))?,
        }
        _ => return Err(_support::ParseError::conformance_error("schema.SimplePattern::lit"))?,
    }
    let _tmp1 = ();
    _tmp0.shift(Some(_support::B::Item::RecordField));
    r.boundary(&_tmp0)?;
    let _tmp2 = r.demand_next(true)?;
    r.ensure_complete(_tmp0, &_support::B::Item::RecordField)?;
    Ok(SimplePattern::Lit {value: _tmp2})
}

fn read_simple_pattern_seqof<'de, _Value: preserves::value::NestedValue, R: _support::Reader<'de, _Value>>(r: &mut R) -> std::result::Result<SimplePattern<_Value>, _support::ParseError> {
    r.open_record(None)?;
    let mut _tmp0 = _support::B::Type::default();
    _tmp0.shift(Some(_support::B::Item::RecordLabel));
    r.boundary(&_tmp0)?;
    match r.next_token(true)? {
        preserves::value::Token::Atom(v) => match v.value() {
            preserves::value::Value::Symbol(w) if w == "seqof" => {}
            _ => return Err(_support::ParseError::conformance_error("schema.SimplePattern::seqof"))?,
        }
        _ => return Err(_support::ParseError::conformance_error("schema.SimplePattern::seqof"))?,
    }
    let _tmp1 = ();
    _tmp0.shift(Some(_support::B::Item::RecordField));
    r.boundary(&_tmp0)?;
    let _tmp2 = SimplePattern::deserialize(r)?;
    r.ensure_complete(_tmp0, &_support::B::Item::RecordField)?;
    Ok(SimplePattern::Seqof {pattern: std::boxed::Box::new(_tmp2)})
}

fn read_simple_pattern_setof<'de, _Value: preserves::value::NestedValue, R: _support::Reader<'de, _Value>>(r: &mut R) -> std::result::Result<SimplePattern<_Value>, _support::ParseError> {
    r.open_record(None)?;
    let mut _tmp0 = _support::B::Type::default();
    _tmp0.shift(Some(_support::B::Item::RecordLabel));
    r.boundary(&_tmp0)?;
    match r.next_token(true)? {
        preserves::value::Token::Atom(v) => match v.value() {
            preserves::value::Value::Symbol(w) if w == "setof" => {}
            _ => return Err(_support::ParseError::conformance_error("schema.SimplePattern::setof"))?,
        }
        _ => return Err(_support::ParseError::conformance_error("schema.SimplePattern::setof"))?,
    }
    let _tmp1 = ();
    _tmp0.shift(Some(_support::B::Item::RecordField));
    r.boundary(&_tmp0)?;
    let _tmp2 = SimplePattern::deserialize(r)?;
    r.ensure_complete(_tmp0, &_support::B::Item::RecordField)?;
    Ok(SimplePattern::Setof {pattern: std::boxed::Box::new(_tmp2)})
}

fn read_simple_pattern_dictof<'de, _Value: preserves::value::NestedValue, R: _support::Reader<'de, _Value>>(r: &mut R) -> std::result::Result<SimplePattern<_Value>, _support::ParseError> {
    r.open_record(None)?;
    let mut _tmp0 = _support::B::Type::default();
    _tmp0.shift(Some(_support::B::Item::RecordLabel));
    r.boundary(&_tmp0)?;
    match r.next_token(true)? {
        preserves::value::Token::Atom(v) => match v.value() {
            preserves::value::Value::Symbol(w) if w == "dictof" => {}
            _ => return Err(_support::ParseError::conformance_error("schema.SimplePattern::dictof"))?,
        }
        _ => return Err(_support::ParseError::conformance_error("schema.SimplePattern::dictof"))?,
    }
    let _tmp1 = ();
    _tmp0.shift(Some(_support::B::Item::RecordField));
    r.boundary(&_tmp0)?;
    let _tmp2 = SimplePattern::deserialize(r)?;
    _tmp0.shift(Some(_support::B::Item::RecordField));
    r.boundary(&_tmp0)?;
    let _tmp3 = SimplePattern::deserialize(r)?;
    r.ensure_complete(_tmp0, &_support::B::Item::RecordField)?;
    Ok(SimplePattern::Dictof {key: std::boxed::Box::new(_tmp2), value: std::boxed::Box::new(_tmp3)})
}

fn read_simple_pattern_ref<'de, _Value: preserves::value::NestedValue, R: _support::Reader<'de, _Value>>(r: &mut R) -> std::result::Result<SimplePattern<_Value>, _support::ParseError> {
    let _tmp0 = Ref::deserialize(r)?;
    Ok(SimplePattern::Ref(std::boxed::Box::new(_tmp0)))
}

impl<'de, _Value: preserves::value::NestedValue, R: _support::Reader<'de, _Value>> _support::Deserialize<'de, _Value, R> for SimplePattern<_Value> {
    fn deserialize(r: &mut R) -> std::result::Result<Self, _support::ParseError> {
        let _mark = r.mark()?;
        match read_simple_pattern_any(r) { Err(e) if e.is_conformance_error() => r.restore(&_mark)?, result => return result }
        match read_simple_pattern_atom(r) { Err(e) if e.is_conformance_error() => r.restore(&_mark)?, result => return result }
        match read_simple_pattern_embedded(r) { Err(e) if e.is_conformance_error() => r.restore(&_mark)?, result => return result }
        match read_simple_pattern_lit(r) { Err(e) if e.is_conformance_error() => r.restore(&_mark)?, result => return result }
        match read_simple_pattern_seqof(r) { Err(e) if e.is_conformance_error() => r.restore(&_mark)?, result => return result }
        match read_simple_pattern_setof(r) { Err(e) if e.is_conformance_error() => r.restore(&_mark)?, result => return result }
        match read_simple_pattern_dictof(r) { Err(e) if e.is_conformance_error() => r.restore(&_mark)?, result => return result }
        match read_simple_pattern_ref(r) { Err(e) if e.is_conformance_error() => r.restore(&_mark)?, result => return result }
        Err(_support::ParseError::conformance_error("schema.SimplePattern"))
    }
}

fn parse_simple_pattern_any<
    'a,
    _L: Copy + Into<&'a crate::gen::Language<_Value>>,
    _Value: preserves::value::NestedValue + 'a
>(_ctxt: _L, value: &_Value) -> std::result::Result<SimplePattern<_Value>, _support::ParseError> {
    if value != &<_L as Into<&'a crate::gen::Language<_Value>>>::into(_ctxt).LIT_21_ANY { return Err(_support::ParseError::conformance_error("schema.SimplePattern::any")); }
    let _tmp0 = ();
    Ok(SimplePattern::Any)
}

fn parse_simple_pattern_atom<
    'a,
    _L: Copy + Into<&'a crate::gen::Language<_Value>>,
    _Value: preserves::value::NestedValue + 'a
>(_ctxt: _L, value: &_Value) -> std::result::Result<SimplePattern<_Value>, _support::ParseError> {
    let _tmp0 = value.value().to_record(None)?;
    if _tmp0.label() != &<_L as Into<&'a crate::gen::Language<_Value>>>::into(_ctxt).LIT_22_ATOM { return Err(_support::ParseError::conformance_error("schema.SimplePattern::atom")); }
    let _tmp1 = ();
    if _tmp0.fields().len() < 1 { return Err(_support::ParseError::conformance_error("schema.SimplePattern::atom")); }
    let _tmp2 = AtomKind::parse(_ctxt, (&_tmp0.fields()[0]))?;
    Ok(SimplePattern::Atom {atom_kind: std::boxed::Box::new(_tmp2)})
}

fn parse_simple_pattern_embedded<
    'a,
    _L: Copy + Into<&'a crate::gen::Language<_Value>>,
    _Value: preserves::value::NestedValue + 'a
>(_ctxt: _L, value: &_Value) -> std::result::Result<SimplePattern<_Value>, _support::ParseError> {
    let _tmp0 = value.value().to_record(None)?;
    if _tmp0.label() != &<_L as Into<&'a crate::gen::Language<_Value>>>::into(_ctxt).LIT_23_EMBEDDED { return Err(_support::ParseError::conformance_error("schema.SimplePattern::embedded")); }
    let _tmp1 = ();
    if _tmp0.fields().len() < 1 { return Err(_support::ParseError::conformance_error("schema.SimplePattern::embedded")); }
    let _tmp2 = SimplePattern::parse(_ctxt, (&_tmp0.fields()[0]))?;
    Ok(SimplePattern::Embedded {interface: std::boxed::Box::new(_tmp2)})
}

fn parse_simple_pattern_lit<
    'a,
    _L: Copy + Into<&'a crate::gen::Language<_Value>>,
    _Value: preserves::value::NestedValue + 'a
>(_ctxt: _L, value: &_Value) -> std::result::Result<SimplePattern<_Value>, _support::ParseError> {
    let _tmp0 = value.value().to_record(None)?;
    if _tmp0.label() != &<_L as Into<&'a crate::gen::Language<_Value>>>::into(_ctxt).LIT_24_LIT { return Err(_support::ParseError::conformance_error("schema.SimplePattern::lit")); }
    let _tmp1 = ();
    if _tmp0.fields().len() < 1 { return Err(_support::ParseError::conformance_error("schema.SimplePattern::lit")); }
    let _tmp2 = (&_tmp0.fields()[0]);
    Ok(SimplePattern::Lit {value: _tmp2.clone()})
}

fn parse_simple_pattern_seqof<
    'a,
    _L: Copy + Into<&'a crate::gen::Language<_Value>>,
    _Value: preserves::value::NestedValue + 'a
>(_ctxt: _L, value: &_Value) -> std::result::Result<SimplePattern<_Value>, _support::ParseError> {
    let _tmp0 = value.value().to_record(None)?;
    if _tmp0.label() != &<_L as Into<&'a crate::gen::Language<_Value>>>::into(_ctxt).LIT_25_SEQOF { return Err(_support::ParseError::conformance_error("schema.SimplePattern::seqof")); }
    let _tmp1 = ();
    if _tmp0.fields().len() < 1 { return Err(_support::ParseError::conformance_error("schema.SimplePattern::seqof")); }
    let _tmp2 = SimplePattern::parse(_ctxt, (&_tmp0.fields()[0]))?;
    Ok(SimplePattern::Seqof {pattern: std::boxed::Box::new(_tmp2)})
}

fn parse_simple_pattern_setof<
    'a,
    _L: Copy + Into<&'a crate::gen::Language<_Value>>,
    _Value: preserves::value::NestedValue + 'a
>(_ctxt: _L, value: &_Value) -> std::result::Result<SimplePattern<_Value>, _support::ParseError> {
    let _tmp0 = value.value().to_record(None)?;
    if _tmp0.label() != &<_L as Into<&'a crate::gen::Language<_Value>>>::into(_ctxt).LIT_26_SETOF { return Err(_support::ParseError::conformance_error("schema.SimplePattern::setof")); }
    let _tmp1 = ();
    if _tmp0.fields().len() < 1 { return Err(_support::ParseError::conformance_error("schema.SimplePattern::setof")); }
    let _tmp2 = SimplePattern::parse(_ctxt, (&_tmp0.fields()[0]))?;
    Ok(SimplePattern::Setof {pattern: std::boxed::Box::new(_tmp2)})
}

fn parse_simple_pattern_dictof<
    'a,
    _L: Copy + Into<&'a crate::gen::Language<_Value>>,
    _Value: preserves::value::NestedValue + 'a
>(_ctxt: _L, value: &_Value) -> std::result::Result<SimplePattern<_Value>, _support::ParseError> {
    let _tmp0 = value.value().to_record(None)?;
    if _tmp0.label() != &<_L as Into<&'a crate::gen::Language<_Value>>>::into(_ctxt).LIT_27_DICTOF { return Err(_support::ParseError::conformance_error("schema.SimplePattern::dictof")); }
    let _tmp1 = ();
    if _tmp0.fields().len() < 2 { return Err(_support::ParseError::conformance_error("schema.SimplePattern::dictof")); }
    let _tmp2 = SimplePattern::parse(_ctxt, (&_tmp0.fields()[0]))?;
    let _tmp3 = SimplePattern::parse(_ctxt, (&_tmp0.fields()[1]))?;
    Ok(SimplePattern::Dictof {key: std::boxed::Box::new(_tmp2), value: std::boxed::Box::new(_tmp3)})
}

fn parse_simple_pattern_ref<
    'a,
    _L: Copy + Into<&'a crate::gen::Language<_Value>>,
    _Value: preserves::value::NestedValue + 'a
>(_ctxt: _L, value: &_Value) -> std::result::Result<SimplePattern<_Value>, _support::ParseError> {
    let _tmp0 = Ref::parse(_ctxt, value)?;
    Ok(SimplePattern::Ref(std::boxed::Box::new(_tmp0)))
}

impl<
    'a,
    _L: Copy + Into<&'a crate::gen::Language<_Value>>,
    _Value: preserves::value::NestedValue + 'a
> _support::Parse<_L, _Value> for SimplePattern<_Value> {
    fn parse(_ctxt: _L, value: &_Value) -> std::result::Result<Self, _support::ParseError> {
        if let Ok(r) = parse_simple_pattern_any(_ctxt, value) { return Ok(r); }
        if let Ok(r) = parse_simple_pattern_atom(_ctxt, value) { return Ok(r); }
        if let Ok(r) = parse_simple_pattern_embedded(_ctxt, value) { return Ok(r); }
        if let Ok(r) = parse_simple_pattern_lit(_ctxt, value) { return Ok(r); }
        if let Ok(r) = parse_simple_pattern_seqof(_ctxt, value) { return Ok(r); }
        if let Ok(r) = parse_simple_pattern_setof(_ctxt, value) { return Ok(r); }
        if let Ok(r) = parse_simple_pattern_dictof(_ctxt, value) { return Ok(r); }
        if let Ok(r) = parse_simple_pattern_ref(_ctxt, value) { return Ok(r); }
        Err(_support::ParseError::conformance_error("schema.SimplePattern"))
    }
}

impl<
    'a,
    _L: Copy + Into<&'a crate::gen::Language<_Value>>,
    _Value: preserves::value::NestedValue + 'a
> _support::Unparse<_L, _Value> for SimplePattern<_Value> {
    fn unparse(&self, _ctxt: _L) -> _Value {
        match self {
            SimplePattern::Any => (&<_L as Into<&'a crate::gen::Language<_Value>>>::into(_ctxt).LIT_21_ANY).clone(),
            SimplePattern::Atom {atom_kind: _tmp0} => {
                let mut _tmp1 = preserves::value::Record(vec![(&<_L as Into<&'a crate::gen::Language<_Value>>>::into(_ctxt).LIT_22_ATOM).clone()]);
                _tmp1.fields_vec_mut().push(_tmp0.as_ref().unparse(_ctxt));
                _tmp1.finish().wrap()
            },
            SimplePattern::Embedded {interface: _tmp0} => {
                let mut _tmp1 = preserves::value::Record(vec![(&<_L as Into<&'a crate::gen::Language<_Value>>>::into(_ctxt).LIT_23_EMBEDDED).clone()]);
                _tmp1.fields_vec_mut().push(_tmp0.as_ref().unparse(_ctxt));
                _tmp1.finish().wrap()
            },
            SimplePattern::Lit {value: _tmp0} => {
                let mut _tmp1 = preserves::value::Record(vec![(&<_L as Into<&'a crate::gen::Language<_Value>>>::into(_ctxt).LIT_24_LIT).clone()]);
                _tmp1.fields_vec_mut().push(_tmp0.clone());
                _tmp1.finish().wrap()
            },
            SimplePattern::Seqof {pattern: _tmp0} => {
                let mut _tmp1 = preserves::value::Record(vec![(&<_L as Into<&'a crate::gen::Language<_Value>>>::into(_ctxt).LIT_25_SEQOF).clone()]);
                _tmp1.fields_vec_mut().push(_tmp0.as_ref().unparse(_ctxt));
                _tmp1.finish().wrap()
            },
            SimplePattern::Setof {pattern: _tmp0} => {
                let mut _tmp1 = preserves::value::Record(vec![(&<_L as Into<&'a crate::gen::Language<_Value>>>::into(_ctxt).LIT_26_SETOF).clone()]);
                _tmp1.fields_vec_mut().push(_tmp0.as_ref().unparse(_ctxt));
                _tmp1.finish().wrap()
            },
            SimplePattern::Dictof {key: _tmp0, value: _tmp1} => {
                let mut _tmp2 = preserves::value::Record(vec![(&<_L as Into<&'a crate::gen::Language<_Value>>>::into(_ctxt).LIT_27_DICTOF).clone()]);
                _tmp2.fields_vec_mut().push(_tmp0.as_ref().unparse(_ctxt));
                _tmp2.fields_vec_mut().push(_tmp1.as_ref().unparse(_ctxt));
                _tmp2.finish().wrap()
            },
            SimplePattern::Ref(_tmp0) => _tmp0.as_ref().unparse(_ctxt),
        }
    }
}

#[derive(Debug, PartialOrd, Ord, PartialEq, Eq, Clone, Hash)]
pub struct Version;

impl preserves::value::Domain for Version {}

impl<'de, _Value: preserves::value::NestedValue, R: _support::Reader<'de, _Value>> _support::Deserialize<'de, _Value, R> for Version {
    fn deserialize(r: &mut R) -> std::result::Result<Self, _support::ParseError> {
        match r.next_token(true)? {
            preserves::value::Token::Atom(v) => match v.value() {
                preserves::value::Value::SignedInteger(w) if *w == (1).into() => {}
                _ => return Err(_support::ParseError::conformance_error("schema.Version"))?,
            }
            _ => return Err(_support::ParseError::conformance_error("schema.Version"))?,
        }
        let _tmp0 = ();
        Ok(Version)
    }
}

impl<
    'a,
    _L: Copy + Into<&'a crate::gen::Language<_Value>>,
    _Value: preserves::value::NestedValue + 'a
> _support::Parse<_L, _Value> for Version {
    fn parse(_ctxt: _L, value: &_Value) -> std::result::Result<Self, _support::ParseError> {
        if value != &<_L as Into<&'a crate::gen::Language<_Value>>>::into(_ctxt).LIT_28_1 { return Err(_support::ParseError::conformance_error("schema.Version")); }
        let _tmp0 = ();
        Ok(Version)
    }
}

impl<
    'a,
    _L: Copy + Into<&'a crate::gen::Language<_Value>>,
    _Value: preserves::value::NestedValue + 'a
> _support::Unparse<_L, _Value> for Version {
    fn unparse(&self, _ctxt: _L) -> _Value {
        let Version = self;
        (&<_L as Into<&'a crate::gen::Language<_Value>>>::into(_ctxt).LIT_28_1).clone()
    }
}
