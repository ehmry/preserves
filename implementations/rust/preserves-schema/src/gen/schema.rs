#![allow(unused_parens)]
#![allow(unused_imports)]

use std::convert::TryFrom;
use preserves::value::Domain;
use preserves::value::NestedValue;
use crate::support as _support;
use _support::Deserialize;

mod _any_ {
    use super::_Any;
    use crate::support as _support;
    _support::lazy_static! {
        pub static ref LIT_15_FALSE: _Any = /* #f */ _support::decode_lit(&vec![128]).unwrap();
        pub static ref LIT_28_1: _Any = /* 1 */ _support::decode_lit(&vec![145]).unwrap();
        pub static ref LIT_0_BOOLEAN: _Any = /* Boolean */ _support::decode_lit(&vec![179, 7, 66, 111, 111, 108, 101, 97, 110]).unwrap();
        pub static ref LIT_5_BYTE_STRING: _Any = /* ByteString */ _support::decode_lit(&vec![179, 10, 66, 121, 116, 101, 83, 116, 114, 105, 110, 103]).unwrap();
        pub static ref LIT_2_DOUBLE: _Any = /* Double */ _support::decode_lit(&vec![179, 6, 68, 111, 117, 98, 108, 101]).unwrap();
        pub static ref LIT_1_FLOAT: _Any = /* Float */ _support::decode_lit(&vec![179, 5, 70, 108, 111, 97, 116]).unwrap();
        pub static ref LIT_3_SIGNED_INTEGER: _Any = /* SignedInteger */ _support::decode_lit(&vec![179, 13, 83, 105, 103, 110, 101, 100, 73, 110, 116, 101, 103, 101, 114]).unwrap();
        pub static ref LIT_4_STRING: _Any = /* String */ _support::decode_lit(&vec![179, 6, 83, 116, 114, 105, 110, 103]).unwrap();
        pub static ref LIT_6_SYMBOL: _Any = /* Symbol */ _support::decode_lit(&vec![179, 6, 83, 121, 109, 98, 111, 108]).unwrap();
        pub static ref LIT_14_AND: _Any = /* and */ _support::decode_lit(&vec![179, 3, 97, 110, 100]).unwrap();
        pub static ref LIT_21_ANY: _Any = /* any */ _support::decode_lit(&vec![179, 3, 97, 110, 121]).unwrap();
        pub static ref LIT_22_ATOM: _Any = /* atom */ _support::decode_lit(&vec![179, 4, 97, 116, 111, 109]).unwrap();
        pub static ref LIT_8_BUNDLE: _Any = /* bundle */ _support::decode_lit(&vec![179, 6, 98, 117, 110, 100, 108, 101]).unwrap();
        pub static ref LIT_18_DEFINITIONS: _Any = /* definitions */ _support::decode_lit(&vec![179, 11, 100, 101, 102, 105, 110, 105, 116, 105, 111, 110, 115]).unwrap();
        pub static ref LIT_12_DICT: _Any = /* dict */ _support::decode_lit(&vec![179, 4, 100, 105, 99, 116]).unwrap();
        pub static ref LIT_27_DICTOF: _Any = /* dictof */ _support::decode_lit(&vec![179, 6, 100, 105, 99, 116, 111, 102]).unwrap();
        pub static ref LIT_23_EMBEDDED: _Any = /* embedded */ _support::decode_lit(&vec![179, 8, 101, 109, 98, 101, 100, 100, 101, 100]).unwrap();
        pub static ref LIT_19_EMBEDDED_TYPE: _Any = /* embeddedType */ _support::decode_lit(&vec![179, 12, 101, 109, 98, 101, 100, 100, 101, 100, 84, 121, 112, 101]).unwrap();
        pub static ref LIT_24_LIT: _Any = /* lit */ _support::decode_lit(&vec![179, 3, 108, 105, 116]).unwrap();
        pub static ref LIT_7_NAMED: _Any = /* named */ _support::decode_lit(&vec![179, 5, 110, 97, 109, 101, 100]).unwrap();
        pub static ref LIT_13_OR: _Any = /* or */ _support::decode_lit(&vec![179, 2, 111, 114]).unwrap();
        pub static ref LIT_9_REC: _Any = /* rec */ _support::decode_lit(&vec![179, 3, 114, 101, 99]).unwrap();
        pub static ref LIT_16_REF: _Any = /* ref */ _support::decode_lit(&vec![179, 3, 114, 101, 102]).unwrap();
        pub static ref LIT_17_SCHEMA: _Any = /* schema */ _support::decode_lit(&vec![179, 6, 115, 99, 104, 101, 109, 97]).unwrap();
        pub static ref LIT_25_SEQOF: _Any = /* seqof */ _support::decode_lit(&vec![179, 5, 115, 101, 113, 111, 102]).unwrap();
        pub static ref LIT_26_SETOF: _Any = /* setof */ _support::decode_lit(&vec![179, 5, 115, 101, 116, 111, 102]).unwrap();
        pub static ref LIT_10_TUPLE: _Any = /* tuple */ _support::decode_lit(&vec![179, 5, 116, 117, 112, 108, 101]).unwrap();
        pub static ref LIT_11_TUPLE_PREFIX: _Any = /* tuplePrefix */ _support::decode_lit(&vec![179, 11, 116, 117, 112, 108, 101, 80, 114, 101, 102, 105, 120]).unwrap();
        pub static ref LIT_20_VERSION: _Any = /* version */ _support::decode_lit(&vec![179, 7, 118, 101, 114, 115, 105, 111, 110]).unwrap();
    }
}

pub type _Ptr = preserves::value::IOValue;

pub type _Any = preserves::value::IOValue;

#[derive(Debug, PartialOrd, Ord, PartialEq, Eq, Clone, Hash)]
pub enum AtomKind {Boolean, Float, Double, SignedInteger, String, ByteString, Symbol}

impl preserves::value::Domain for AtomKind {}

#[derive(Debug, PartialOrd, Ord, PartialEq, Eq, Clone, Hash)]
pub struct Binding {pub name: std::string::String, pub pattern: SimplePattern}

impl preserves::value::Domain for Binding {}

#[derive(Debug, PartialOrd, Ord, PartialEq, Eq, Clone, Hash)]
pub struct Bundle {pub modules: Modules}

impl preserves::value::Domain for Bundle {}

#[derive(Debug, PartialOrd, Ord, PartialEq, Eq, Clone, Hash)]
pub enum CompoundPattern {
    Rec {label: std::boxed::Box<NamedPattern>, fields: std::boxed::Box<NamedPattern>},
    Tuple {patterns: std::vec::Vec<NamedPattern>},
    TuplePrefix {
        fixed: std::vec::Vec<NamedPattern>,
        variable: std::boxed::Box<NamedSimplePattern>
    },
    Dict {entries: std::boxed::Box<DictionaryEntries>}
}

impl preserves::value::Domain for CompoundPattern {}

#[derive(Debug, PartialOrd, Ord, PartialEq, Eq, Clone, Hash)]
pub enum Definition {
    Or {
        pattern_0: std::boxed::Box<NamedAlternative>,
        pattern_1: std::boxed::Box<NamedAlternative>,
        pattern_n: std::vec::Vec<NamedAlternative>
    },
    And {
        pattern_0: std::boxed::Box<NamedPattern>,
        pattern_1: std::boxed::Box<NamedPattern>,
        pattern_n: std::vec::Vec<NamedPattern>
    },
    Pattern(std::boxed::Box<Pattern>)
}

impl preserves::value::Domain for Definition {}

#[derive(Debug, PartialOrd, Ord, PartialEq, Eq, Clone, Hash)]
pub struct Definitions(pub preserves::value::Map<std::string::String, Definition>);

impl preserves::value::Domain for Definitions {}

#[derive(Debug, PartialOrd, Ord, PartialEq, Eq, Clone, Hash)]
pub struct DictionaryEntries(pub preserves::value::Map<_Any, NamedSimplePattern>);

impl preserves::value::Domain for DictionaryEntries {}

#[derive(Debug, PartialOrd, Ord, PartialEq, Eq, Clone, Hash)]
pub enum EmbeddedTypeName {Ref(std::boxed::Box<Ref>), False}

impl preserves::value::Domain for EmbeddedTypeName {}

#[derive(Debug, PartialOrd, Ord, PartialEq, Eq, Clone, Hash)]
pub struct ModulePath(pub std::vec::Vec<std::string::String>);

impl preserves::value::Domain for ModulePath {}

#[derive(Debug, PartialOrd, Ord, PartialEq, Eq, Clone, Hash)]
pub struct Modules(pub preserves::value::Map<ModulePath, Schema>);

impl preserves::value::Domain for Modules {}

#[derive(Debug, PartialOrd, Ord, PartialEq, Eq, Clone, Hash)]
pub struct NamedAlternative {pub variant_label: std::string::String, pub pattern: Pattern}

impl preserves::value::Domain for NamedAlternative {}

#[derive(Debug, PartialOrd, Ord, PartialEq, Eq, Clone, Hash)]
pub enum NamedPattern {Named(std::boxed::Box<Binding>), Anonymous(std::boxed::Box<Pattern>)}

impl preserves::value::Domain for NamedPattern {}

#[derive(Debug, PartialOrd, Ord, PartialEq, Eq, Clone, Hash)]
pub enum NamedSimplePattern {Named(std::boxed::Box<Binding>), Anonymous(std::boxed::Box<SimplePattern>)}

impl preserves::value::Domain for NamedSimplePattern {}

#[derive(Debug, PartialOrd, Ord, PartialEq, Eq, Clone, Hash)]
pub enum Pattern {
    SimplePattern(std::boxed::Box<SimplePattern>),
    CompoundPattern(std::boxed::Box<CompoundPattern>)
}

impl preserves::value::Domain for Pattern {}

#[derive(Debug, PartialOrd, Ord, PartialEq, Eq, Clone, Hash)]
pub struct Ref {pub module: ModulePath, pub name: std::string::String}

impl preserves::value::Domain for Ref {}

#[derive(Debug, PartialOrd, Ord, PartialEq, Eq, Clone, Hash)]
pub struct Schema {
    pub definitions: Definitions,
    pub embedded_type: EmbeddedTypeName,
    pub version: Version
}

impl preserves::value::Domain for Schema {}

#[derive(Debug, PartialOrd, Ord, PartialEq, Eq, Clone, Hash)]
pub enum SimplePattern {
    Any,
    Atom {atom_kind: std::boxed::Box<AtomKind>},
    Embedded {interface: std::boxed::Box<SimplePattern>},
    Lit {value: _Any},
    Seqof {pattern: std::boxed::Box<SimplePattern>},
    Setof {pattern: std::boxed::Box<SimplePattern>},
    Dictof {key: std::boxed::Box<SimplePattern>, value: std::boxed::Box<SimplePattern>},
    Ref(std::boxed::Box<Ref>)
}

impl preserves::value::Domain for SimplePattern {}

#[derive(Debug, PartialOrd, Ord, PartialEq, Eq, Clone, Hash)]
pub struct Version;

impl preserves::value::Domain for Version {}

fn _any_parse_atom_kind_boolean(value: &_Any) -> std::result::Result<AtomKind, _support::ParseError> {
    if value != &*_any_::LIT_0_BOOLEAN { return Err(_support::ParseError::conformance_error("schema.AtomKind::Boolean")); }
    let _tmp0 = ();
    Ok(AtomKind::Boolean)
}

fn _any_parse_atom_kind_float(value: &_Any) -> std::result::Result<AtomKind, _support::ParseError> {
    if value != &*_any_::LIT_1_FLOAT { return Err(_support::ParseError::conformance_error("schema.AtomKind::Float")); }
    let _tmp0 = ();
    Ok(AtomKind::Float)
}

fn _any_parse_atom_kind_double(value: &_Any) -> std::result::Result<AtomKind, _support::ParseError> {
    if value != &*_any_::LIT_2_DOUBLE { return Err(_support::ParseError::conformance_error("schema.AtomKind::Double")); }
    let _tmp0 = ();
    Ok(AtomKind::Double)
}

fn _any_parse_atom_kind_signed_integer(value: &_Any) -> std::result::Result<AtomKind, _support::ParseError> {
    if value != &*_any_::LIT_3_SIGNED_INTEGER { return Err(_support::ParseError::conformance_error("schema.AtomKind::SignedInteger")); }
    let _tmp0 = ();
    Ok(AtomKind::SignedInteger)
}

fn _any_parse_atom_kind_string(value: &_Any) -> std::result::Result<AtomKind, _support::ParseError> {
    if value != &*_any_::LIT_4_STRING { return Err(_support::ParseError::conformance_error("schema.AtomKind::String")); }
    let _tmp0 = ();
    Ok(AtomKind::String)
}

fn _any_parse_atom_kind_byte_string(value: &_Any) -> std::result::Result<AtomKind, _support::ParseError> {
    if value != &*_any_::LIT_5_BYTE_STRING { return Err(_support::ParseError::conformance_error("schema.AtomKind::ByteString")); }
    let _tmp0 = ();
    Ok(AtomKind::ByteString)
}

fn _any_parse_atom_kind_symbol(value: &_Any) -> std::result::Result<AtomKind, _support::ParseError> {
    if value != &*_any_::LIT_6_SYMBOL { return Err(_support::ParseError::conformance_error("schema.AtomKind::Symbol")); }
    let _tmp0 = ();
    Ok(AtomKind::Symbol)
}

impl std::convert::TryFrom<&_Any> for AtomKind {
    type Error = _support::ParseError;
    fn try_from(value: &_Any) -> std::result::Result<Self, _support::ParseError> {
        if let Ok(r) = _any_parse_atom_kind_boolean(value) { return Ok(r); }
        if let Ok(r) = _any_parse_atom_kind_float(value) { return Ok(r); }
        if let Ok(r) = _any_parse_atom_kind_double(value) { return Ok(r); }
        if let Ok(r) = _any_parse_atom_kind_signed_integer(value) { return Ok(r); }
        if let Ok(r) = _any_parse_atom_kind_string(value) { return Ok(r); }
        if let Ok(r) = _any_parse_atom_kind_byte_string(value) { return Ok(r); }
        if let Ok(r) = _any_parse_atom_kind_symbol(value) { return Ok(r); }
        Err(_support::ParseError::conformance_error("schema.AtomKind"))
    }
}

impl std::convert::From<&AtomKind> for _Any {
    fn from(value: &AtomKind) -> Self {
        match value {
            AtomKind::Boolean => (&*_any_::LIT_0_BOOLEAN).clone(),
            AtomKind::Float => (&*_any_::LIT_1_FLOAT).clone(),
            AtomKind::Double => (&*_any_::LIT_2_DOUBLE).clone(),
            AtomKind::SignedInteger => (&*_any_::LIT_3_SIGNED_INTEGER).clone(),
            AtomKind::String => (&*_any_::LIT_4_STRING).clone(),
            AtomKind::ByteString => (&*_any_::LIT_5_BYTE_STRING).clone(),
            AtomKind::Symbol => (&*_any_::LIT_6_SYMBOL).clone(),
        }
    }
}

fn read_atom_kind_boolean<'de, R: _support::Reader<'de, _Ptr, _Any>>(r: &mut R) -> std::result::Result<AtomKind, _support::ParseError> {
    match r.next_token(false)? {
        preserves::value::Token::Atom(v) => match v.value() {
            preserves::value::Value::Symbol(w) if w == "Boolean" => {}
            _ => return Err(_support::ParseError::conformance_error("schema.AtomKind::Boolean"))?,
        }
        _ => return Err(_support::ParseError::conformance_error("schema.AtomKind::Boolean"))?,
    }
    let _tmp0 = ();
    Ok(AtomKind::Boolean)
}

fn read_atom_kind_float<'de, R: _support::Reader<'de, _Ptr, _Any>>(r: &mut R) -> std::result::Result<AtomKind, _support::ParseError> {
    match r.next_token(false)? {
        preserves::value::Token::Atom(v) => match v.value() {
            preserves::value::Value::Symbol(w) if w == "Float" => {}
            _ => return Err(_support::ParseError::conformance_error("schema.AtomKind::Float"))?,
        }
        _ => return Err(_support::ParseError::conformance_error("schema.AtomKind::Float"))?,
    }
    let _tmp0 = ();
    Ok(AtomKind::Float)
}

fn read_atom_kind_double<'de, R: _support::Reader<'de, _Ptr, _Any>>(r: &mut R) -> std::result::Result<AtomKind, _support::ParseError> {
    match r.next_token(false)? {
        preserves::value::Token::Atom(v) => match v.value() {
            preserves::value::Value::Symbol(w) if w == "Double" => {}
            _ => return Err(_support::ParseError::conformance_error("schema.AtomKind::Double"))?,
        }
        _ => return Err(_support::ParseError::conformance_error("schema.AtomKind::Double"))?,
    }
    let _tmp0 = ();
    Ok(AtomKind::Double)
}

fn read_atom_kind_signed_integer<'de, R: _support::Reader<'de, _Ptr, _Any>>(r: &mut R) -> std::result::Result<AtomKind, _support::ParseError> {
    match r.next_token(false)? {
        preserves::value::Token::Atom(v) => match v.value() {
            preserves::value::Value::Symbol(w) if w == "SignedInteger" => {}
            _ => return Err(_support::ParseError::conformance_error("schema.AtomKind::SignedInteger"))?,
        }
        _ => return Err(_support::ParseError::conformance_error("schema.AtomKind::SignedInteger"))?,
    }
    let _tmp0 = ();
    Ok(AtomKind::SignedInteger)
}

fn read_atom_kind_string<'de, R: _support::Reader<'de, _Ptr, _Any>>(r: &mut R) -> std::result::Result<AtomKind, _support::ParseError> {
    match r.next_token(false)? {
        preserves::value::Token::Atom(v) => match v.value() {
            preserves::value::Value::Symbol(w) if w == "String" => {}
            _ => return Err(_support::ParseError::conformance_error("schema.AtomKind::String"))?,
        }
        _ => return Err(_support::ParseError::conformance_error("schema.AtomKind::String"))?,
    }
    let _tmp0 = ();
    Ok(AtomKind::String)
}

fn read_atom_kind_byte_string<'de, R: _support::Reader<'de, _Ptr, _Any>>(r: &mut R) -> std::result::Result<AtomKind, _support::ParseError> {
    match r.next_token(false)? {
        preserves::value::Token::Atom(v) => match v.value() {
            preserves::value::Value::Symbol(w) if w == "ByteString" => {}
            _ => return Err(_support::ParseError::conformance_error("schema.AtomKind::ByteString"))?,
        }
        _ => return Err(_support::ParseError::conformance_error("schema.AtomKind::ByteString"))?,
    }
    let _tmp0 = ();
    Ok(AtomKind::ByteString)
}

fn read_atom_kind_symbol<'de, R: _support::Reader<'de, _Ptr, _Any>>(r: &mut R) -> std::result::Result<AtomKind, _support::ParseError> {
    match r.next_token(false)? {
        preserves::value::Token::Atom(v) => match v.value() {
            preserves::value::Value::Symbol(w) if w == "Symbol" => {}
            _ => return Err(_support::ParseError::conformance_error("schema.AtomKind::Symbol"))?,
        }
        _ => return Err(_support::ParseError::conformance_error("schema.AtomKind::Symbol"))?,
    }
    let _tmp0 = ();
    Ok(AtomKind::Symbol)
}

impl<'de, R: _support::Reader<'de, _Ptr, _Any>> _support::Deserialize<'de, _Ptr, _Any, R> for AtomKind {
    fn deserialize(r: &mut R) -> std::result::Result<Self, _support::ParseError> {
        let _mark = r.mark()?;
        match read_atom_kind_boolean(r) { Err(e) if e.is_conformance_error() => r.restore(&_mark)?, result => return result }
        match read_atom_kind_float(r) { Err(e) if e.is_conformance_error() => r.restore(&_mark)?, result => return result }
        match read_atom_kind_double(r) { Err(e) if e.is_conformance_error() => r.restore(&_mark)?, result => return result }
        match read_atom_kind_signed_integer(r) { Err(e) if e.is_conformance_error() => r.restore(&_mark)?, result => return result }
        match read_atom_kind_string(r) { Err(e) if e.is_conformance_error() => r.restore(&_mark)?, result => return result }
        match read_atom_kind_byte_string(r) { Err(e) if e.is_conformance_error() => r.restore(&_mark)?, result => return result }
        match read_atom_kind_symbol(r) { Err(e) if e.is_conformance_error() => r.restore(&_mark)?, result => return result }
        Err(_support::ParseError::conformance_error("schema.AtomKind"))
    }
}

impl std::convert::TryFrom<&_Any> for Binding {
    type Error = _support::ParseError;
    fn try_from(value: &_Any) -> std::result::Result<Self, _support::ParseError> {
        let _tmp0 = value.value().to_record(None)?;
        if _tmp0.label() != &*_any_::LIT_7_NAMED { return Err(_support::ParseError::conformance_error("schema.Binding")); }
        let _tmp1 = ();
        if _tmp0.fields().len() < 2 { return Err(_support::ParseError::conformance_error("schema.Binding")); }
        let _tmp2 = (&_tmp0.fields()[0]).value().to_symbol()?;
        let _tmp3 = SimplePattern::try_from((&_tmp0.fields()[1]))?;
        Ok(Binding {name: _tmp2.clone(), pattern: _tmp3})
    }
}

impl std::convert::From<&Binding> for _Any {
    fn from(value: &Binding) -> Self {
        let Binding {name: _tmp0, pattern: _tmp1} = value;
        {
            let mut _tmp2 = preserves::value::Record(vec![(&*_any_::LIT_7_NAMED).clone()]);
            _tmp2.fields_vec_mut().push(preserves::value::Value::symbol(_tmp0).wrap());
            _tmp2.fields_vec_mut().push(_Any::from(_tmp1));
            _tmp2.finish().wrap()
        }
    }
}

impl<'de, R: _support::Reader<'de, _Ptr, _Any>> _support::Deserialize<'de, _Ptr, _Any, R> for Binding {
    fn deserialize(r: &mut R) -> std::result::Result<Self, _support::ParseError> {
        r.open_record(None)?;
        match r.next_token(false)? {
            preserves::value::Token::Atom(v) => match v.value() {
                preserves::value::Value::Symbol(w) if w == "named" => {}
                _ => return Err(_support::ParseError::conformance_error("schema.Binding"))?,
            }
            _ => return Err(_support::ParseError::conformance_error("schema.Binding"))?,
        }
        let _tmp0 = ();
        let _tmp1 = r.next_symbol()?.into_owned();
        let _tmp2 = SimplePattern::deserialize(r)?;
        r.ensure_complete()?;
        Ok(Binding {name: _tmp1, pattern: _tmp2})
    }
}

impl std::convert::TryFrom<&_Any> for Bundle {
    type Error = _support::ParseError;
    fn try_from(value: &_Any) -> std::result::Result<Self, _support::ParseError> {
        let _tmp0 = value.value().to_record(None)?;
        if _tmp0.label() != &*_any_::LIT_8_BUNDLE { return Err(_support::ParseError::conformance_error("schema.Bundle")); }
        let _tmp1 = ();
        if _tmp0.fields().len() < 1 { return Err(_support::ParseError::conformance_error("schema.Bundle")); }
        let _tmp2 = Modules::try_from((&_tmp0.fields()[0]))?;
        Ok(Bundle {modules: _tmp2})
    }
}

impl std::convert::From<&Bundle> for _Any {
    fn from(value: &Bundle) -> Self {
        let Bundle {modules: _tmp0} = value;
        {
            let mut _tmp1 = preserves::value::Record(vec![(&*_any_::LIT_8_BUNDLE).clone()]);
            _tmp1.fields_vec_mut().push(_Any::from(_tmp0));
            _tmp1.finish().wrap()
        }
    }
}

impl<'de, R: _support::Reader<'de, _Ptr, _Any>> _support::Deserialize<'de, _Ptr, _Any, R> for Bundle {
    fn deserialize(r: &mut R) -> std::result::Result<Self, _support::ParseError> {
        r.open_record(None)?;
        match r.next_token(false)? {
            preserves::value::Token::Atom(v) => match v.value() {
                preserves::value::Value::Symbol(w) if w == "bundle" => {}
                _ => return Err(_support::ParseError::conformance_error("schema.Bundle"))?,
            }
            _ => return Err(_support::ParseError::conformance_error("schema.Bundle"))?,
        }
        let _tmp0 = ();
        let _tmp1 = Modules::deserialize(r)?;
        r.ensure_complete()?;
        Ok(Bundle {modules: _tmp1})
    }
}

fn _any_parse_compound_pattern_rec(value: &_Any) -> std::result::Result<CompoundPattern, _support::ParseError> {
    let _tmp0 = value.value().to_record(None)?;
    if _tmp0.label() != &*_any_::LIT_9_REC { return Err(_support::ParseError::conformance_error("schema.CompoundPattern::rec")); }
    let _tmp1 = ();
    if _tmp0.fields().len() < 2 { return Err(_support::ParseError::conformance_error("schema.CompoundPattern::rec")); }
    let _tmp2 = NamedPattern::try_from((&_tmp0.fields()[0]))?;
    let _tmp3 = NamedPattern::try_from((&_tmp0.fields()[1]))?;
    Ok(CompoundPattern::Rec {label: std::boxed::Box::new(_tmp2), fields: std::boxed::Box::new(_tmp3)})
}

fn _any_parse_compound_pattern_tuple(value: &_Any) -> std::result::Result<CompoundPattern, _support::ParseError> {
    let _tmp0 = value.value().to_record(None)?;
    if _tmp0.label() != &*_any_::LIT_10_TUPLE { return Err(_support::ParseError::conformance_error("schema.CompoundPattern::tuple")); }
    let _tmp1 = ();
    if _tmp0.fields().len() < 1 { return Err(_support::ParseError::conformance_error("schema.CompoundPattern::tuple")); }
    let _tmp3 = (&_tmp0.fields()[0]).value().to_sequence()?;
    let mut _tmp2 = std::vec::Vec::new();
    for _tmp4 in &_tmp3[0..] {let _tmp5 = NamedPattern::try_from(_tmp4)?; _tmp2.push(_tmp5);}
    Ok(CompoundPattern::Tuple {patterns: _tmp2})
}

fn _any_parse_compound_pattern_tuple_prefix(value: &_Any) -> std::result::Result<CompoundPattern, _support::ParseError> {
    let _tmp0 = value.value().to_record(None)?;
    if _tmp0.label() != &*_any_::LIT_11_TUPLE_PREFIX { return Err(_support::ParseError::conformance_error("schema.CompoundPattern::tuplePrefix")); }
    let _tmp1 = ();
    if _tmp0.fields().len() < 2 { return Err(_support::ParseError::conformance_error("schema.CompoundPattern::tuplePrefix")); }
    let _tmp3 = (&_tmp0.fields()[0]).value().to_sequence()?;
    let mut _tmp2 = std::vec::Vec::new();
    for _tmp4 in &_tmp3[0..] {let _tmp5 = NamedPattern::try_from(_tmp4)?; _tmp2.push(_tmp5);}
    let _tmp6 = NamedSimplePattern::try_from((&_tmp0.fields()[1]))?;
    Ok(CompoundPattern::TuplePrefix {fixed: _tmp2, variable: std::boxed::Box::new(_tmp6)})
}

fn _any_parse_compound_pattern_dict(value: &_Any) -> std::result::Result<CompoundPattern, _support::ParseError> {
    let _tmp0 = value.value().to_record(None)?;
    if _tmp0.label() != &*_any_::LIT_12_DICT { return Err(_support::ParseError::conformance_error("schema.CompoundPattern::dict")); }
    let _tmp1 = ();
    if _tmp0.fields().len() < 1 { return Err(_support::ParseError::conformance_error("schema.CompoundPattern::dict")); }
    let _tmp2 = DictionaryEntries::try_from((&_tmp0.fields()[0]))?;
    Ok(CompoundPattern::Dict {entries: std::boxed::Box::new(_tmp2)})
}

impl std::convert::TryFrom<&_Any> for CompoundPattern {
    type Error = _support::ParseError;
    fn try_from(value: &_Any) -> std::result::Result<Self, _support::ParseError> {
        if let Ok(r) = _any_parse_compound_pattern_rec(value) { return Ok(r); }
        if let Ok(r) = _any_parse_compound_pattern_tuple(value) { return Ok(r); }
        if let Ok(r) = _any_parse_compound_pattern_tuple_prefix(value) { return Ok(r); }
        if let Ok(r) = _any_parse_compound_pattern_dict(value) { return Ok(r); }
        Err(_support::ParseError::conformance_error("schema.CompoundPattern"))
    }
}

impl std::convert::From<&CompoundPattern> for _Any {
    fn from(value: &CompoundPattern) -> Self {
        match value {
            CompoundPattern::Rec {label: _tmp0, fields: _tmp1} => {
                let mut _tmp2 = preserves::value::Record(vec![(&*_any_::LIT_9_REC).clone()]);
                _tmp2.fields_vec_mut().push(_Any::from(_tmp0.as_ref()));
                _tmp2.fields_vec_mut().push(_Any::from(_tmp1.as_ref()));
                _tmp2.finish().wrap()
            },
            CompoundPattern::Tuple {patterns: _tmp0} => {
                let mut _tmp1 = preserves::value::Record(vec![(&*_any_::LIT_10_TUPLE).clone()]);
                _tmp1.fields_vec_mut().push(
                    {
                        let mut _tmp2 = std::vec::Vec::new();
                        for _tmp3 in _tmp0 {_tmp2.push(_Any::from(_tmp3));}
                        preserves::value::Value::Sequence(_tmp2).wrap()
                    }
                );
                _tmp1.finish().wrap()
            },
            CompoundPattern::TuplePrefix {fixed: _tmp0, variable: _tmp1} => {
                let mut _tmp2 = preserves::value::Record(vec![(&*_any_::LIT_11_TUPLE_PREFIX).clone()]);
                _tmp2.fields_vec_mut().push(
                    {
                        let mut _tmp3 = std::vec::Vec::new();
                        for _tmp4 in _tmp0 {_tmp3.push(_Any::from(_tmp4));}
                        preserves::value::Value::Sequence(_tmp3).wrap()
                    }
                );
                _tmp2.fields_vec_mut().push(_Any::from(_tmp1.as_ref()));
                _tmp2.finish().wrap()
            },
            CompoundPattern::Dict {entries: _tmp0} => {
                let mut _tmp1 = preserves::value::Record(vec![(&*_any_::LIT_12_DICT).clone()]);
                _tmp1.fields_vec_mut().push(_Any::from(_tmp0.as_ref()));
                _tmp1.finish().wrap()
            },
        }
    }
}

fn read_compound_pattern_rec<'de, R: _support::Reader<'de, _Ptr, _Any>>(r: &mut R) -> std::result::Result<CompoundPattern, _support::ParseError> {
    r.open_record(None)?;
    match r.next_token(false)? {
        preserves::value::Token::Atom(v) => match v.value() {
            preserves::value::Value::Symbol(w) if w == "rec" => {}
            _ => return Err(_support::ParseError::conformance_error("schema.CompoundPattern::rec"))?,
        }
        _ => return Err(_support::ParseError::conformance_error("schema.CompoundPattern::rec"))?,
    }
    let _tmp0 = ();
    let _tmp1 = NamedPattern::deserialize(r)?;
    let _tmp2 = NamedPattern::deserialize(r)?;
    r.ensure_complete()?;
    Ok(CompoundPattern::Rec {label: std::boxed::Box::new(_tmp1), fields: std::boxed::Box::new(_tmp2)})
}

fn read_compound_pattern_tuple<'de, R: _support::Reader<'de, _Ptr, _Any>>(r: &mut R) -> std::result::Result<CompoundPattern, _support::ParseError> {
    r.open_record(None)?;
    match r.next_token(false)? {
        preserves::value::Token::Atom(v) => match v.value() {
            preserves::value::Value::Symbol(w) if w == "tuple" => {}
            _ => return Err(_support::ParseError::conformance_error("schema.CompoundPattern::tuple"))?,
        }
        _ => return Err(_support::ParseError::conformance_error("schema.CompoundPattern::tuple"))?,
    }
    let _tmp0 = ();
    r.open_sequence()?;
    let mut _tmp1 = std::vec::Vec::new();
    while !(r.close_compound()?) {let _tmp2 = NamedPattern::deserialize(r)?; _tmp1.push(_tmp2);}
    r.ensure_complete()?;
    Ok(CompoundPattern::Tuple {patterns: _tmp1})
}

fn read_compound_pattern_tuple_prefix<'de, R: _support::Reader<'de, _Ptr, _Any>>(r: &mut R) -> std::result::Result<CompoundPattern, _support::ParseError> {
    r.open_record(None)?;
    match r.next_token(false)? {
        preserves::value::Token::Atom(v) => match v.value() {
            preserves::value::Value::Symbol(w) if w == "tuplePrefix" => {}
            _ => return Err(_support::ParseError::conformance_error("schema.CompoundPattern::tuplePrefix"))?,
        }
        _ => return Err(_support::ParseError::conformance_error("schema.CompoundPattern::tuplePrefix"))?,
    }
    let _tmp0 = ();
    r.open_sequence()?;
    let mut _tmp1 = std::vec::Vec::new();
    while !(r.close_compound()?) {let _tmp2 = NamedPattern::deserialize(r)?; _tmp1.push(_tmp2);}
    let _tmp3 = NamedSimplePattern::deserialize(r)?;
    r.ensure_complete()?;
    Ok(CompoundPattern::TuplePrefix {fixed: _tmp1, variable: std::boxed::Box::new(_tmp3)})
}

fn read_compound_pattern_dict<'de, R: _support::Reader<'de, _Ptr, _Any>>(r: &mut R) -> std::result::Result<CompoundPattern, _support::ParseError> {
    r.open_record(None)?;
    match r.next_token(false)? {
        preserves::value::Token::Atom(v) => match v.value() {
            preserves::value::Value::Symbol(w) if w == "dict" => {}
            _ => return Err(_support::ParseError::conformance_error("schema.CompoundPattern::dict"))?,
        }
        _ => return Err(_support::ParseError::conformance_error("schema.CompoundPattern::dict"))?,
    }
    let _tmp0 = ();
    let _tmp1 = DictionaryEntries::deserialize(r)?;
    r.ensure_complete()?;
    Ok(CompoundPattern::Dict {entries: std::boxed::Box::new(_tmp1)})
}

impl<'de, R: _support::Reader<'de, _Ptr, _Any>> _support::Deserialize<'de, _Ptr, _Any, R> for CompoundPattern {
    fn deserialize(r: &mut R) -> std::result::Result<Self, _support::ParseError> {
        let _mark = r.mark()?;
        match read_compound_pattern_rec(r) { Err(e) if e.is_conformance_error() => r.restore(&_mark)?, result => return result }
        match read_compound_pattern_tuple(r) { Err(e) if e.is_conformance_error() => r.restore(&_mark)?, result => return result }
        match read_compound_pattern_tuple_prefix(r) { Err(e) if e.is_conformance_error() => r.restore(&_mark)?, result => return result }
        match read_compound_pattern_dict(r) { Err(e) if e.is_conformance_error() => r.restore(&_mark)?, result => return result }
        Err(_support::ParseError::conformance_error("schema.CompoundPattern"))
    }
}

fn _any_parse_definition_or(value: &_Any) -> std::result::Result<Definition, _support::ParseError> {
    let _tmp0 = value.value().to_record(None)?;
    if _tmp0.label() != &*_any_::LIT_13_OR { return Err(_support::ParseError::conformance_error("schema.Definition::or")); }
    let _tmp1 = ();
    if _tmp0.fields().len() < 1 { return Err(_support::ParseError::conformance_error("schema.Definition::or")); }
    let _tmp2 = (&_tmp0.fields()[0]).value().to_sequence()?;
    if _tmp2.len() < 2 { return Err(_support::ParseError::conformance_error("schema.Definition::or")); }
    let _tmp3 = NamedAlternative::try_from((&_tmp2[0]))?;
    let _tmp4 = NamedAlternative::try_from((&_tmp2[1]))?;
    let mut _tmp5 = std::vec::Vec::new();
    for _tmp6 in &_tmp2[2..] {let _tmp7 = NamedAlternative::try_from(_tmp6)?; _tmp5.push(_tmp7);}
    Ok(Definition::Or {
        pattern_0: std::boxed::Box::new(_tmp3),
        pattern_1: std::boxed::Box::new(_tmp4),
        pattern_n: _tmp5
    })
}

fn _any_parse_definition_and(value: &_Any) -> std::result::Result<Definition, _support::ParseError> {
    let _tmp0 = value.value().to_record(None)?;
    if _tmp0.label() != &*_any_::LIT_14_AND { return Err(_support::ParseError::conformance_error("schema.Definition::and")); }
    let _tmp1 = ();
    if _tmp0.fields().len() < 1 { return Err(_support::ParseError::conformance_error("schema.Definition::and")); }
    let _tmp2 = (&_tmp0.fields()[0]).value().to_sequence()?;
    if _tmp2.len() < 2 { return Err(_support::ParseError::conformance_error("schema.Definition::and")); }
    let _tmp3 = NamedPattern::try_from((&_tmp2[0]))?;
    let _tmp4 = NamedPattern::try_from((&_tmp2[1]))?;
    let mut _tmp5 = std::vec::Vec::new();
    for _tmp6 in &_tmp2[2..] {let _tmp7 = NamedPattern::try_from(_tmp6)?; _tmp5.push(_tmp7);}
    Ok(Definition::And {
        pattern_0: std::boxed::Box::new(_tmp3),
        pattern_1: std::boxed::Box::new(_tmp4),
        pattern_n: _tmp5
    })
}

fn _any_parse_definition_pattern(value: &_Any) -> std::result::Result<Definition, _support::ParseError> {
    let _tmp0 = Pattern::try_from(value)?;
    Ok(Definition::Pattern(std::boxed::Box::new(_tmp0)))
}

impl std::convert::TryFrom<&_Any> for Definition {
    type Error = _support::ParseError;
    fn try_from(value: &_Any) -> std::result::Result<Self, _support::ParseError> {
        if let Ok(r) = _any_parse_definition_or(value) { return Ok(r); }
        if let Ok(r) = _any_parse_definition_and(value) { return Ok(r); }
        if let Ok(r) = _any_parse_definition_pattern(value) { return Ok(r); }
        Err(_support::ParseError::conformance_error("schema.Definition"))
    }
}

impl std::convert::From<&Definition> for _Any {
    fn from(value: &Definition) -> Self {
        match value {
            Definition::Or {pattern_0: _tmp0, pattern_1: _tmp1, pattern_n: _tmp2} => {
                let mut _tmp3 = preserves::value::Record(vec![(&*_any_::LIT_13_OR).clone()]);
                _tmp3.fields_vec_mut().push(
                    {
                        let mut _tmp4 = std::vec::Vec::new();
                        _tmp4.push(_Any::from(_tmp0.as_ref()));
                        _tmp4.push(_Any::from(_tmp1.as_ref()));
                        for _tmp5 in _tmp2 {_tmp4.push(_Any::from(_tmp5));}
                        preserves::value::Value::Sequence(_tmp4).wrap()
                    }
                );
                _tmp3.finish().wrap()
            },
            Definition::And {pattern_0: _tmp0, pattern_1: _tmp1, pattern_n: _tmp2} => {
                let mut _tmp3 = preserves::value::Record(vec![(&*_any_::LIT_14_AND).clone()]);
                _tmp3.fields_vec_mut().push(
                    {
                        let mut _tmp4 = std::vec::Vec::new();
                        _tmp4.push(_Any::from(_tmp0.as_ref()));
                        _tmp4.push(_Any::from(_tmp1.as_ref()));
                        for _tmp5 in _tmp2 {_tmp4.push(_Any::from(_tmp5));}
                        preserves::value::Value::Sequence(_tmp4).wrap()
                    }
                );
                _tmp3.finish().wrap()
            },
            Definition::Pattern(_tmp0) => _Any::from(_tmp0.as_ref()),
        }
    }
}

fn read_definition_or<'de, R: _support::Reader<'de, _Ptr, _Any>>(r: &mut R) -> std::result::Result<Definition, _support::ParseError> {
    r.open_record(None)?;
    match r.next_token(false)? {
        preserves::value::Token::Atom(v) => match v.value() {
            preserves::value::Value::Symbol(w) if w == "or" => {}
            _ => return Err(_support::ParseError::conformance_error("schema.Definition::or"))?,
        }
        _ => return Err(_support::ParseError::conformance_error("schema.Definition::or"))?,
    }
    let _tmp0 = ();
    r.open_sequence()?;
    let _tmp1 = NamedAlternative::deserialize(r)?;
    let _tmp2 = NamedAlternative::deserialize(r)?;
    let mut _tmp3 = std::vec::Vec::new();
    while !(r.close_compound()?) {let _tmp4 = NamedAlternative::deserialize(r)?; _tmp3.push(_tmp4);}
    r.ensure_complete()?;
    Ok(Definition::Or {
        pattern_0: std::boxed::Box::new(_tmp1),
        pattern_1: std::boxed::Box::new(_tmp2),
        pattern_n: _tmp3
    })
}

fn read_definition_and<'de, R: _support::Reader<'de, _Ptr, _Any>>(r: &mut R) -> std::result::Result<Definition, _support::ParseError> {
    r.open_record(None)?;
    match r.next_token(false)? {
        preserves::value::Token::Atom(v) => match v.value() {
            preserves::value::Value::Symbol(w) if w == "and" => {}
            _ => return Err(_support::ParseError::conformance_error("schema.Definition::and"))?,
        }
        _ => return Err(_support::ParseError::conformance_error("schema.Definition::and"))?,
    }
    let _tmp0 = ();
    r.open_sequence()?;
    let _tmp1 = NamedPattern::deserialize(r)?;
    let _tmp2 = NamedPattern::deserialize(r)?;
    let mut _tmp3 = std::vec::Vec::new();
    while !(r.close_compound()?) {let _tmp4 = NamedPattern::deserialize(r)?; _tmp3.push(_tmp4);}
    r.ensure_complete()?;
    Ok(Definition::And {
        pattern_0: std::boxed::Box::new(_tmp1),
        pattern_1: std::boxed::Box::new(_tmp2),
        pattern_n: _tmp3
    })
}

fn read_definition_pattern<'de, R: _support::Reader<'de, _Ptr, _Any>>(r: &mut R) -> std::result::Result<Definition, _support::ParseError> {
    let _tmp0 = Pattern::deserialize(r)?;
    Ok(Definition::Pattern(std::boxed::Box::new(_tmp0)))
}

impl<'de, R: _support::Reader<'de, _Ptr, _Any>> _support::Deserialize<'de, _Ptr, _Any, R> for Definition {
    fn deserialize(r: &mut R) -> std::result::Result<Self, _support::ParseError> {
        let _mark = r.mark()?;
        match read_definition_or(r) { Err(e) if e.is_conformance_error() => r.restore(&_mark)?, result => return result }
        match read_definition_and(r) { Err(e) if e.is_conformance_error() => r.restore(&_mark)?, result => return result }
        match read_definition_pattern(r) { Err(e) if e.is_conformance_error() => r.restore(&_mark)?, result => return result }
        Err(_support::ParseError::conformance_error("schema.Definition"))
    }
}

impl std::convert::TryFrom<&_Any> for Definitions {
    type Error = _support::ParseError;
    fn try_from(value: &_Any) -> std::result::Result<Self, _support::ParseError> {
        let mut _tmp0 = preserves::value::Map::new();
        for (_tmp1, _tmp2) in value.value().to_dictionary()? {
            let _tmp3 = _tmp1.value().to_symbol()?;
            let _tmp4 = Definition::try_from(_tmp2)?;
            _tmp0.insert(_tmp3.clone(), _tmp4);
        }
        Ok(Definitions(_tmp0))
    }
}

impl std::convert::From<&Definitions> for _Any {
    fn from(value: &Definitions) -> Self {
        let Definitions(_tmp0) = value;
        preserves::value::Value::Dictionary(_tmp0.iter().map(|(_tmp1, _tmp2)| (preserves::value::Value::symbol(_tmp1).wrap(), _Any::from(_tmp2))).collect()).wrap()
    }
}

impl<'de, R: _support::Reader<'de, _Ptr, _Any>> _support::Deserialize<'de, _Ptr, _Any, R> for Definitions {
    fn deserialize(r: &mut R) -> std::result::Result<Self, _support::ParseError> {
        r.open_dictionary()?;
        let mut _tmp0 = preserves::value::Map::new();
        while !(r.close_compound()?) {
            let _tmp1 = r.next_symbol()?.into_owned();
            let _tmp2 = Definition::deserialize(r)?;
            _tmp0.insert(_tmp1, _tmp2);
        }
        Ok(Definitions(_tmp0))
    }
}

impl std::convert::TryFrom<&_Any> for DictionaryEntries {
    type Error = _support::ParseError;
    fn try_from(value: &_Any) -> std::result::Result<Self, _support::ParseError> {
        let mut _tmp0 = preserves::value::Map::new();
        for (_tmp1, _tmp2) in value.value().to_dictionary()? {
            let _tmp3 = _tmp1;
            let _tmp4 = NamedSimplePattern::try_from(_tmp2)?;
            _tmp0.insert(_tmp3.clone(), _tmp4);
        }
        Ok(DictionaryEntries(_tmp0))
    }
}

impl std::convert::From<&DictionaryEntries> for _Any {
    fn from(value: &DictionaryEntries) -> Self {
        let DictionaryEntries(_tmp0) = value;
        preserves::value::Value::Dictionary(_tmp0.iter().map(|(_tmp1, _tmp2)| (_tmp1.clone(), _Any::from(_tmp2))).collect()).wrap()
    }
}

impl<'de, R: _support::Reader<'de, _Ptr, _Any>> _support::Deserialize<'de, _Ptr, _Any, R> for DictionaryEntries {
    fn deserialize(r: &mut R) -> std::result::Result<Self, _support::ParseError> {
        r.open_dictionary()?;
        let mut _tmp0 = preserves::value::Map::new();
        while !(r.close_compound()?) {
            let _tmp1 = r.demand_next(false)?;
            let _tmp2 = NamedSimplePattern::deserialize(r)?;
            _tmp0.insert(_tmp1, _tmp2);
        }
        Ok(DictionaryEntries(_tmp0))
    }
}

fn _any_parse_embedded_type_name_ref(value: &_Any) -> std::result::Result<EmbeddedTypeName, _support::ParseError> {
    let _tmp0 = Ref::try_from(value)?;
    Ok(EmbeddedTypeName::Ref(std::boxed::Box::new(_tmp0)))
}

fn _any_parse_embedded_type_name_false(value: &_Any) -> std::result::Result<EmbeddedTypeName, _support::ParseError> {
    if value != &*_any_::LIT_15_FALSE { return Err(_support::ParseError::conformance_error("schema.EmbeddedTypeName::false")); }
    let _tmp0 = ();
    Ok(EmbeddedTypeName::False)
}

impl std::convert::TryFrom<&_Any> for EmbeddedTypeName {
    type Error = _support::ParseError;
    fn try_from(value: &_Any) -> std::result::Result<Self, _support::ParseError> {
        if let Ok(r) = _any_parse_embedded_type_name_ref(value) { return Ok(r); }
        if let Ok(r) = _any_parse_embedded_type_name_false(value) { return Ok(r); }
        Err(_support::ParseError::conformance_error("schema.EmbeddedTypeName"))
    }
}

impl std::convert::From<&EmbeddedTypeName> for _Any {
    fn from(value: &EmbeddedTypeName) -> Self {
        match value {
            EmbeddedTypeName::Ref(_tmp0) => _Any::from(_tmp0.as_ref()),
            EmbeddedTypeName::False => (&*_any_::LIT_15_FALSE).clone(),
        }
    }
}

fn read_embedded_type_name_ref<'de, R: _support::Reader<'de, _Ptr, _Any>>(r: &mut R) -> std::result::Result<EmbeddedTypeName, _support::ParseError> {
    let _tmp0 = Ref::deserialize(r)?;
    Ok(EmbeddedTypeName::Ref(std::boxed::Box::new(_tmp0)))
}

fn read_embedded_type_name_false<'de, R: _support::Reader<'de, _Ptr, _Any>>(r: &mut R) -> std::result::Result<EmbeddedTypeName, _support::ParseError> {
    match r.next_token(false)? {
        preserves::value::Token::Atom(v) => match v.value() {
            preserves::value::Value::Boolean(w) if !*w => {}
            _ => return Err(_support::ParseError::conformance_error("schema.EmbeddedTypeName::false"))?,
        }
        _ => return Err(_support::ParseError::conformance_error("schema.EmbeddedTypeName::false"))?,
    }
    let _tmp0 = ();
    Ok(EmbeddedTypeName::False)
}

impl<'de, R: _support::Reader<'de, _Ptr, _Any>> _support::Deserialize<'de, _Ptr, _Any, R> for EmbeddedTypeName {
    fn deserialize(r: &mut R) -> std::result::Result<Self, _support::ParseError> {
        let _mark = r.mark()?;
        match read_embedded_type_name_ref(r) { Err(e) if e.is_conformance_error() => r.restore(&_mark)?, result => return result }
        match read_embedded_type_name_false(r) { Err(e) if e.is_conformance_error() => r.restore(&_mark)?, result => return result }
        Err(_support::ParseError::conformance_error("schema.EmbeddedTypeName"))
    }
}

impl std::convert::TryFrom<&_Any> for ModulePath {
    type Error = _support::ParseError;
    fn try_from(value: &_Any) -> std::result::Result<Self, _support::ParseError> {
        let _tmp1 = value.value().to_sequence()?;
        let mut _tmp0 = std::vec::Vec::new();
        for _tmp2 in &_tmp1[0..] {let _tmp3 = _tmp2.value().to_symbol()?; _tmp0.push(_tmp3.clone());}
        Ok(ModulePath(_tmp0))
    }
}

impl std::convert::From<&ModulePath> for _Any {
    fn from(value: &ModulePath) -> Self {
        let ModulePath(_tmp0) = value;
        {
            let mut _tmp1 = std::vec::Vec::new();
            for _tmp2 in _tmp0 {_tmp1.push(preserves::value::Value::symbol(_tmp2).wrap());}
            preserves::value::Value::Sequence(_tmp1).wrap()
        }
    }
}

impl<'de, R: _support::Reader<'de, _Ptr, _Any>> _support::Deserialize<'de, _Ptr, _Any, R> for ModulePath {
    fn deserialize(r: &mut R) -> std::result::Result<Self, _support::ParseError> {
        r.open_sequence()?;
        let mut _tmp0 = std::vec::Vec::new();
        while !(r.close_compound()?) {let _tmp1 = r.next_symbol()?.into_owned(); _tmp0.push(_tmp1);}
        Ok(ModulePath(_tmp0))
    }
}

impl std::convert::TryFrom<&_Any> for Modules {
    type Error = _support::ParseError;
    fn try_from(value: &_Any) -> std::result::Result<Self, _support::ParseError> {
        let mut _tmp0 = preserves::value::Map::new();
        for (_tmp1, _tmp2) in value.value().to_dictionary()? {
            let _tmp3 = ModulePath::try_from(_tmp1)?;
            let _tmp4 = Schema::try_from(_tmp2)?;
            _tmp0.insert(_tmp3, _tmp4);
        }
        Ok(Modules(_tmp0))
    }
}

impl std::convert::From<&Modules> for _Any {
    fn from(value: &Modules) -> Self {
        let Modules(_tmp0) = value;
        preserves::value::Value::Dictionary(_tmp0.iter().map(|(_tmp1, _tmp2)| (_Any::from(_tmp1), _Any::from(_tmp2))).collect()).wrap()
    }
}

impl<'de, R: _support::Reader<'de, _Ptr, _Any>> _support::Deserialize<'de, _Ptr, _Any, R> for Modules {
    fn deserialize(r: &mut R) -> std::result::Result<Self, _support::ParseError> {
        r.open_dictionary()?;
        let mut _tmp0 = preserves::value::Map::new();
        while !(r.close_compound()?) {
            let _tmp1 = ModulePath::deserialize(r)?;
            let _tmp2 = Schema::deserialize(r)?;
            _tmp0.insert(_tmp1, _tmp2);
        }
        Ok(Modules(_tmp0))
    }
}

impl std::convert::TryFrom<&_Any> for NamedAlternative {
    type Error = _support::ParseError;
    fn try_from(value: &_Any) -> std::result::Result<Self, _support::ParseError> {
        let _tmp0 = value.value().to_sequence()?;
        if _tmp0.len() < 2 { return Err(_support::ParseError::conformance_error("schema.NamedAlternative")); }
        let _tmp1 = (&_tmp0[0]).value().to_string()?;
        let _tmp2 = Pattern::try_from((&_tmp0[1]))?;
        Ok(NamedAlternative {variant_label: _tmp1.clone(), pattern: _tmp2})
    }
}

impl std::convert::From<&NamedAlternative> for _Any {
    fn from(value: &NamedAlternative) -> Self {
        let NamedAlternative {variant_label: _tmp0, pattern: _tmp1} = value;
        {
            let mut _tmp2 = std::vec::Vec::new();
            _tmp2.push(preserves::value::Value::from(_tmp0).wrap());
            _tmp2.push(_Any::from(_tmp1));
            preserves::value::Value::Sequence(_tmp2).wrap()
        }
    }
}

impl<'de, R: _support::Reader<'de, _Ptr, _Any>> _support::Deserialize<'de, _Ptr, _Any, R> for NamedAlternative {
    fn deserialize(r: &mut R) -> std::result::Result<Self, _support::ParseError> {
        r.open_sequence()?;
        let _tmp0 = r.next_str()?.into_owned();
        let _tmp1 = Pattern::deserialize(r)?;
        r.ensure_complete()?;
        Ok(NamedAlternative {variant_label: _tmp0, pattern: _tmp1})
    }
}

fn _any_parse_named_pattern_named(value: &_Any) -> std::result::Result<NamedPattern, _support::ParseError> {
    let _tmp0 = Binding::try_from(value)?;
    Ok(NamedPattern::Named(std::boxed::Box::new(_tmp0)))
}

fn _any_parse_named_pattern_anonymous(value: &_Any) -> std::result::Result<NamedPattern, _support::ParseError> {
    let _tmp0 = Pattern::try_from(value)?;
    Ok(NamedPattern::Anonymous(std::boxed::Box::new(_tmp0)))
}

impl std::convert::TryFrom<&_Any> for NamedPattern {
    type Error = _support::ParseError;
    fn try_from(value: &_Any) -> std::result::Result<Self, _support::ParseError> {
        if let Ok(r) = _any_parse_named_pattern_named(value) { return Ok(r); }
        if let Ok(r) = _any_parse_named_pattern_anonymous(value) { return Ok(r); }
        Err(_support::ParseError::conformance_error("schema.NamedPattern"))
    }
}

impl std::convert::From<&NamedPattern> for _Any {
    fn from(value: &NamedPattern) -> Self {
        match value {
            NamedPattern::Named(_tmp0) => _Any::from(_tmp0.as_ref()),
            NamedPattern::Anonymous(_tmp0) => _Any::from(_tmp0.as_ref()),
        }
    }
}

fn read_named_pattern_named<'de, R: _support::Reader<'de, _Ptr, _Any>>(r: &mut R) -> std::result::Result<NamedPattern, _support::ParseError> {
    let _tmp0 = Binding::deserialize(r)?;
    Ok(NamedPattern::Named(std::boxed::Box::new(_tmp0)))
}

fn read_named_pattern_anonymous<'de, R: _support::Reader<'de, _Ptr, _Any>>(r: &mut R) -> std::result::Result<NamedPattern, _support::ParseError> {
    let _tmp0 = Pattern::deserialize(r)?;
    Ok(NamedPattern::Anonymous(std::boxed::Box::new(_tmp0)))
}

impl<'de, R: _support::Reader<'de, _Ptr, _Any>> _support::Deserialize<'de, _Ptr, _Any, R> for NamedPattern {
    fn deserialize(r: &mut R) -> std::result::Result<Self, _support::ParseError> {
        let _mark = r.mark()?;
        match read_named_pattern_named(r) { Err(e) if e.is_conformance_error() => r.restore(&_mark)?, result => return result }
        match read_named_pattern_anonymous(r) { Err(e) if e.is_conformance_error() => r.restore(&_mark)?, result => return result }
        Err(_support::ParseError::conformance_error("schema.NamedPattern"))
    }
}

fn _any_parse_named_simple_pattern_named(value: &_Any) -> std::result::Result<NamedSimplePattern, _support::ParseError> {
    let _tmp0 = Binding::try_from(value)?;
    Ok(NamedSimplePattern::Named(std::boxed::Box::new(_tmp0)))
}

fn _any_parse_named_simple_pattern_anonymous(value: &_Any) -> std::result::Result<NamedSimplePattern, _support::ParseError> {
    let _tmp0 = SimplePattern::try_from(value)?;
    Ok(NamedSimplePattern::Anonymous(std::boxed::Box::new(_tmp0)))
}

impl std::convert::TryFrom<&_Any> for NamedSimplePattern {
    type Error = _support::ParseError;
    fn try_from(value: &_Any) -> std::result::Result<Self, _support::ParseError> {
        if let Ok(r) = _any_parse_named_simple_pattern_named(value) { return Ok(r); }
        if let Ok(r) = _any_parse_named_simple_pattern_anonymous(value) { return Ok(r); }
        Err(_support::ParseError::conformance_error("schema.NamedSimplePattern"))
    }
}

impl std::convert::From<&NamedSimplePattern> for _Any {
    fn from(value: &NamedSimplePattern) -> Self {
        match value {
            NamedSimplePattern::Named(_tmp0) => _Any::from(_tmp0.as_ref()),
            NamedSimplePattern::Anonymous(_tmp0) => _Any::from(_tmp0.as_ref()),
        }
    }
}

fn read_named_simple_pattern_named<'de, R: _support::Reader<'de, _Ptr, _Any>>(r: &mut R) -> std::result::Result<NamedSimplePattern, _support::ParseError> {
    let _tmp0 = Binding::deserialize(r)?;
    Ok(NamedSimplePattern::Named(std::boxed::Box::new(_tmp0)))
}

fn read_named_simple_pattern_anonymous<'de, R: _support::Reader<'de, _Ptr, _Any>>(r: &mut R) -> std::result::Result<NamedSimplePattern, _support::ParseError> {
    let _tmp0 = SimplePattern::deserialize(r)?;
    Ok(NamedSimplePattern::Anonymous(std::boxed::Box::new(_tmp0)))
}

impl<'de, R: _support::Reader<'de, _Ptr, _Any>> _support::Deserialize<'de, _Ptr, _Any, R> for NamedSimplePattern {
    fn deserialize(r: &mut R) -> std::result::Result<Self, _support::ParseError> {
        let _mark = r.mark()?;
        match read_named_simple_pattern_named(r) { Err(e) if e.is_conformance_error() => r.restore(&_mark)?, result => return result }
        match read_named_simple_pattern_anonymous(r) { Err(e) if e.is_conformance_error() => r.restore(&_mark)?, result => return result }
        Err(_support::ParseError::conformance_error("schema.NamedSimplePattern"))
    }
}

fn _any_parse_pattern_simple_pattern(value: &_Any) -> std::result::Result<Pattern, _support::ParseError> {
    let _tmp0 = SimplePattern::try_from(value)?;
    Ok(Pattern::SimplePattern(std::boxed::Box::new(_tmp0)))
}

fn _any_parse_pattern_compound_pattern(value: &_Any) -> std::result::Result<Pattern, _support::ParseError> {
    let _tmp0 = CompoundPattern::try_from(value)?;
    Ok(Pattern::CompoundPattern(std::boxed::Box::new(_tmp0)))
}

impl std::convert::TryFrom<&_Any> for Pattern {
    type Error = _support::ParseError;
    fn try_from(value: &_Any) -> std::result::Result<Self, _support::ParseError> {
        if let Ok(r) = _any_parse_pattern_simple_pattern(value) { return Ok(r); }
        if let Ok(r) = _any_parse_pattern_compound_pattern(value) { return Ok(r); }
        Err(_support::ParseError::conformance_error("schema.Pattern"))
    }
}

impl std::convert::From<&Pattern> for _Any {
    fn from(value: &Pattern) -> Self {
        match value {
            Pattern::SimplePattern(_tmp0) => _Any::from(_tmp0.as_ref()),
            Pattern::CompoundPattern(_tmp0) => _Any::from(_tmp0.as_ref()),
        }
    }
}

fn read_pattern_simple_pattern<'de, R: _support::Reader<'de, _Ptr, _Any>>(r: &mut R) -> std::result::Result<Pattern, _support::ParseError> {
    let _tmp0 = SimplePattern::deserialize(r)?;
    Ok(Pattern::SimplePattern(std::boxed::Box::new(_tmp0)))
}

fn read_pattern_compound_pattern<'de, R: _support::Reader<'de, _Ptr, _Any>>(r: &mut R) -> std::result::Result<Pattern, _support::ParseError> {
    let _tmp0 = CompoundPattern::deserialize(r)?;
    Ok(Pattern::CompoundPattern(std::boxed::Box::new(_tmp0)))
}

impl<'de, R: _support::Reader<'de, _Ptr, _Any>> _support::Deserialize<'de, _Ptr, _Any, R> for Pattern {
    fn deserialize(r: &mut R) -> std::result::Result<Self, _support::ParseError> {
        let _mark = r.mark()?;
        match read_pattern_simple_pattern(r) { Err(e) if e.is_conformance_error() => r.restore(&_mark)?, result => return result }
        match read_pattern_compound_pattern(r) { Err(e) if e.is_conformance_error() => r.restore(&_mark)?, result => return result }
        Err(_support::ParseError::conformance_error("schema.Pattern"))
    }
}

impl std::convert::TryFrom<&_Any> for Ref {
    type Error = _support::ParseError;
    fn try_from(value: &_Any) -> std::result::Result<Self, _support::ParseError> {
        let _tmp0 = value.value().to_record(None)?;
        if _tmp0.label() != &*_any_::LIT_16_REF { return Err(_support::ParseError::conformance_error("schema.Ref")); }
        let _tmp1 = ();
        if _tmp0.fields().len() < 2 { return Err(_support::ParseError::conformance_error("schema.Ref")); }
        let _tmp2 = ModulePath::try_from((&_tmp0.fields()[0]))?;
        let _tmp3 = (&_tmp0.fields()[1]).value().to_symbol()?;
        Ok(Ref {module: _tmp2, name: _tmp3.clone()})
    }
}

impl std::convert::From<&Ref> for _Any {
    fn from(value: &Ref) -> Self {
        let Ref {module: _tmp0, name: _tmp1} = value;
        {
            let mut _tmp2 = preserves::value::Record(vec![(&*_any_::LIT_16_REF).clone()]);
            _tmp2.fields_vec_mut().push(_Any::from(_tmp0));
            _tmp2.fields_vec_mut().push(preserves::value::Value::symbol(_tmp1).wrap());
            _tmp2.finish().wrap()
        }
    }
}

impl<'de, R: _support::Reader<'de, _Ptr, _Any>> _support::Deserialize<'de, _Ptr, _Any, R> for Ref {
    fn deserialize(r: &mut R) -> std::result::Result<Self, _support::ParseError> {
        r.open_record(None)?;
        match r.next_token(false)? {
            preserves::value::Token::Atom(v) => match v.value() {
                preserves::value::Value::Symbol(w) if w == "ref" => {}
                _ => return Err(_support::ParseError::conformance_error("schema.Ref"))?,
            }
            _ => return Err(_support::ParseError::conformance_error("schema.Ref"))?,
        }
        let _tmp0 = ();
        let _tmp1 = ModulePath::deserialize(r)?;
        let _tmp2 = r.next_symbol()?.into_owned();
        r.ensure_complete()?;
        Ok(Ref {module: _tmp1, name: _tmp2})
    }
}

impl std::convert::TryFrom<&_Any> for Schema {
    type Error = _support::ParseError;
    fn try_from(value: &_Any) -> std::result::Result<Self, _support::ParseError> {
        let _tmp0 = value.value().to_record(None)?;
        if _tmp0.label() != &*_any_::LIT_17_SCHEMA { return Err(_support::ParseError::conformance_error("schema.Schema")); }
        let _tmp1 = ();
        if _tmp0.fields().len() < 1 { return Err(_support::ParseError::conformance_error("schema.Schema")); }
        let _tmp2 = (&_tmp0.fields()[0]).value().to_dictionary()?;
        let _tmp3 = _tmp2.get(&*_any_::LIT_18_DEFINITIONS).ok_or_else(|| _support::ParseError::conformance_error("schema.Schema"))?;
        let _tmp4 = Definitions::try_from(_tmp3)?;
        let _tmp5 = _tmp2.get(&*_any_::LIT_19_EMBEDDED_TYPE).ok_or_else(|| _support::ParseError::conformance_error("schema.Schema"))?;
        let _tmp6 = EmbeddedTypeName::try_from(_tmp5)?;
        let _tmp7 = _tmp2.get(&*_any_::LIT_20_VERSION).ok_or_else(|| _support::ParseError::conformance_error("schema.Schema"))?;
        let _tmp8 = Version::try_from(_tmp7)?;
        Ok(Schema {definitions: _tmp4, embedded_type: _tmp6, version: _tmp8})
    }
}

impl std::convert::From<&Schema> for _Any {
    fn from(value: &Schema) -> Self {
        let Schema {definitions: _tmp0, embedded_type: _tmp1, version: _tmp2} = value;
        {
            let mut _tmp3 = preserves::value::Record(vec![(&*_any_::LIT_17_SCHEMA).clone()]);
            _tmp3.fields_vec_mut().push(
                {
                    let mut _tmp4 = preserves::value::Map::new();
                    _tmp4.insert((&*_any_::LIT_18_DEFINITIONS).clone(), _Any::from(_tmp0));
                    _tmp4.insert((&*_any_::LIT_19_EMBEDDED_TYPE).clone(), _Any::from(_tmp1));
                    _tmp4.insert((&*_any_::LIT_20_VERSION).clone(), _Any::from(_tmp2));
                    preserves::value::Value::Dictionary(_tmp4).wrap()
                }
            );
            _tmp3.finish().wrap()
        }
    }
}

impl<'de, R: _support::Reader<'de, _Ptr, _Any>> _support::Deserialize<'de, _Ptr, _Any, R> for Schema {
    fn deserialize(r: &mut R) -> std::result::Result<Self, _support::ParseError> {
        r.open_record(None)?;
        match r.next_token(false)? {
            preserves::value::Token::Atom(v) => match v.value() {
                preserves::value::Value::Symbol(w) if w == "schema" => {}
                _ => return Err(_support::ParseError::conformance_error("schema.Schema"))?,
            }
            _ => return Err(_support::ParseError::conformance_error("schema.Schema"))?,
        }
        let _tmp0 = ();
        r.open_dictionary()?;
        let mut _tmp1 = None;
        let mut _tmp2 = None;
        let mut _tmp3 = None;
        while !(r.close_compound()?) {
            match r.next_token(false)? {
                preserves::value::Token::Atom(v) => match v.value() {
                    preserves::value::Value::Symbol(w) if w == "definitions" => {_tmp1 = Some(Definitions::deserialize(r)?); continue;}
                    preserves::value::Value::Symbol(w) if w == "embeddedType" => {_tmp2 = Some(EmbeddedTypeName::deserialize(r)?); continue;}
                    preserves::value::Value::Symbol(w) if w == "version" => {_tmp3 = Some(Version::deserialize(r)?); continue;}
                    _ => return Err(_support::ParseError::conformance_error("schema.Schema"))?,
                }
                _ => return Err(_support::ParseError::conformance_error("schema.Schema"))?,
            }
        }
        r.ensure_complete()?;
        Ok(Schema {
            definitions: _tmp1.ok_or_else(|| _support::ParseError::conformance_error("schema.Schema"))?,
            embedded_type: _tmp2.ok_or_else(|| _support::ParseError::conformance_error("schema.Schema"))?,
            version: _tmp3.ok_or_else(|| _support::ParseError::conformance_error("schema.Schema"))?
        })
    }
}

fn _any_parse_simple_pattern_any(value: &_Any) -> std::result::Result<SimplePattern, _support::ParseError> {
    if value != &*_any_::LIT_21_ANY { return Err(_support::ParseError::conformance_error("schema.SimplePattern::any")); }
    let _tmp0 = ();
    Ok(SimplePattern::Any)
}

fn _any_parse_simple_pattern_atom(value: &_Any) -> std::result::Result<SimplePattern, _support::ParseError> {
    let _tmp0 = value.value().to_record(None)?;
    if _tmp0.label() != &*_any_::LIT_22_ATOM { return Err(_support::ParseError::conformance_error("schema.SimplePattern::atom")); }
    let _tmp1 = ();
    if _tmp0.fields().len() < 1 { return Err(_support::ParseError::conformance_error("schema.SimplePattern::atom")); }
    let _tmp2 = AtomKind::try_from((&_tmp0.fields()[0]))?;
    Ok(SimplePattern::Atom {atom_kind: std::boxed::Box::new(_tmp2)})
}

fn _any_parse_simple_pattern_embedded(value: &_Any) -> std::result::Result<SimplePattern, _support::ParseError> {
    let _tmp0 = value.value().to_record(None)?;
    if _tmp0.label() != &*_any_::LIT_23_EMBEDDED { return Err(_support::ParseError::conformance_error("schema.SimplePattern::embedded")); }
    let _tmp1 = ();
    if _tmp0.fields().len() < 1 { return Err(_support::ParseError::conformance_error("schema.SimplePattern::embedded")); }
    let _tmp2 = SimplePattern::try_from((&_tmp0.fields()[0]))?;
    Ok(SimplePattern::Embedded {interface: std::boxed::Box::new(_tmp2)})
}

fn _any_parse_simple_pattern_lit(value: &_Any) -> std::result::Result<SimplePattern, _support::ParseError> {
    let _tmp0 = value.value().to_record(None)?;
    if _tmp0.label() != &*_any_::LIT_24_LIT { return Err(_support::ParseError::conformance_error("schema.SimplePattern::lit")); }
    let _tmp1 = ();
    if _tmp0.fields().len() < 1 { return Err(_support::ParseError::conformance_error("schema.SimplePattern::lit")); }
    let _tmp2 = (&_tmp0.fields()[0]);
    Ok(SimplePattern::Lit {value: _tmp2.clone()})
}

fn _any_parse_simple_pattern_seqof(value: &_Any) -> std::result::Result<SimplePattern, _support::ParseError> {
    let _tmp0 = value.value().to_record(None)?;
    if _tmp0.label() != &*_any_::LIT_25_SEQOF { return Err(_support::ParseError::conformance_error("schema.SimplePattern::seqof")); }
    let _tmp1 = ();
    if _tmp0.fields().len() < 1 { return Err(_support::ParseError::conformance_error("schema.SimplePattern::seqof")); }
    let _tmp2 = SimplePattern::try_from((&_tmp0.fields()[0]))?;
    Ok(SimplePattern::Seqof {pattern: std::boxed::Box::new(_tmp2)})
}

fn _any_parse_simple_pattern_setof(value: &_Any) -> std::result::Result<SimplePattern, _support::ParseError> {
    let _tmp0 = value.value().to_record(None)?;
    if _tmp0.label() != &*_any_::LIT_26_SETOF { return Err(_support::ParseError::conformance_error("schema.SimplePattern::setof")); }
    let _tmp1 = ();
    if _tmp0.fields().len() < 1 { return Err(_support::ParseError::conformance_error("schema.SimplePattern::setof")); }
    let _tmp2 = SimplePattern::try_from((&_tmp0.fields()[0]))?;
    Ok(SimplePattern::Setof {pattern: std::boxed::Box::new(_tmp2)})
}

fn _any_parse_simple_pattern_dictof(value: &_Any) -> std::result::Result<SimplePattern, _support::ParseError> {
    let _tmp0 = value.value().to_record(None)?;
    if _tmp0.label() != &*_any_::LIT_27_DICTOF { return Err(_support::ParseError::conformance_error("schema.SimplePattern::dictof")); }
    let _tmp1 = ();
    if _tmp0.fields().len() < 2 { return Err(_support::ParseError::conformance_error("schema.SimplePattern::dictof")); }
    let _tmp2 = SimplePattern::try_from((&_tmp0.fields()[0]))?;
    let _tmp3 = SimplePattern::try_from((&_tmp0.fields()[1]))?;
    Ok(SimplePattern::Dictof {key: std::boxed::Box::new(_tmp2), value: std::boxed::Box::new(_tmp3)})
}

fn _any_parse_simple_pattern_ref(value: &_Any) -> std::result::Result<SimplePattern, _support::ParseError> {
    let _tmp0 = Ref::try_from(value)?;
    Ok(SimplePattern::Ref(std::boxed::Box::new(_tmp0)))
}

impl std::convert::TryFrom<&_Any> for SimplePattern {
    type Error = _support::ParseError;
    fn try_from(value: &_Any) -> std::result::Result<Self, _support::ParseError> {
        if let Ok(r) = _any_parse_simple_pattern_any(value) { return Ok(r); }
        if let Ok(r) = _any_parse_simple_pattern_atom(value) { return Ok(r); }
        if let Ok(r) = _any_parse_simple_pattern_embedded(value) { return Ok(r); }
        if let Ok(r) = _any_parse_simple_pattern_lit(value) { return Ok(r); }
        if let Ok(r) = _any_parse_simple_pattern_seqof(value) { return Ok(r); }
        if let Ok(r) = _any_parse_simple_pattern_setof(value) { return Ok(r); }
        if let Ok(r) = _any_parse_simple_pattern_dictof(value) { return Ok(r); }
        if let Ok(r) = _any_parse_simple_pattern_ref(value) { return Ok(r); }
        Err(_support::ParseError::conformance_error("schema.SimplePattern"))
    }
}

impl std::convert::From<&SimplePattern> for _Any {
    fn from(value: &SimplePattern) -> Self {
        match value {
            SimplePattern::Any => (&*_any_::LIT_21_ANY).clone(),
            SimplePattern::Atom {atom_kind: _tmp0} => {
                let mut _tmp1 = preserves::value::Record(vec![(&*_any_::LIT_22_ATOM).clone()]);
                _tmp1.fields_vec_mut().push(_Any::from(_tmp0.as_ref()));
                _tmp1.finish().wrap()
            },
            SimplePattern::Embedded {interface: _tmp0} => {
                let mut _tmp1 = preserves::value::Record(vec![(&*_any_::LIT_23_EMBEDDED).clone()]);
                _tmp1.fields_vec_mut().push(_Any::from(_tmp0.as_ref()));
                _tmp1.finish().wrap()
            },
            SimplePattern::Lit {value: _tmp0} => {
                let mut _tmp1 = preserves::value::Record(vec![(&*_any_::LIT_24_LIT).clone()]);
                _tmp1.fields_vec_mut().push(_tmp0.clone());
                _tmp1.finish().wrap()
            },
            SimplePattern::Seqof {pattern: _tmp0} => {
                let mut _tmp1 = preserves::value::Record(vec![(&*_any_::LIT_25_SEQOF).clone()]);
                _tmp1.fields_vec_mut().push(_Any::from(_tmp0.as_ref()));
                _tmp1.finish().wrap()
            },
            SimplePattern::Setof {pattern: _tmp0} => {
                let mut _tmp1 = preserves::value::Record(vec![(&*_any_::LIT_26_SETOF).clone()]);
                _tmp1.fields_vec_mut().push(_Any::from(_tmp0.as_ref()));
                _tmp1.finish().wrap()
            },
            SimplePattern::Dictof {key: _tmp0, value: _tmp1} => {
                let mut _tmp2 = preserves::value::Record(vec![(&*_any_::LIT_27_DICTOF).clone()]);
                _tmp2.fields_vec_mut().push(_Any::from(_tmp0.as_ref()));
                _tmp2.fields_vec_mut().push(_Any::from(_tmp1.as_ref()));
                _tmp2.finish().wrap()
            },
            SimplePattern::Ref(_tmp0) => _Any::from(_tmp0.as_ref()),
        }
    }
}

fn read_simple_pattern_any<'de, R: _support::Reader<'de, _Ptr, _Any>>(r: &mut R) -> std::result::Result<SimplePattern, _support::ParseError> {
    match r.next_token(false)? {
        preserves::value::Token::Atom(v) => match v.value() {
            preserves::value::Value::Symbol(w) if w == "any" => {}
            _ => return Err(_support::ParseError::conformance_error("schema.SimplePattern::any"))?,
        }
        _ => return Err(_support::ParseError::conformance_error("schema.SimplePattern::any"))?,
    }
    let _tmp0 = ();
    Ok(SimplePattern::Any)
}

fn read_simple_pattern_atom<'de, R: _support::Reader<'de, _Ptr, _Any>>(r: &mut R) -> std::result::Result<SimplePattern, _support::ParseError> {
    r.open_record(None)?;
    match r.next_token(false)? {
        preserves::value::Token::Atom(v) => match v.value() {
            preserves::value::Value::Symbol(w) if w == "atom" => {}
            _ => return Err(_support::ParseError::conformance_error("schema.SimplePattern::atom"))?,
        }
        _ => return Err(_support::ParseError::conformance_error("schema.SimplePattern::atom"))?,
    }
    let _tmp0 = ();
    let _tmp1 = AtomKind::deserialize(r)?;
    r.ensure_complete()?;
    Ok(SimplePattern::Atom {atom_kind: std::boxed::Box::new(_tmp1)})
}

fn read_simple_pattern_embedded<'de, R: _support::Reader<'de, _Ptr, _Any>>(r: &mut R) -> std::result::Result<SimplePattern, _support::ParseError> {
    r.open_record(None)?;
    match r.next_token(false)? {
        preserves::value::Token::Atom(v) => match v.value() {
            preserves::value::Value::Symbol(w) if w == "embedded" => {}
            _ => return Err(_support::ParseError::conformance_error("schema.SimplePattern::embedded"))?,
        }
        _ => return Err(_support::ParseError::conformance_error("schema.SimplePattern::embedded"))?,
    }
    let _tmp0 = ();
    let _tmp1 = SimplePattern::deserialize(r)?;
    r.ensure_complete()?;
    Ok(SimplePattern::Embedded {interface: std::boxed::Box::new(_tmp1)})
}

fn read_simple_pattern_lit<'de, R: _support::Reader<'de, _Ptr, _Any>>(r: &mut R) -> std::result::Result<SimplePattern, _support::ParseError> {
    r.open_record(None)?;
    match r.next_token(false)? {
        preserves::value::Token::Atom(v) => match v.value() {
            preserves::value::Value::Symbol(w) if w == "lit" => {}
            _ => return Err(_support::ParseError::conformance_error("schema.SimplePattern::lit"))?,
        }
        _ => return Err(_support::ParseError::conformance_error("schema.SimplePattern::lit"))?,
    }
    let _tmp0 = ();
    let _tmp1 = r.demand_next(false)?;
    r.ensure_complete()?;
    Ok(SimplePattern::Lit {value: _tmp1})
}

fn read_simple_pattern_seqof<'de, R: _support::Reader<'de, _Ptr, _Any>>(r: &mut R) -> std::result::Result<SimplePattern, _support::ParseError> {
    r.open_record(None)?;
    match r.next_token(false)? {
        preserves::value::Token::Atom(v) => match v.value() {
            preserves::value::Value::Symbol(w) if w == "seqof" => {}
            _ => return Err(_support::ParseError::conformance_error("schema.SimplePattern::seqof"))?,
        }
        _ => return Err(_support::ParseError::conformance_error("schema.SimplePattern::seqof"))?,
    }
    let _tmp0 = ();
    let _tmp1 = SimplePattern::deserialize(r)?;
    r.ensure_complete()?;
    Ok(SimplePattern::Seqof {pattern: std::boxed::Box::new(_tmp1)})
}

fn read_simple_pattern_setof<'de, R: _support::Reader<'de, _Ptr, _Any>>(r: &mut R) -> std::result::Result<SimplePattern, _support::ParseError> {
    r.open_record(None)?;
    match r.next_token(false)? {
        preserves::value::Token::Atom(v) => match v.value() {
            preserves::value::Value::Symbol(w) if w == "setof" => {}
            _ => return Err(_support::ParseError::conformance_error("schema.SimplePattern::setof"))?,
        }
        _ => return Err(_support::ParseError::conformance_error("schema.SimplePattern::setof"))?,
    }
    let _tmp0 = ();
    let _tmp1 = SimplePattern::deserialize(r)?;
    r.ensure_complete()?;
    Ok(SimplePattern::Setof {pattern: std::boxed::Box::new(_tmp1)})
}

fn read_simple_pattern_dictof<'de, R: _support::Reader<'de, _Ptr, _Any>>(r: &mut R) -> std::result::Result<SimplePattern, _support::ParseError> {
    r.open_record(None)?;
    match r.next_token(false)? {
        preserves::value::Token::Atom(v) => match v.value() {
            preserves::value::Value::Symbol(w) if w == "dictof" => {}
            _ => return Err(_support::ParseError::conformance_error("schema.SimplePattern::dictof"))?,
        }
        _ => return Err(_support::ParseError::conformance_error("schema.SimplePattern::dictof"))?,
    }
    let _tmp0 = ();
    let _tmp1 = SimplePattern::deserialize(r)?;
    let _tmp2 = SimplePattern::deserialize(r)?;
    r.ensure_complete()?;
    Ok(SimplePattern::Dictof {key: std::boxed::Box::new(_tmp1), value: std::boxed::Box::new(_tmp2)})
}

fn read_simple_pattern_ref<'de, R: _support::Reader<'de, _Ptr, _Any>>(r: &mut R) -> std::result::Result<SimplePattern, _support::ParseError> {
    let _tmp0 = Ref::deserialize(r)?;
    Ok(SimplePattern::Ref(std::boxed::Box::new(_tmp0)))
}

impl<'de, R: _support::Reader<'de, _Ptr, _Any>> _support::Deserialize<'de, _Ptr, _Any, R> for SimplePattern {
    fn deserialize(r: &mut R) -> std::result::Result<Self, _support::ParseError> {
        let _mark = r.mark()?;
        match read_simple_pattern_any(r) { Err(e) if e.is_conformance_error() => r.restore(&_mark)?, result => return result }
        match read_simple_pattern_atom(r) { Err(e) if e.is_conformance_error() => r.restore(&_mark)?, result => return result }
        match read_simple_pattern_embedded(r) { Err(e) if e.is_conformance_error() => r.restore(&_mark)?, result => return result }
        match read_simple_pattern_lit(r) { Err(e) if e.is_conformance_error() => r.restore(&_mark)?, result => return result }
        match read_simple_pattern_seqof(r) { Err(e) if e.is_conformance_error() => r.restore(&_mark)?, result => return result }
        match read_simple_pattern_setof(r) { Err(e) if e.is_conformance_error() => r.restore(&_mark)?, result => return result }
        match read_simple_pattern_dictof(r) { Err(e) if e.is_conformance_error() => r.restore(&_mark)?, result => return result }
        match read_simple_pattern_ref(r) { Err(e) if e.is_conformance_error() => r.restore(&_mark)?, result => return result }
        Err(_support::ParseError::conformance_error("schema.SimplePattern"))
    }
}

impl std::convert::TryFrom<&_Any> for Version {
    type Error = _support::ParseError;
    fn try_from(value: &_Any) -> std::result::Result<Self, _support::ParseError> {
        if value != &*_any_::LIT_28_1 { return Err(_support::ParseError::conformance_error("schema.Version")); }
        let _tmp0 = ();
        Ok(Version)
    }
}

impl std::convert::From<&Version> for _Any {
    fn from(value: &Version) -> Self {let Version = value; (&*_any_::LIT_28_1).clone()}
}

impl<'de, R: _support::Reader<'de, _Ptr, _Any>> _support::Deserialize<'de, _Ptr, _Any, R> for Version {
    fn deserialize(r: &mut R) -> std::result::Result<Self, _support::ParseError> {
        match r.next_token(false)? {
            preserves::value::Token::Atom(v) => match v.value() {
                preserves::value::Value::SignedInteger(w) if *w == (1).into() => {}
                _ => return Err(_support::ParseError::conformance_error("schema.Version"))?,
            }
            _ => return Err(_support::ParseError::conformance_error("schema.Version"))?,
        }
        let _tmp0 = ();
        Ok(Version)
    }
}
