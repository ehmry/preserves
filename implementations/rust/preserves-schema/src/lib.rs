pub mod syntax;
pub mod compiler;
pub mod support;
pub mod gen;

#[cfg(test)]
mod tests {
    #[test]
    fn can_access_preserves_core() {
        use preserves::value::*;
        assert_eq!(format!("{:?}", UnwrappedIOValue::from(3 + 4)), "7");
    }

    #[test]
    fn simple_rendering() {
        use crate::*;
        use crate::syntax::block::*;

        let code = semiblock![
            seq!["f", parens!["a", "b", "c"]],
            seq!["f", parens!["a", "b", "c"]],
            seq!["f", parens!["a", "b", "c"]],
            seq!["f", parens!["a", "b", "c"]],
            seq!["f", parens!["a", "b", "c"]],
            seq!["f", parens!["a", "b", "c"]],
            seq!["g", parens![]],
            parens![]
        ];
        println!("{}", Formatter::to_string(&code));
    }

    #[test]
    fn metaschema_parsing() -> Result<(), std::io::Error> {
        use preserves::value::{BinarySource, IOBinarySource, IOValue, Reader};
        use std::convert::TryFrom;
        use std::convert::From;
        use crate::gen::schema::*;

        let mut f = std::fs::File::open("../../../schema/schema.bin")?;
        let mut src = IOBinarySource::new(&mut f);
        let mut reader = src.packed_iovalues();
        let schema = reader.demand_next(false)?;
        let parsed = Schema::try_from(&schema).expect("successful parse");
        assert_eq!(schema, IOValue::from(&parsed));
        Ok(())
    }
}
