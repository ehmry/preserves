pub use lazy_static::lazy_static;

pub use preserves::value::Reader;

use preserves::value::ArcValue;
use preserves::value::Domain;
use preserves::value::Embeddable;
use preserves::value::IOValue;
use preserves::value::NestedValue;
use preserves::value::NoEmbeddedDomainCodec;
use preserves::value::Value;

use std::convert::From;
use std::convert::TryFrom;
use std::io;
use std::sync::Arc;

use thiserror::Error;

pub trait Deserialize<'de, D: Embeddable, N: NestedValue<D>, R: Reader<'de, D, N>>
where
    Self: Sized
{
    fn deserialize(r: &mut R) -> Result<Self, ParseError>;
}

pub fn decode_lit<D: Embeddable, N: NestedValue<D>>(bs: &[u8]) -> io::Result<N> {
    preserves::value::packed::from_bytes(bs, NoEmbeddedDomainCodec)
}

pub fn decode_embedded<D: Domain>(
    v: &IOValue,
) -> Result<ArcValue<Arc<D>>, ParseError>
where
    for<'a> D: TryFrom<&'a IOValue, Error = ParseError>
{
    v.copy_via(&mut |d| Ok(Value::Embedded(Arc::new(D::try_from(d)?))))
}

pub fn encode_embedded<D: Domain>(
    v: &ArcValue<Arc<D>>,
) -> IOValue
where
    for<'a> IOValue: From<&'a D>
{
    v.copy_via::<_, _, _, std::convert::Infallible>(
        &mut |d| Ok(Value::Embedded(IOValue::from(d)))).unwrap()
}

#[derive(Error, Debug)]
pub enum ParseError {
    #[error("Input not conformant with Schema: {0}")]
    ConformanceError(&'static str),
    #[error(transparent)]
    Preserves(#[from] preserves::error::Error),
}

impl From<io::Error> for ParseError {
    fn from(v: io::Error) -> Self {
        preserves::error::Error::from(v).into()
    }
}

impl From<ParseError> for io::Error {
    fn from(v: ParseError) -> Self {
        match v {
            ParseError::ConformanceError(_) => io::Error::new(io::ErrorKind::InvalidData, v),
            ParseError::Preserves(e) => e.into(),
        }
    }
}

impl ParseError {
    pub fn conformance_error(context: &'static str) -> Self {
        ParseError::ConformanceError(context)
    }

    pub fn is_conformance_error(&self) -> bool {
        return if let ParseError::ConformanceError(_) = self { true } else { false }
    }
}
