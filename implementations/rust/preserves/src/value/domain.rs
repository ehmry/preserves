use std::io;

use super::BinarySource;
use super::Embeddable;
use super::IOValue;
use super::Reader;
use super::Writer;
use super::packed::PackedReader;

pub trait DomainDecode<D: Embeddable> {
    fn decode_embedded<'de, 'src, S: BinarySource<'de>>(
        &mut self,
        src: &'src mut S,
        read_annotations: bool,
    ) -> io::Result<D>;
}

pub trait DomainEncode<D: Embeddable> {
    fn encode_embedded<W: Writer>(
        &mut self,
        w: &mut W,
        d: &D,
    ) -> io::Result<()>;
}

impl <'a, D: Embeddable, T: DomainDecode<D>> DomainDecode<D> for &'a mut T {
    fn decode_embedded<'de, 'src, S: BinarySource<'de>>(
        &mut self,
        src: &'src mut S,
        read_annotations: bool,
    ) -> io::Result<D> {
        (**self).decode_embedded(src, read_annotations)
    }
}

pub struct IOValueDomainCodec;

impl DomainDecode<IOValue> for IOValueDomainCodec {
    fn decode_embedded<'de, 'src, S: BinarySource<'de>>(
        &mut self,
        src: &'src mut S,
        read_annotations: bool,
    ) -> io::Result<IOValue> {
        PackedReader::new(src, IOValueDomainCodec).demand_next(read_annotations)
    }
}

impl DomainEncode<IOValue> for IOValueDomainCodec {
    fn encode_embedded<W: Writer>(
        &mut self,
        w: &mut W,
        d: &IOValue,
    ) -> io::Result<()> {
        w.write(self, d)
    }
}

pub struct NoEmbeddedDomainCodec;

impl<D: Embeddable> DomainDecode<D> for NoEmbeddedDomainCodec {
    fn decode_embedded<'de, 'src, S: BinarySource<'de>>(
        &mut self,
        _src: &'src mut S,
        _read_annotations: bool,
    ) -> io::Result<D> {
        Err(io::Error::new(io::ErrorKind::Unsupported, "Embedded values not supported here"))
    }
}

impl<D: Embeddable> DomainEncode<D> for NoEmbeddedDomainCodec {
    fn encode_embedded<W: Writer>(
        &mut self,
        _w: &mut W,
        _d: &D,
    ) -> io::Result<()> {
        Err(io::Error::new(io::ErrorKind::Unsupported, "Embedded values not supported here"))
    }
}
