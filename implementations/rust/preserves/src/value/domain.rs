use std::io;

use super::BinarySource;
use super::BytesBinarySource;
use super::Embeddable;
use super::IOValue;
use super::Reader;
use super::Writer;
use super::packed;

pub trait DomainParse<D: Embeddable> {
    fn parse_embedded(
        &mut self,
        v: &IOValue,
    ) -> io::Result<D>;
}

pub trait DomainDecode<D: Embeddable> {
    fn decode_embedded<'de, 'src, S: BinarySource<'de>>(
        &mut self,
        src: &'src mut S,
        read_annotations: bool,
    ) -> io::Result<D>;
}

pub trait DomainEncode<D: Embeddable> {
    fn encode_embedded<W: Writer>(
        &mut self,
        w: &mut W,
        d: &D,
    ) -> io::Result<()>;
}

impl<'a, D: Embeddable, T: DomainParse<D>> DomainParse<D> for &'a mut T {
    fn parse_embedded(
        &mut self,
        v: &IOValue,
    ) -> io::Result<D> {
        (**self).parse_embedded(v)
    }
}

impl<'a, D: Embeddable, T: DomainDecode<D>> DomainDecode<D> for &'a mut T {
    fn decode_embedded<'de, 'src, S: BinarySource<'de>>(
        &mut self,
        src: &'src mut S,
        read_annotations: bool,
    ) -> io::Result<D> {
        (**self).decode_embedded(src, read_annotations)
    }
}

pub struct IOValueDomainCodec;

impl DomainDecode<IOValue> for IOValueDomainCodec {
    fn decode_embedded<'de, 'src, S: BinarySource<'de>>(
        &mut self,
        src: &'src mut S,
        read_annotations: bool,
    ) -> io::Result<IOValue> {
        packed::PackedReader::new(src, IOValueDomainCodec).demand_next(read_annotations)
    }
}

impl DomainEncode<IOValue> for IOValueDomainCodec {
    fn encode_embedded<W: Writer>(
        &mut self,
        w: &mut W,
        d: &IOValue,
    ) -> io::Result<()> {
        w.write(self, d)
    }
}

pub struct NoEmbeddedDomainCodec;

impl<D: Embeddable> DomainDecode<D> for NoEmbeddedDomainCodec {
    fn decode_embedded<'de, 'src, S: BinarySource<'de>>(
        &mut self,
        _src: &'src mut S,
        _read_annotations: bool,
    ) -> io::Result<D> {
        Err(io::Error::new(io::ErrorKind::Unsupported, "Embedded values not supported here"))
    }
}

impl<D: Embeddable> DomainEncode<D> for NoEmbeddedDomainCodec {
    fn encode_embedded<W: Writer>(
        &mut self,
        _w: &mut W,
        _d: &D,
    ) -> io::Result<()> {
        Err(io::Error::new(io::ErrorKind::Unsupported, "Embedded values not supported here"))
    }
}

pub struct ViaCodec<C>(C);

impl<C> ViaCodec<C> {
    pub fn new(c: C) -> Self {
        ViaCodec(c)
    }
}

impl<D: Embeddable, C: DomainDecode<D>> DomainParse<D> for ViaCodec<C> {
    fn parse_embedded(
        &mut self,
        v: &IOValue,
    ) -> io::Result<D> {
        let bs = packed::PackedWriter::encode_iovalue(v)?;
        self.0.decode_embedded(&mut BytesBinarySource::new(&bs), true)
    }
}

impl<D: Embeddable, C: DomainParse<D>> DomainDecode<D> for ViaCodec<C> {
    fn decode_embedded<'de, 'src, S: BinarySource<'de>>(
        &mut self,
        src: &'src mut S,
        read_annotations: bool,
    ) -> io::Result<D> {
        let v = src.packed(IOValueDomainCodec).demand_next(read_annotations)?;
        self.0.parse_embedded(&v)
    }
}
