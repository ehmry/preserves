pub mod boundary;
pub mod de;
pub mod domain;
pub mod magic;
pub mod packed;
pub mod reader;
pub mod repr;
pub mod ser;
pub mod signed_integer;
pub mod suspendable;
pub mod text;
pub mod writer;
pub mod merge;

pub use de::Deserializer;
pub use de::from_value;
pub use domain::DomainDecode;
pub use domain::DomainEncode;
pub use domain::DomainParse;
pub use domain::IOValueDomainCodec;
pub use domain::NoEmbeddedDomainCodec;
pub use domain::ViaCodec;
pub use merge::merge;
pub use packed::PackedReader;
pub use packed::PackedWriter;
pub use reader::BinarySource;
pub use reader::BytesBinarySource;
pub use reader::ConfiguredReader;
pub use reader::IOBinarySource;
pub use reader::Reader;
pub use reader::Token;
pub use repr::AnnotatedValue;
pub use repr::ArcValue;
pub use repr::AtomClass;
pub use repr::CompoundClass;
pub use repr::Domain;
pub use repr::Double;
pub use repr::DummyValue;
pub use repr::Embeddable;
pub use repr::Float;
pub use repr::IOValue;
pub use repr::Map;
pub use repr::NestedValue;
pub use repr::PlainValue;
pub use repr::RcValue;
pub use repr::Record;
pub use repr::Set;
pub use repr::UnwrappedIOValue;
pub use repr::Value;
pub use repr::ValueClass;
pub use ser::Serializer;
pub use ser::to_value;
pub use text::TextReader;
pub use text::TextWriter;
pub use writer::Writer;

pub fn invert_map<A, B>(m: &Map<A, B>) -> Map<B, A>
    where A: Clone, B: Clone + Ord
{
    m.iter().map(|(a, b)| (b.clone(), a.clone())).collect()
}
