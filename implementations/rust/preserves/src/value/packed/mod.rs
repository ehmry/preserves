pub mod constants;
pub mod reader;
pub mod writer;

pub use reader::PackedReader;
pub use writer::PackedWriter;

use std::io;

use super::{BinarySource, DomainDecode, IOValue, IOValueDomainCodec, NestedValue, Reader};

pub fn from_bytes<N: NestedValue, Dec: DomainDecode<N::Embedded>>(
    bs: &[u8],
    decode_embedded: Dec,
) -> io::Result<N> {
    super::BytesBinarySource::new(bs).packed(decode_embedded).demand_next(false)
}

pub fn iovalue_from_bytes(bs: &[u8]) -> io::Result<IOValue> {
    from_bytes(bs, IOValueDomainCodec)
}

pub fn annotated_from_bytes<N: NestedValue, Dec: DomainDecode<N::Embedded>>(
    bs: &[u8],
    decode_embedded: Dec,
) -> io::Result<N> {
    super::BytesBinarySource::new(bs).packed(decode_embedded).demand_next(true)
}

pub fn annotated_iovalue_from_bytes(bs: &[u8]) -> io::Result<IOValue> {
    annotated_from_bytes(bs, IOValueDomainCodec)
}
