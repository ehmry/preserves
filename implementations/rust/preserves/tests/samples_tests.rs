use preserves::error::{is_eof_io_error, is_syntax_io_error};
use preserves::symbol::Symbol;
use preserves::value::BinarySource;
use preserves::value::BytesBinarySource;
use preserves::value::IOBinarySource;
use preserves::value::IOValue;
use preserves::value::PackedWriter;
use preserves::value::Reader;
use preserves::value::de::from_value as deserialize_from_value;
use std::iter::Iterator;
use std::io;

mod samples;
use samples::*;

fn decode_all(bytes: &'_ [u8]) -> io::Result<Vec<IOValue>> {
    BytesBinarySource::new(bytes).packed_iovalues().configured(true).collect()
}

#[test] fn run() -> io::Result<()> {
    let mut fh = std::fs::File::open("../../../tests/samples.bin").unwrap();
    let mut src = IOBinarySource::new(&mut fh);
    let mut d = src.packed_iovalues().configured(true);
    let tests: TestCases = deserialize_from_value(&d.next().unwrap().unwrap()).unwrap();
    // println!("{:#?}", tests);

    for (Symbol(ref name), ref case) in tests.tests {
        println!("{:?} ==> {:?}", name, case);
        match case {
            TestCase::Test(ref bin, ref val) => {
                assert_eq!(&decode_all(&PackedWriter::encode_iovalue(val)?[..])?, &[val.clone()]);
                assert_eq!(&decode_all(&bin[..])?, &[val.clone()]);
                assert_eq!(&PackedWriter::encode_iovalue(val)?, bin);
            }
            TestCase::NondeterministicTest(ref bin, ref val) => {
                // The test cases in samples.pr are carefully written
                // so that while strictly "nondeterministic", the
                // order of keys in encoded dictionaries follows
                // Preserves canonical order.
                assert_eq!(&PackedWriter::encode_iovalue(val)?, bin);
                assert_eq!(&decode_all(&PackedWriter::encode_iovalue(val)?[..])?, &[val.clone()]);
                assert_eq!(&decode_all(&bin[..])?, &[val.clone()]);
            }
            TestCase::DecodeTest(ref bin, ref val) => {
                assert_eq!(&decode_all(&PackedWriter::encode_iovalue(val)?[..])?, &[val.clone()]);
                assert_eq!(&decode_all(&bin[..])?, &[val.clone()]);
            }
            TestCase::ParseError(_) => (),
            TestCase::ParseShort(_) => (),
            TestCase::ParseEOF(_) => (),
            TestCase::DecodeError(ref bin) => {
                match decode_all(&bin[..]) {
                    Ok(_) => panic!("Unexpected success"),
                    Err(e) => if is_syntax_io_error(&e) {
                        // all is OK
                    } else {
                        panic!("Unexpected error {:?}", e)
                    }
                }
            }
            TestCase::DecodeShort(ref bin) => {
                assert!(if let Err(e) = BytesBinarySource::new(bin).packed_iovalues().configured(true).next().unwrap() {
                    is_eof_io_error(&e)
                } else {
                    false
                })
            }
            TestCase::DecodeEOF(ref bin) => {
                assert!(BytesBinarySource::new(bin).packed_iovalues().configured(true).next().is_none());
            }
        }
    }
    Ok(())
}
