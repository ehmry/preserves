---
---

Motivations goals and priorities would be good - up front
 - so, I say values have an order and can be compared but NOT WHY that's useful
    - even if briefly, explain why that's important

On the YAML-JSON spectrum: JSON is like a transfer syntax for values
in JavaScript. The idea is to have a syntax to make those readable, so
when inspecting, they're easy to inspect. Not necessarily good for
writing those values. OTOH YAML, if it's intended this way, is
supposed to be easy to write. Not so useful for encoding values on the
wire. More oriented toward human reading-writing, where JSON is more
for computer use but human readable. This, then, is closer to JSON in
that respect. Textual syntax much more like JSON than YAML (though
that's easy to fix). Much more intended as a transfer syntax.

JSON lives up to its goal, as far as that goes: it is indeed a
representation of a subset of JS values!

Interesting features:

 - the total ordering. Because you also order between types, it makes
   MB think that this is the mechanism upon which something else
   useful will be built, rather than being an end in itself. What is
   that other thing?

 - if you have an equivalence, you have a chance of defining hash
   functions. You can use any hash so long as it is coarser than the
   equivalence.

 - Record syntax. Unusual! YAML has tagged values, but they don't work
   in quite the same way. They're probably equivalent, but in YAML
   you're encouraged to not call them records, they're *tagged
   values*. Where here, they're records. The expectation is that for
   each label, you interpret the "value", the field sequence, in a
   fixed way. I can think of lots of uses for this. One suggested in
   the doc is self-describing-struct things (field names, then the
   values); later the doc uses it for MIME; it seems to have uses,
   but...??? The line about records being more about algebraic data
   types is [good?]

Where matching on JSON data gets tricky in Go is when there's a union.
Say an event structure. Time, URL, plus some details depending on
event type. You end up writing deserializers that depend on some
discriminator. A field with type `interface{}`, plus another tag field
"event type" telling you what the thing is supposed to be. You write
the code to dispatch and decode by hand.

So "how would you do this stuff in Go?". You'd have types in your
library representing the preserve `Value`. You could decode to that if
you wanted to. Then an additional thing which is to take one of those
and decode it again in a type-directed way to Go structures. There
might be different ways of decoding the raw `Value`s into Go values!

Realization: what I actually want is not comments but docstrings! Or
annotations, or metadata. Reason for that instead of comments is
automated transformations of config files will lose comments. In
principle it might be possible to parse it in such a way as to be able
to operate on the value without losing the comments during
deserialization. BUT this is much easier if there's some recognition
of attaching some kind of metadata to a value. This then is another
reason for wanting an abstract model of a Preserves document that is
separate from say its Go-struct decoding: the abstract model can have
docstrings in it!

((CONCRETE SYNTAX TREE is missing! This would be the place to attach
annotations/comments))

Couple of uses for annotations/comments:
 - roundtripping config (augeas-style)
 - embedding trace information in messages, so even binary formatted
   `Value`s would carry their annotations which could be converted to
   text format for humans to look at
    - provenance information
       - how the value was calculated, which databases it came from, etc
       - which lines of source code produced certain pieces
    - stack information
       - a trace of machines it's been through; causal history
       - distributed trace information
    - ((nice thing about both of those is that they are PROPERLY
      metadata, they are not part of the domain, they are part of the
      implementation))
    - ((comments are another out-of-domain description about the
      value, that is not part of the value.))

((Idea:: Annotations "must not direct control flow". Labels are for
that. An annotation's presence must not cause the program to do
anything observably different.)) ((NB it's left open as to whether
labels *must* direct control flow! Clearly a label like `Date`
*wouldn't* direct control flow, so in some ways could be an
annotation, but it's a murky area.))
