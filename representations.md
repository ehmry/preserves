---
title: "Representing Values in Programming Languages"
---

**NOT YET READY**

We have given a definition of `Value` and its semantics, and proposed
a concrete syntax for communicating and storing `Value`s. We now turn
to **suggested** representations of `Value`s as *programming-language
values* for various programming languages.

When designing a language mapping, an important consideration is
roundtripping: serialization after deserialization, and vice versa,
should both be identities.

Also, the presence or absence of annotations on a `Value` should not
affect comparisons of that `Value` to others in any way.

## JavaScript.

 - `Boolean` ↔ `Boolean`
 - `Float` and `Double` ↔ numbers
 - `SignedInteger` ↔ numbers or `BigInt` (see [here](https://developers.google.com/web/updates/2018/05/bigint) and [here](https://github.com/tc39/proposal-bigint))
 - `String` ↔ strings
 - `ByteString` ↔ `Uint8Array`
 - `Symbol` ↔ `Symbol.for(...)`
 - `Record` ↔ `{ "_label": theLabel, "_fields": [field0, ..., fieldN] }`, plus convenience accessors
    - `(undefined)` ↔ the undefined value
    - `(rfc3339 F)` ↔ `Date`, if `F` matches the `date-time` RFC 3339 production
 - `Sequence` ↔ `Array`
 - `Set` ↔ `{ "_set": M }` where `M` is a `Map` from the elements of the set to `true`
 - `Dictionary` ↔ a `Map`

## Scheme/Racket.

 - `Boolean` ↔ booleans
 - `Float` and `Double` ↔ inexact numbers (Racket: single- and double-precision floats)
 - `SignedInteger` ↔ exact numbers
 - `String` ↔ strings
 - `ByteString` ↔ byte vector (Racket: "Bytes")
 - `Symbol` ↔ symbols
 - `Record` ↔ structures (Racket: prefab struct)
 - `Sequence` ↔ lists
 - `Set` ↔ Racket: sets
 - `Dictionary` ↔ Racket: hash-table

## Java.

 - `Boolean` ↔ `Boolean`
 - `Float` and `Double` ↔ `Float` and `Double`
 - `SignedInteger` ↔ `Integer`, `Long`, `BigInteger`
 - `String` ↔ `String`
 - `ByteString` ↔ `byte[]`
 - `Symbol` ↔ a simple data class wrapping a `String`
 - `Record` ↔ in a simple implementation, a generic `Record` class; else perhaps a bean mapping?
    - `(mime T B)` ↔ an implementation of `javax.activation.DataSource`?
 - `Sequence` ↔ an implementation of `java.util.List`
 - `Set` ↔ an implementation of `java.util.Set`
 - `Dictionary` ↔ an implementation of `java.util.Map`

## Erlang.

 - `Boolean` ↔ `true` and `false`
 - `Float` and `Double` ↔ floats (unsure how Erlang deals with single-precision)
 - `SignedInteger` ↔ integers
 - `String` ↔ pair of `utf8` and a binary
 - `ByteString` ↔ a binary
 - `Symbol` ↔ pair of `atom` and a binary
 - `Record` ↔ triple of `obj`, label, and field list
 - `Sequence` ↔ a list
 - `Set` ↔ a `sets` set
 - `Dictionary` ↔ a [map][erlang-map] (new in Erlang/OTP R17)

This is a somewhat unsatisfactory mapping because: (a) Erlang doesn't
garbage-collect its atoms, meaning that (a.1) representing `Symbol`s
as atoms could lead to denial-of-service and (a.2) representing
`Symbol`-labelled `Record`s as Erlang records must be rejected for the
same reason; (b) even if it did, Erlang's boolean values are atoms,
which would then clash with the `Symbol`s `true` and `false`; and (c)
Erlang has no distinct string type, making for a trilemma where
`String`s are in danger of clashing with `ByteString`s, `Sequence`s,
or `Record`s.

## Python.

 - `Boolean` ↔ `True` and `False`
 - `Float` ↔ a `Float` wrapper-class for a double-precision value
 - `Double` ↔ float
 - `SignedInteger` ↔ int and long
 - `String` ↔ `unicode`
 - `ByteString` ↔ `bytes`
 - `Symbol` ↔ a simple data class wrapping a `unicode`
 - `Record` ↔ something like `namedtuple`, but that doesn't care about class identity?
 - `Sequence` ↔ `tuple` (but accept `list` during encoding)
 - `Set` ↔ `frozenset` (but accept `set` during encoding)
 - `Dictionary` ↔ a hashable (immutable) dictionary-like thing (but accept `dict` during encoding)

## Squeak Smalltalk.

 - `Boolean` ↔ `true` and `false`
 - `Float` ↔ perhaps a subclass of `Float`?
 - `Double` ↔ `Float`
 - `SignedInteger` ↔ `Integer`
 - `String` ↔ `WideString`
 - `ByteString` ↔ `ByteArray`
 - `Symbol` ↔ `WideSymbol`
 - `Record` ↔ a simple data class
 - `Sequence` ↔ `ArrayedCollection` (usually `OrderedCollection`)
 - `Set` ↔ `Set`
 - `Dictionary` ↔ `Dictionary`
